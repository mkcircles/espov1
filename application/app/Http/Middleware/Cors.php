<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*$domains = ['http://localhost:4200'];

        if(isset($request->server()['HTTP_ORIGIN'])){
            $origin = $request->server()['HTTP_ORIGIN'];

            if(in_array($origin, $domains)){
                header('Access-Control-Allow-Origin: '.$origin);
                header('Access-Control-Allow-Headers: Content-Type, Origin, Authorization');
            }
        }
        return $next($request);*/
        $http_origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : false;
        $allowed_origins = ['https://vendors.pay-leo.com/api/' , 'http://localhost:4200'];

        if(in_array($http_origin, $allowed_origins)) {
            return $next($request)->header('Access-Control-Allow-Origin' , '*')
              ->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE')
              ->header('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With');
        }
        
        return $next($request);
        
    }
}
