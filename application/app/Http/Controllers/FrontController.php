<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use App\Level;
use App\Notes;
use App\Post;
use App\Subject;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{
    public function front(){
        $subject = DB::table('subjects')
            ->join('levels', 'subjects.level_id', '=', 'levels.id')
            ->inRandomOrder()->limit(8)->get();
        $notes = $this->getLatestNotes(4);
        $videos = $this->getLatestVideos(4);
        //Subject::inRandomOrder()->limit(4)->get();
        return view('frontend/index')->with(['subjects'=>$subject,'notes'=>$notes,'videos'=>$videos]);
    }

    /************COurses / Subjects ************/
    public function getCourses(){
        $subject = DB::table('subjects')
            ->join('levels', 'subjects.level_id', '=', 'levels.id')->get();
        $levels = Level::all();
        return view('frontend/subjects')->with(['subjects'=>$subject,'levels'=>$levels]);
    }
    public function getLevelCourses($levelId){
        $subject = DB::table('subjects')
            ->join('levels', 'subjects.level_id', '=', 'levels.id')
            ->where(['subjects.level_id'=>$levelId])->get();
        $level = Level::where(['id'=>$levelId])->first();
        $search = $level->name;
        return view('frontend/subjects')->with(['subjects'=>$subject,'side_subjects'=>$subject,'search'=>$search]);
    }
    public function getSubjectCourses($subjectId){
        $subject = Subject::where(['id'=>$subjectId])->select('subject_name')->first();
        $posts = DB::table('posts')
            ->join('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->where(['topics.subject_id'=>$subjectId/*,'posts.posts_status'=>'Active'*/])
            ->select('posts.*','subjects.subject_name','topics.topic_name')
            ->distinct()->paginate(12);
        $topics = DB::table('topics')
            ->join('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->where(['topics.subject_id'=>$subjectId/*,'posts.posts_status'=>'Active'*/])
            ->get();

        return view('frontend/subject-posts')->with(['posts'=>$posts,'topics'=>$topics,'subject'=>$subject]);
    }
    public function getNotes(){
        $post = DB::table('posts')
            ->join('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->where(['posts.has_notes'=>1/*,'posts.posts_status'=>'Active'*/])
            ->select('posts.*','subjects.subject_name','topics.topic_name','users.name')
            ->distinct()->paginate(12);
        return view('frontend/notes')->with(['notes'=>$post]);
    }
    public function getLatestNotes($limit){
        $post = DB::table('posts')
            ->join('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->where(['posts.has_notes'=>1/*,'posts.posts_status'=>'Active'*/])
            ->select('posts.*','subjects.subject_name','topics.topic_name','users.name')
            ->distinct()->orderBy('posts.id','DESC')->limit($limit)->get();
        return $post;
    }
    //
    public function getVideos(){
        $post = $post = DB::table('posts')
            ->join('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->where(['posts.has_video'=>1/*,'posts.posts_status'=>'Active'*/])
            ->select('posts.*','subjects.subject_name','topics.topic_name','users.name')
            ->distinct()->paginate(12);
        return view('frontend/video')->with(['videos'=>$post]);
    }

    public function getLatestVideos($limit){
        $post = DB::table('posts')
            ->join('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->where(['posts.has_video'=>1/*,'posts.posts_status'=>'Active'*/])
            ->select('posts.*','subjects.subject_name','topics.topic_name','users.name')
            ->distinct()->orderBy('posts.id','DESC')->limit($limit)->get();
        return $post;
    }
    /******** Notes Details******/
    public function getNotesDetails($id){
        $note = Notes::where(['post_id'=>$id])->first();

        if(count($note) > 0){
            $post = Post::where(['id'=>$id])->first();
            $teacher = User::where(['id'=>$post->user_id])->first();
            return view('frontend/notes_details')->with(['notes'=>$note,'post'=>$post,'teacher'=>$teacher]);
        }
        return redirect()->back()->with(['notes_error'=>'Notes Post not found']);
    }
    /******** Notes Details******/
    public function getVideoDetails($id){
        $video = Video::where(['post_id'=>$id])->first();
        if(count($video) > 0){
            $post = Post::where(['id'=>$id])->first();
            $teacher = User::where(['id'=>$post->user_id])->first();
            return view('frontend/video_details')->with(['video'=>$video,'post'=>$post,'teacher'=>$teacher]);
        }
        return redirect()->back()->with(['videos_error'=>'Videos Post not found']);

    }





}
