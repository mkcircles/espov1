<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use App\User;
use App\Level;
use App\Notes;
use App\Subject;
use App\Topic;
use App\SubTopic;
use App\Question;
use App\Answer;
use App\Video;
use App\School;
use App\Post;
use App\Ratings;
use App\Subscriptions;
use App\TeacherSubjects;
use Datatables;
use Youtube;
use Response;

class TeachersController extends Controller
{
    public function teacher_index(){
    	//subjects subscribed to ...
        $subjects_subscribed_to = TeacherSubjects::where(['user_id'=>Auth::id()])->get();
    	return view('teacher/index')->with(['subjects_subscribed_to'=>$subjects_subscribed_to]);
    }

    public function fetchTeacherData(){
    	//teachers posts...
        return Datatables::of(
            DB::table('posts')
            ->join('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('notes', 'posts.id', '=', 'notes.post_id')
            ->leftJoin('videos', 'posts.id', '=', 'videos.post_id')
            ->leftJoin('questions', 'posts.id', '=', 'questions.post_id')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
            ->leftJoin('subjects', 'subjects.id', '=', 'teacher_subjects.subject_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
            ->where(['users.id'=>Auth::id()])
            ->select('levels.name', 'posts.posts_status', 'posts.created_at', 'posts.has_notes', 'posts.has_video', 'posts.has_question', 'subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
            ->distinct('posts')
            ->get()
        )->make(true);
    }

    public function teachers_profile(){
    	return view('teacher/profile');
    }

    public function getNotes(){
        $notes = DB::table('notes')
            ->join('posts', 'notes.post_id', '=', 'posts.id')
            ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
            ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
            ->where(['users.id'=>Auth::id()])
            ->select('levels.name', 'notes.id', 'notes.notes_file', 'notes.file_type', 'notes.views', 'notes.notes_pricing', 'notes.notes_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
            ->distinct('notes')
            ->get();
        $teacherz_subjects = DB::table('subjects')
            ->join('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.subject_id', '=', 'subjects.id')
            ->leftJoin('users', 'teacher_subjects.user_id', '=', 'teacher_subjects.user_id')
            ->where(['teacher_subjects.user_id'=>Auth::id()])
            ->select('subjects.id', 'subjects.subject_name', 'levels.name')
            ->distinct('subjects')
            ->get();    
        //var_dump($notes); exit;    
        return view('teacher/notes')->with(['notes'=>$notes, 'teacherz_subjects'=>$teacherz_subjects]);
    }

    public function getTeacherSubjectTopics(Request $request){
        $validator = Validator::make($request->all(), [
            'subject_id' => 'required',
        ]);

        if($validator->passes())
        {
            //get topics under this subject id...
            $topics = Topic::where(['subject_id'=>$request->subject_id])->get();
            return $topics;
        }else{
            return redirect()->back();
        }
    }

    public function getTeacherSubjectTopicSubtopics(Request $request){
        $validator = Validator::make($request->all(), [
            'topic_id' => 'required',
        ]);

        if($validator->passes())
        {
            //get topics under this topic id...
            $topics = SubTopic::where(['topic_id'=>$request->topic_id])->get();
            return $topics;
        }else{
             return redirect()->back();
        }
    }

    public function postNotes(Request $request){
        $validator = Validator::make($request->all(), [
            'subject_id'=>'required',
            'topic_id'=>'required',
            'subtopic_id'=>'required',
            'billable'=>'required',
            'post_title'=>'required',
            //'topic_requirements'=>'required',
            //'topic_expectation'=>'required',
            'notes_description'=>'required',
            'cover_photo'=>'required',
            'notes_file'=>'required',
            //'questions_file'=>'required',
            //'answers_file'=>'required'
        ]);

        if($validator->passes())
        {
            $destinationPath = public_path().'/uploads/files';

            $notes_file = $request->file('notes_file');
            $questions_file = $request->file('questions_file');
            $answers_file = $request->file('answers_file');
            $cover_photo = $request->file('cover_photo');

            $new_post = new Post();
            $new_post->subtopic_id = $request->subtopic_id;
            $new_post->billable = $request->billable > 0 ? 1 : 0;
            $new_post->post_title = $request->post_title;
            $new_post->user_id = Auth::id();
            //$new_post->user_id = 2;
            $new_post->has_notes = 1;
            $new_post->has_video = 0;
            $new_post->has_question = empty($questions_file) ? 0 : 1;
            $new_post->has_answer = empty($answers_file) ? 0: 1;
            $new_post->posts_status = 'Inactive';

            if(!empty($cover_photo)) {
                $filename = $cover_photo->getClientOriginalName();
                $upload_cover_photo_success = $cover_photo->move($destinationPath, $filename);
                $new_post->cover_photo = $filename;
            }
            
            $new_post->save();

            if(!empty($notes_file)) {
                $filename = $notes_file->getClientOriginalName();
                $upload_questions_success = $notes_file->move($destinationPath, $filename);

                $new_notes = new Notes();
                $new_notes->post_id = $new_post->id;
                $new_notes->file_type = 'pdf';
                $new_notes->notes_file = $filename;
                $new_notes->description = $request->notes_description;
                $new_notes->requirements = $request->topic_requirements;
                $new_notes->expectations = $request->topic_expectation;
                $new_notes->notes_status = 'Inactive';
                $new_notes->views = 0;
                $new_notes->notes_pricing = $request->billable;
                $new_notes->currency = 'UGX';
                $new_notes->save();

            }

            if(!empty($questions_file)) {
                $filename = $questions_file->getClientOriginalName();
                $upload_questions_success = $questions_file->move($destinationPath, $filename);
                $new_post->question_file = $filename;

                $new_question = new Question();
                $new_question->post_id = $new_post->id;
                $new_question->file_type = 'pdf';
                $new_question->question_file = $filename;
                $new_question->question_status = 'Inactive';
                $new_question->views = 0;
                $new_question->question_pricing = $request->billable;
                $new_question->currency = 'UGX';
                $new_question->save();

                if(!empty($answers_file)){
                    $filename = $answers_file->getClientOriginalName();
                    $upload_answers_success = $answers_file->move($destinationPath, $filename);
                    $new_post->answer_file = $filename;

                    $new_question = new Answer();
                    $new_question->question_id = $new_question->id;
                    $new_question->file_type = 'pdf';
                    $new_question->answer_file = $filename;
                    $new_question->save();
                }
            }

            return redirect('/teacher/notes')->with(['notes_success' => 'You have successfully uploaded notes']);            
              
        }else{
            return redirect('/teacher/notes')->withErrors($validator)->withInput();
        }
    }

    public function fetchNotesData(Request $request){
        $validator = Validator::make($request->all(), [
            'notes_id' => 'required',
        ]);

        if($validator->passes())
        {
            $notes = DB::table('notes')                
                ->join('posts', 'posts.id', '=', 'notes.post_id')
                ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
                ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
                ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
                ->leftJoin('levels', 'levels.id', '=', 'subjects.id')
                ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
                ->where(['notes.id'=>$request->notes_id])
                ->select('levels.name', 'notes.id', 'notes.notes_file', 'notes.file_type', 'notes.views', 'notes.notes_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
                ->get();

            return $notes; 
            //return response()->file(public_path().'/uploads/files/'.$notes[0]->notes_file);   
        }else{
            return redirect('/teachers/notes')->withErrors($validator)->withInput();
        }
    }

    public function getTeacherSubjects(){
        return view('/teacher/subjects');
    }

    public function fetchTeacherSubjectsData(){
        return Datatables::of(
            DB::table('teacher_subjects')
            ->join('subjects', 'teacher_subjects.subject_id', '=', 'subjects.id')
            ->join('users', 'users.id', '=', 'teacher_subjects.user_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->where(['users.id'=>Auth::id()])
            //->where(['users.id'=>2])
            ->select('levels.name', 'subjects.subject_name', 'teacher_subjects.created_at' )
            ->distinct()
            ->get()
        )->make(true);
    }

    public function getVideos(){
        // $videos = DB::table('posts')
        //     ->join('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
        //     ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
        //     ->leftJoin('videos', 'posts.id', '=', 'videos.post_id')
        //     ->join('users', 'users.id', '=', 'posts.user_id')
        //     ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
        //     ->leftJoin('subjects', 'subjects.id', '=', 'teacher_subjects.subject_id')
        //     ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
        //     ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
        //     // ->where(['users.id'=>Auth::id()])
        //     ->where(['users.id'=>2])
        //     ->select('levels.name', 'videos.id', 'videos.video_file', 'videos.file_type', 'videos.views', 'videos.video_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
        //     ->get();
        $videos = DB::table('videos')
            ->join('posts', 'videos.post_id', '=', 'posts.id')
            ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
            ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
            ->where(['users.id'=>Auth::id()])
            ->select('levels.name', 'videos.id', 'videos.video_file', 'videos.file_type', 'videos.views','videos.video_pricing', 'videos.video_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
            ->distinct('videos')
            ->get();

        $teacherz_subjects = DB::table('subjects')
            ->join('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.subject_id', '=', 'subjects.id')
            ->leftJoin('users', 'teacher_subjects.user_id', '=', 'teacher_subjects.user_id')
            // ->where(['users.id'=>Auth::id()])
            ->where(['teacher_subjects.user_id'=>Auth::id()])
            ->select('subjects.id', 'subjects.subject_name', 'levels.name')
            ->distinct('subjects')
            ->get();    
        //var_dump($videos); exit; 
        return view('teacher/videos')->with(['videos'=>$videos, 'teacherz_subjects'=>$teacherz_subjects]);
    }

    public function postVideos(Request $request){

        $validator = Validator::make($request->all(), [
            'subject_id'=>'required',
            'topic_id'=>'required',
            'subtopic_id'=>'required',
            'billable'=>'required',
            'post_title'=>'required',
            //'topic_requirements'=>'required',
            //'topic_expectation'=>'required',
            'video_description'=>'required',
            'cover_photo'=>'required',
            'video_file'=>'required',
            //'questions_file'=>'required',
            //'answers_file'=>'required'
        ]);

        if($validator->passes())
        {
            $destinationPath = public_path().'/uploads/files';

            
            $cover_photo = $request->file('cover_photo');
            $questions_file = $request->file('questions_file');
            $answers_file = $request->file('answers_file');
            $video_file = $request->file('video_file');

            $new_post = new Post();
            $new_post->subtopic_id = $request->subtopic_id;
            $new_post->billable = $request->billable > 0 ? 1 : 0;
            $new_post->post_title = $request->post_title;
            //$new_post->user_id = Auth::id();
            $new_post->user_id = 2;
            $new_post->has_notes = 0;
            $new_post->has_video = 1;
            $new_post->has_question = empty($questions_file) ? 0 : 1;
            $new_post->has_answer = empty($answers_file) ? 0: 1;
            $new_post->posts_status = 'Inactive';

            if(!empty($cover_photo)) {
                $filename = $cover_photo->getClientOriginalName();
                $upload_cover_photo_success = $cover_photo->move($destinationPath, $filename);
                $new_post->cover_photo = $filename;
            }
            
            $new_post->save();

            if(!empty($video_file)) {
                //video tags(subjects, topic, sub-topic)...
                $videotagsubject = Subject::where(['id'=>$request->subject_id])->first();
                $videotagtopic = Topic::where(['id'=>$request->topic_id])->first();
                $videotagsubtopic = SubTopic::where(['id'=>$request->subtopic_id])->first();

                $video = Youtube::upload($video_file, [
                    'title'       => $request->post_title,
                    'description' => $request->video_description,
                    'tags'        => [$videotagsubject, $videotagtopic, $videotagsubtopic],
                    'category_id' => 10
                ], 'unlisted');
               /* $filename = $notes_file->getClientOriginalName();
                $upload_questions_success = $notes_file->move($destinationPath, $filename);*/


                $new_video = new Video();
                $new_video->post_id = $new_post->id;
                $new_video->file_type = 'pdf';
                $new_video->video_file = $video->getVideoId();
                // $new_video->video_file =  $filename;
                $new_video->description = $request->video_description;
                $new_video->requirements = $request->topic_requirements;
                $new_video->expectations = $request->topic_expectation;
                $new_video->video_status = 'Inactive';
                $new_video->views = 0;
                $new_video->video_pricing = $request->billable;
                $new_video->currency = 'UGX';
                $new_video->save();

            }

            if(!empty($questions_file)) {
                $filename = $questions_file->getClientOriginalName();
                $upload_questions_success = $questions_file->move($destinationPath, $filename);
                $new_post->question_file = $filename;

                $new_question = new Question();
                $new_question->post_id = $new_post->id;
                $new_question->file_type = 'pdf';
                $new_question->question_file = $filename;
                $new_question->question_status = 'Inactive';
                $new_question->views = 0;
                $new_question->question_pricing = $request->billable;
                $new_question->currency = 'UGX';
                $new_question->save();

                if(!empty($answers_file)){
                    $filename = $answers_file->getClientOriginalName();
                    $upload_answers_success = $answers_file->move($destinationPath, $filename);
                    $new_post->answer_file = $filename;

                    $new_question = new Answer();
                    $new_question->question_id = $new_question->id;
                    $new_question->file_type = 'pdf';
                    $new_question->answer_file = $filename;
                    $new_question->save();
                }
            }

            return redirect('/teacher/videos')->with(['video_success' => 'You have successfully uploaded a video']);            
              
        }else{
            return redirect('/teacher/videos')->withErrors($validator)->withInput();
        }
    }

    public function fetchVideoData(Request $request){
        $validator = Validator::make($request->all(), [
            'video_id' => 'required',
        ]);

        if($validator->passes())
        {
            $videos = DB::table('videos')                
                ->join('posts', 'posts.id', '=', 'videos.post_id')
                ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
                ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
                ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
                ->leftJoin('levels', 'levels.id', '=', 'subjects.id')
                ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
                ->where(['videos.id'=>$request->video_id])
                ->select('levels.name', 'videos.id', 'videos.video_file', 'videos.file_type', 'videos.views', 'videos.video_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
                ->get();
            return $videos;    
        }else{
            return redirect('/teachers/videos')->withErrors($validator)->withInput();
        }
    }



    // public function editVideo(Request $request){
    //     $validator = Validator::make($request->all(), [
    //         'video_id' => 'required',
    //     ]);

    //     if($validator->passes())
    //     {

    //     }
    // }
}
