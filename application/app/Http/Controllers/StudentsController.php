<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use App\User;
use App\Level;
use App\Notes;
use App\Subject;
use App\Topic;
use App\SubTopic;
use App\Question;
use App\Answer;
use App\Video;
use App\School;
use App\Post;
use App\Ratings;
use App\Subscriptions;
use App\TeacherSubjects;
use Datatables;
use Youtube;
use Response;

class StudentsController extends Controller
{
    public function login(Request $request){
    	$this->validate($request, [
            'email' => 'required',
            'password' => 'required'
            ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            //check if administrator or teacher...
            $user = User::where(['id'=>Auth::id()])->first();
            if($user->user_type == 'Student'){
                return redirect('/')->with(['student_success' => 'Welcome '.Auth::user()->name.', you have logged in successfully']);
            }
           
        }

        return redirect()->back()->with(['student_login_errors'=>'Wrong email or password']);
    }

    public function register(Request $request){
    	// getting all of the post data

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required',
            'country' => 'required'
        ]);

        $file = $request->file('profile_pic');

        if($validator->passes()){
            //$destinationPath = public_path().'/uploads/team_photos';
            $destinationPath = 'uploads/students_photos';

            $student_password = str_random(8);

            $student = new User();
            $student->name = $request->name;
            $student->email = $request->email;
            $student->contact = $request->phone;
            $student->password = bcrypt($student_password);
            $student->user_type = 'Student';
            $student->user_status = 'Inactive';
            $student->profile_desc =  $request->profile_desc;
            $student->country = $request->country;
            $student->address = $request->address;

            if(!empty($file)) {
                $filename = $file->getClientOriginalName();
                $upload_success = $file->move($destinationPath, $filename);
                $student->profile_pic = $filename;
            }else{
                $student->profile_pic = 'avator.jpg';
            }
            $student->save();
            //send email to the owner of the account
            mail($request->email,"Credentials for ESPO","Your password is ".$student_password);
            return redirect('/')->with(['student_success' => 'You have successfully created an Espo account, please check your email for password']);

        }else{
            return redirect('/')->withErrors($validator)->withInput();
        }
    }
}
