<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Payments;
use Response;

class PaymentsController extends Controller
{
    public function makePayment(Request $request){
    	$validator = Validator::make($request->all(), [
            'post_id' => 'required',
            'amount' => 'required',
            'phone' => 'required'
        ]);

        if($validator->fails())
        {
            return redirect('/admin/videos')->withErrors($validator)->withInput(); 
        }

        $transactionId = date('Ymd').''.date('hms');

        $payments = new Payments();
        $payments->post_id = $request->post_id;
        $payments->amount = $request->amount;
        $payments->phone = $request->phone;
        $payments->user_id = $request->user_id;
        $payments->status = 'pending';
        $payments->trans_id = $transactionId;
        $payments->save();

        #return the transaction id
        return $transactionId;
    }
}
