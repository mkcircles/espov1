<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use App\User;
use App\Level;
use App\Notes;
use App\Subject;
use App\Topic;
use App\SubTopic;
use App\Question;
use App\Answer;
use App\Video;
use App\School;
use App\Post;
use App\Ratings;
use App\Subscriptions;
use Datatables;

class AdminController extends Controller
{
    public function index(){
		
    }

    public function create_account(Request $request){
        // getting all of the post data
        $file = $request->file('staff_image');
       
        $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(array('file'=> $file), $rules);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'role' => 'required',
        ]);

        if($validator->passes()){
            //$destinationPath = public_path().'/uploads/team_photos';
            $destinationPath = 'uploads/team_photos';

            $staff_password = str_random(8);
            $staff = new User();
            $staff->name = $request->name;
            $staff->email = $request->email;
            $staff->contact = $request->contact;
            $staff->password = bcrypt($staff_password);
            $staff->role = $request->role;
            $staff->user_type = 'staff';
            $staff->position = $request->position;
            $staff->department =  $request->department;
            $staff->description =  $request->desc;

            if(!empty($file)) {
                $filename = $file->getClientOriginalName();
                $upload_success = $file->move($destinationPath, $filename);
                $staff->photoLink = $filename;
            }else{
                $staff->photoLink = 'avator.jpg';
            }

            $staff->save();
            //send email to the owner of the account
            mail($request->email,"Credentials for ESPO","Your password is ".$staff_password);
            return redirect('/admin/users')->with(['success' => 'You have successfully added a staff member']);
            
        }else{
            return redirect('/admin/users')->withErrors($validator)->withInput();
        }
    }

    
    public function delete(Request $request){
    	$validator = Validator::make($request->all(), [
            'userid' => 'required'
        ]);

        if($validator->passes()){
            $admin = User::where('id',$request->userid)->delete();
            return redirect('/admin/users')->with(['success' => 'You have successfully deleted a staff member']);
        }else{
            return redirect('/admin/users')->withErrors($validator)->withInput();
        }
    }

    /*************** Users Staff *****************/

    public function users_index(){
        return view('/backend/users');
    }

    public function fetchUsersData(){
        return Datatables::of(Users::all())->make(true);
    }

    /*************** Levels ****************/

    public function levels_index(){
        return view('backend/school/levels');
    }

    public function fetchLevelsData(){
        //return Datatables::of(Level::all())->make(true);
        return Datatables::of(
                DB::table('levels')
                ->leftJoin('subjects', 'levels.id', '=', 'subjects.level_id')
                ->leftJoin('teacher_subjects', 'teacher_subjects.subject_id', '=', 'subjects.id')
                ->select('levels.*', DB::raw('(select count(*) from subjects where levels.id = subjects.level_id) as no_of_subjects, (select count(*) from teacher_subjects where levels.id=subjects.level_id and subjects.id=teacher_subjects.subject_id) as no_of_teachers') )
                ->distinct('levels')
                ->get()
            )->make(true);
    }

    public function create_level(Request $request){
        $validator = Validator::make($request->all(), [
            'level_name' => 'required',
        ]);

        if($validator->passes())
        {
            $levelExists = Level::where(['name' => $request->level_name])->get();
            if(count($levelExists) == 0){
                $new_level = new Level();
                $new_level->name = $request->level_name;
                $new_level->save();
                
                return redirect('/admin/levels')->with(['level_success' => 'You have successfully added a level of education']);
            }

            return redirect('/admin/levels')->with(['level_error' => 'Duplicates not allowed. The level already exists']);
        }else{
            return redirect('/admin/levels')->withErrors($validator)->withInput(); 
        }
    }

    public function getLevel(Request $request){
        $validator = Validator::make($request->all(), [
            'level_id' => 'required'
        ]);

        if($validator->passes())
        {
            $level = Level::where(['id'=> $request->level_id])->first();
            return $level;
        }else{
            return redirect('/admin/levels')->withErrors($validator)->withInput(); 
        }    
    }

    public function edit_level(Request $request){
        $validator = Validator::make($request->all(), [
            'level_name' => 'required',
            'level_id' => 'required'
        ]);

        if($validator->passes())
        {
            $levelExists = Level::where(['name' => $request->level_name])->get();
            if(count($levelExists) == 0){
                $level = Level::where(['id'=> $request->level_id])->update(['name'=> $request->level_name]);
                
                return redirect('/admin/levels')->with(['level_success' => 'You have successfully edited a level of education']);
            }

            return redirect('/admin/levels')->with(['level_error' => 'Duplicates not allowed. The level already exists']);
        }else{
            return redirect('/admin/levels')->withErrors($validator)->withInput(); 
        }
    }

    public function delete_level(Request $request){
        $validator = Validator::make($request->all(), [
            'level_id' => 'required'
        ]);

        if($validator->passes())
        {
            $level = Level::where(['id'=> $request->level_id])->delete();
                
            return redirect('/admin/levels')->with(['level_success' => 'You have successfully deleted a level of education']);

        }else{
            return redirect('/admin/levels')->withErrors($validator)->withInput(); 
        }
    }

    public function getLevelDetails($level_id){
        $all_levels = Level::all();
        return view('backend/school/level')->with(['level_id'=>$level_id, 'levels' => $all_levels]);
    }

    public function fetch_levels_details_data(Request $request){
        $validator = Validator::make($request->all(), [
            'level_id' => 'required'
        ]);

        if($validator->passes())
        {
            return Datatables::of(
                Subject::where(['level_id'=>$request->level_id])->get()
                )->make(true);
        }else{
            return redirect('/admin/levels')->withErrors($validator)->withInput(); 
        }    
    }

    /*************** Subjects ****************/

    public function fetchSubjectsData(){
        return Datatables::of(Subject::all())->make(true);
    }

    public function create_subject(Request $request){
        $validator = Validator::make($request->all(), [
            'subject_name' => 'required',
            'subject_status' => 'required',
            'level_name' => 'required',
        ]);

        if($validator->passes())
        {
            $subjectExists = Level::where(['subject_name' => $request->subject_name])->get();
            if(count($subjectExists) == 0){
                $new_subject = new Subject();
                $new_subject->subject_name = $request->subject_name;
                $new_subject->subject_status = $request->subject_status;
                $new_subject->save();
                
                return redirect('/admin/level/')->with(['subject_success' => 'You have successfully added a subject for this level']);
            }

            return redirect('/admin/level/')->with(['subject_error' => 'Duplicates not allowed. The subject already exists']);
        }else{
            return redirect('/admin/level/')->withErrors($validator)->withInput(); 
        }
    }

    public function create_subject_under_level(Request $request){
        $validator = Validator::make($request->all(), [
            'subject_name' => 'required',
            'subject_status' => 'required',
            'level_chosen_id' => 'required'
        ]);

        if($validator->passes())
        {
            $subject = new Subject();
            $subject->level_id = $request->level_chosen_id;
            $subject->subject_name = $request->subject_name;
            $subject->subject_status = $request->subject_status;
            $subject->save();
                           
            return redirect('/admin/level/'.$request->level_chosen_id)->with(['subject_success' => 'You have successfully added a subject for this level']);
          
        }else{
            return redirect('/admin/level/'.$request->level_chosen_id)->withErrors($validator)->withInput(); 
        }
    }

    public function get_subject_under_level(Request $request){
        $validator = Validator::make($request->all(), [
            'subject_id' => 'required',
            'level_chosen_id' => 'required'
        ]);

        if($validator->passes())
        {
            $subject = Subject::where(['id'=> $request->subject_id])->first();
            return $subject;
        }else{
            return redirect('/admin/level/'.$request->level_chosen_id)->withErrors($validator)->withInput(); 
        }    
    }

    public function edit_subject_under_level(Request $request){
         $validator = Validator::make($request->all(), [
            'subject_name' => 'required',
            'subject_status' => 'required',
            'subject_id' => 'required',
            'level_chosen_id' => 'required'
        ]);

        if($validator->passes())
        {
            $subject = Subject::where(['id'=> $request->subject_id])->update([
                'subject_name'=> $request->subject_name,
                'subject_status'=> $request->subject_status
                ]);
                           
            return redirect('/admin/level/'.$request->level_chosen_id)->with(['subject_success' => 'You have successfully edited a subject for this level']);
          
        }else{
            return redirect('/admin/level/'.$request->level_chosen_id)->withErrors($validator)->withInput(); 
        }
    }

    /*************** Topics ****************/

    public function topics_index(){
        return view('/backend/topics');
    }

    public function fetchTopicsData(){
        return Datatables::of(Topic::all())->make(true);
    }

    /*************** Questions ****************/

    public function questions_index(){
        return view('/backend/questions');
    }

    public function fetchQuestionsData(){
        return Datatables::of(Question::all())->make(true);
    }

    /*************** Answers ****************/

    public function answers_index(){
        return view('/backend/answers');
    }

    public function fetchAnswersData(){
        return Datatables::of(Answer::all())->make(true);
    }

    /*************** Videos ****************/

    public function videos_index(){
        return view('/backend/videos');
    }

    public function fetchVideosData(){
        return Datatables::of(Video::all())->make(true);
    }

    /************** Schools ****************/

    public function schools_index(){
        return view('/backend/schools');
    }

    public function fetchSchoolsData(){
        return Datatables::of(Video::all())->make(true);
    }

    /************** Teachers ***************/

    public function teachers_index(){
        return view('backend/teachers/index');
    }

    public function fetchTeachersData(){
        return Datatables::of(User::where(['user_type'=>'Teacher']))->make(true);
    }

    public function create_teacher(Request $request){
        // getting all of the post data

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required',
            'country' => 'required'
        ]);

        $file = $request->file('profile_pic');

        if($validator->passes()){
            //$destinationPath = public_path().'/uploads/team_photos';
            $destinationPath = 'uploads/teachers_photos';

            $teacher_password = str_random(8);

            $teacher = new User();
            $teacher->name = $request->name;
            $teacher->email = $request->email;
            $teacher->contact = $request->phone;
            $teacher->password = bcrypt($teacher_password);
            $teacher->user_type = 'Teacher';
            $teacher->user_status = 'Inactive';
            $teacher->profile_desc =  $request->profile_desc;
            $teacher->country = $request->country;
            $teacher->address = $request->address;

            if(!empty($file)) {
                $filename = $file->getClientOriginalName();
                $upload_success = $file->move($destinationPath, $filename);
                $teacher->profile_pic = $filename;
            }else{
                $teacher->profile_pic = 'avator.jpg';
            }
            $teacher->save();
            //send email to the owner of the account
            mail($request->email,"Credentials for ESPO","Your password is ".$teacher_password);
            return redirect('/admin/teachers')->with(['teacher_success' => 'You have successfully added a teacher']);

        }else{
            return redirect('/admin/teachers')->withErrors($validator)->withInput();
        }
    }
    
    public function getTeacher(Request $request){
        $validator = Validator::make($request->all(), [
            'teacher_id' => 'required'
        ]);

        if($validator->passes())
        {
            $teacher = User::where(['id'=> $request->teacher_id])->first();
            return $teacher;
        }else{
            return redirect('/admin/teachers')->withErrors($validator)->withInput(); 
        }    
    }

    public function edit_teacher(Request $request){
        // getting all of the post data

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$request->teacher_id,
            'phone' => 'required',
            'country' => 'required'
        ]);

        $file = $request->file('profile_pic');

        if($validator->passes()){
             $account_owner = User::where(['id'=>$request->teacher_id])->first();
            //$destinationPath = public_path().'/uploads/team_photos';
            $destinationPath = 'uploads/teachers_photos';

            if(!empty($file))
            {
                $filename = $file->getClientOriginalName();

                $upload_success = $file->move($destinationPath, $filename);

                $teacher = User::where(['id'=>$request->teacher_id])->update([
                    'name'=> $request->name,
                    'email'=> $request->email,
                    'contact'=> $request->phone,
                    'country'=> $request->country,
                    'profile_pic' => $filename,
                    'profile_desc'=> $request->profile_desc,
                    'address'=> $request->address,
                    'user_status'=> $request->user_status
                    ]);

                //send email to the owner of the account
                mail($request->email,"ESPO Account Update","Your account has been updated");               
                
                return redirect('/admin/teachers')->with(['teacher_success' => 'You have successfully updated '.$account_owner->name.' account']);  
            }else{
                $teacher = User::where(['id'=>$request->teacher_id])->update([
                    'name'=> $request->name,
                    'email'=> $request->email,
                    'contact'=> $request->phone,
                    'country'=> $request->country,
                    'profile_desc'=> $request->profile_desc,
                    'address'=> $request->address,
                    'user_status'=> $request->user_status
                    ]); 

                //send email to the owner of the account
                mail($request->email,"ESPO Account Update","Your account has been updated"); 
                  
                return redirect('/admin/teachers')->with(['teacher_success' => 'You have successfully updated '.$account_owner->name.' account']);
            }
            
        }else{
            return redirect('/admin/teachers')->withErrors($validator)->withInput();
        }
    }

    public function teacher_profile($teacher_id){
        $user = User::where(['id'=>$teacher_id])->first();
        $videos = DB::table('videos')
            ->join('posts', 'videos.post_id', '=', 'posts.id')
            ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
            ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
            ->where(['users.id'=>$teacher_id])
            ->select('levels.name', 'videos.id', 'videos.video_file', 'videos.file_type', 'videos.views','videos.video_pricing', 'videos.video_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
            ->distinct('videos')
            ->get();
        $notes = DB::table('notes')
            ->join('posts', 'notes.post_id', '=', 'posts.id')
            ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
            ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
            ->where(['users.id'=>$teacher_id])
            ->select('levels.name', 'notes.id', 'notes.notes_file', 'notes.file_type', 'notes.views', 'notes.notes_pricing', 'notes.notes_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
            ->distinct('notes')
            ->get();   
        return view('backend/teachers/teacher')->with(['videos'=>$videos, 'notes'=>$notes, 'user'=>$user]);
    }

    /***** Administrators *****/
    /************** Teachers ***************/

    public function admins_index(){
        return view('backend/admins/index');
    }

    public function fetchAdminsData(){
        return Datatables::of(User::where(['user_type'=>'Admin']))->make(true);
    }

    public function create_admin(Request $request){
        // getting all of the post data

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required',
            'country' => 'required'
        ]);

        $file = $request->file('profile_pic');

        if($validator->passes()){
            //$destinationPath = public_path().'/uploads/team_photos';
            $destinationPath = 'uploads/admins_photos';

            $admin_password = str_random(8);

            $admin = new User();
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->contact = $request->phone;
            $admin->password = bcrypt($admin_password);
            $admin->user_type = 'Admin';
            $admin->user_status = 'Inactive';
            $admin->profile_desc =  $request->profile_desc;
            $admin->country = $request->country;
            $admin->address = $request->address;

            if(!empty($file)) {
                $filename = $file->getClientOriginalName();
                $upload_success = $file->move($destinationPath, $filename);
                $admin->profile_pic = $filename;
            }else{
                $admin->profile_pic = 'avator.jpg';
            }
            $admin->save();
            //send email to the owner of the account
            mail($request->email,"Credentials for ESPO","Your password is ".$admin_password);
            return redirect('/admins')->with(['admin_success' => 'You have successfully added an Administrators account']);

        }else{
            return redirect('/admins')->withErrors($validator)->withInput();
        }
    }
    
    public function getAdmin(Request $request){
        $validator = Validator::make($request->all(), [
            'admin_id' => 'required'
        ]);

        if($validator->passes())
        {
            $admin = User::where(['id'=> $request->admin_id])->first();
            return $admin;
        }else{
            return redirect('/admins')->withErrors($validator)->withInput(); 
        }    
    }

    public function edit_admin(Request $request){
        // getting all of the post data

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$request->admin_id,
            'phone' => 'required',
            'country' => 'required'
        ]);

        $file = $request->file('profile_pic');

        if($validator->passes()){
             $account_owner = User::where(['id'=>$request->admin_id])->first();
            //$destinationPath = public_path().'/uploads/team_photos';
            $destinationPath = 'uploads/admins_photos';

            if(!empty($file))
            {
                $filename = $file->getClientOriginalName();

                $upload_success = $file->move($destinationPath, $filename);

                $admin = User::where(['id'=>$request->admin_id])->update([
                    'name'=> $request->name,
                    'email'=> $request->email,
                    'contact'=> $request->phone,
                    'country'=> $request->country,
                    'profile_pic' => $filename,
                    'profile_desc'=> $request->profile_desc,
                    'address'=> $request->address,
                    'user_status'=> $request->user_status
                    ]);

                //send email to the owner of the account
                mail($request->email,"ESPO Account Update","Your account has been updated");               
                
                return redirect('/admins')->with(['admin_success' => 'You have successfully updated '.$account_owner->name.' account']);  
            }else{
                $teacher = User::where(['id'=>$request->admin_id])->update([
                    'name'=> $request->name,
                    'email'=> $request->email,
                    'contact'=> $request->phone,
                    'country'=> $request->country,
                    'profile_desc'=> $request->profile_desc,
                    'address'=> $request->address,
                    'user_status'=> $request->user_status
                    ]); 

                //send email to the owner of the account
                mail($request->email,"ESPO Account Update","Your account has been updated"); 
                  
                return redirect('/admins')->with(['admin_success' => 'You have successfully updated '.$account_owner->name.' account']);
            }
            
        }else{
            return redirect('/admins')->withErrors($validator)->withInput();
        }
    }

    /** end ** of ** Administrators **/

    public function getUploadedNotes(Request $request){
        $levels = Level::all();
        $subjects = Subject::where(['subject_status'=>'Active'])->get();
        $topics = Topic::all();
        $subtopics = SubTopic::all();

        if(isset($request->form_search_set_id)){
            $search_level_name = Level::where('id', $request->level_id)->first();    
            $search_subject_name = Subject::where('id', $request->subject_id)->first();
            $search_topic_name = Topic::where('id',$request->topic_id)->first();
            $search_subtopic_name = SubTopic::where('id',$request->subtopic_id)->first();

            $searchLevelName = count($search_level_name) > 0? $search_level_name->name: '';
            $searchSubjectName = count($search_subject_name)>0? $search_subject_name->subject_name: '';
            $searchTopicName = count($search_topic_name) > 0? $search_topic_name->topic_name : '';
            $searchSubtopic = count($search_subtopic_name) > 0 ? $search_subtopic_name->subtopic : '';

            if($request->search_word != ''){
                $notes = DB::table('notes')
                    ->join('posts', 'posts.id', '=', 'notes.post_id')
                    ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
                    ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
                    ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
                    ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
                    ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
                    ->leftJoin('users', 'users.id', '=', 'posts.user_id')
                    ->where(['levels.id'=>$request->level_id,'subjects.id'=>$request->subject_id, 'topics.id'=>$request->topic_id, 'sub_topics.id'=>$request->subtopic_id])
                    ->orWhere('posts.post_title', 'LIKE', '%'.$request->search_word.'%')
                    ->select('users.email','users.name as user','levels.name', 'notes.id', 'notes.notes_file', 'notes.file_type', 'notes.views', 'notes.notes_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
                    ->distinct('notes')
                    ->get();
                $search_key_words = "Your search criteria is '{$searchLevelName}', '{$searchSubjectName}', '{$searchTopicName}', '{$searchSubtopic}', '{$request->search_word}'";    
            }else{
                $notes = DB::table('notes')
                    ->join('posts', 'posts.id', '=', 'notes.post_id')
                    ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
                    ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
                    ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
                    ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
                    ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
                    ->leftJoin('users', 'users.id', '=', 'posts.user_id')
                    ->where(['levels.id'=>$request->level_id,'subjects.id'=>$request->subject_id, 'topics.id'=>$request->topic_id, 'sub_topics.id'=>$request->subtopic_id])
                    //->orWhere('posts.post_title', 'LIKE', '%'.$request->search_word.'%')
                    ->select('users.email','users.name as user','levels.name', 'notes.id', 'notes.notes_file', 'notes.file_type', 'notes.views', 'notes.notes_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
                    ->distinct('notes')
                    ->get();
                
                $search_key_words = "Your search criteria is '{$searchLevelName}', '{$searchSubjectName}', '{$searchTopicName}', '{$searchSubtopic}'"; 
            }
            
        }else{
            $notes = DB::table('notes')
                ->join('posts', 'posts.id', '=', 'notes.post_id')
                ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
                ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
                ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
                ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
                ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
                ->leftJoin('users', 'users.id', '=', 'posts.user_id')
                ->select('users.email','users.name as user','levels.name', 'notes.id', 'notes.notes_file', 'notes.file_type', 'notes.views', 'notes.notes_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
                ->distinct('notes')
                ->get(); 
            $search_key_words = '';   
        }

        return view('backend/school/uploaded_notes')->with([
                'notes'=>$notes,
                'levels'=>$levels,
                'subjects'=>$subjects,
                'topics'=>$topics,
                'subtopics'=>$subtopics,
                'search_key_words'=>$search_key_words
                ]);
    }

    public function getUploadedVideos(){
        $levels = Level::all();
        $subjects = Subject::where(['subject_status'=>'Active'])->get();
        $topics = Topic::all();
        $subtopics = SubTopic::all();

        if(isset($request->form_search_set_id)){
            $search_level_name = Level::where('id', $request->level_id)->first();    
            $search_subject_name = Subject::where('id', $request->subject_id)->first();
            $search_topic_name = Topic::where('id',$request->topic_id)->first();
            $search_subtopic_name = SubTopic::where('id',$request->subtopic_id)->first();

            $searchLevelName = count($search_level_name) > 0? $search_level_name->name: '';
            $searchSubjectName = count($search_subject_name)>0? $search_subject_name->subject_name: '';
            $searchTopicName = count($search_topic_name) > 0? $search_topic_name->topic_name : '';
            $searchSubtopic = count($search_subtopic_name) > 0 ? $search_subtopic_name->subtopic : '';

            if($request->search_word != ''){
                $videos = DB::table('videos')
                    ->join('posts', 'videos.post_id', '=', 'posts.id')
                    ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
                    ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
                    ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
                    ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
                    ->leftJoin('users', 'users.id', '=', 'posts.user_id')
                    ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
                    ->where(['levels.id'=>$request->level_id,'subjects.id'=>$request->subject_id, 'topics.id'=>$request->topic_id, 'sub_topics.id'=>$request->subtopic_id])
                    ->orWhere('posts.post_title', 'LIKE', '%'.$request->search_word.'%')
                    ->select('users.email','users.name as user','levels.name', 'videos.id', 'videos.video_file', 'videos.file_type', 'videos.views', 'videos.video_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
                    ->distinct('videos')
                    ->get();

                $search_key_words = "Your search criteria is '{$searchLevelName}', '{$searchSubjectName}', '{$searchTopicName}', '{$searchSubtopic}', '{$request->search_word}'";    
            }else{
                $videos = DB::table('videos')
                    ->join('posts', 'videos.post_id', '=', 'posts.id')
                    ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
                    ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
                    ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
                    ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
                    ->leftJoin('users', 'users.id', '=', 'posts.user_id')
                    ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
                    ->where(['levels.id'=>$request->level_id,'subjects.id'=>$request->subject_id, 'topics.id'=>$request->topic_id, 'sub_topics.id'=>$request->subtopic_id])
                    ->select('users.email','users.name as user','levels.name', 'videos.id', 'videos.video_file', 'videos.file_type', 'videos.views', 'videos.video_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
                    ->distinct('videos')
                    ->get();
                
                $search_key_words = "Your search criteria is '{$searchLevelName}', '{$searchSubjectName}', '{$searchTopicName}', '{$searchSubtopic}'"; 
            }
            
        }else{
            $videos = DB::table('videos')
                ->join('posts', 'videos.post_id', '=', 'posts.id')
                ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
                ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
                ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
                ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
                ->leftJoin('users', 'users.id', '=', 'posts.user_id')
                ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
                ->select('users.email','users.name as user','levels.name', 'videos.id', 'videos.video_file', 'videos.file_type', 'videos.views', 'videos.video_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
                ->distinct('videos')
                ->get(); 
            $search_key_words = '';   
        }

        return view('backend/school/uploaded_videos')->with([
            'videos'=>$videos,
            'levels'=>$levels,
            'subjects'=>$subjects,
            'topics'=>$topics,
            'subtopics'=>$subtopics,
            'search_key_words'=>$search_key_words
            ]);    
    }

    public function getSubjectsAjax(Request $request){
        $validator = Validator::make($request->all(), [
            'level_id' => 'required',
        ]);

        if($validator->passes())
        {
            $subjects = Subject::where(['level_id'=>$request->level_id])->get();
            return $subjects;    
        }else{
            return redirect()->back();
        }
    }

    /*public function searchNotes(Request $request){
        $levels = Level::all();
        $subjects = Subject::where(['subject_status'=>'Active'])->get();
        $topics = Topic::all();
        $subtopics = SubTopic::all();
        $notes = DB::table('notes')
            ->join('posts', 'posts.id', '=', 'notes.post_id')
            ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('videos', 'posts.id', '=', 'videos.post_id')
            ->leftJoin('questions', 'posts.id', '=', 'questions.post_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
            ->leftJoin('subjects', 'subjects.id', '=', 'teacher_subjects.subject_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
            ->where(['levels.id'=>$request->level_id,'subjects.id'=>$request->subject_id, 'topics.id'=>$request->topic_id, 'sub_topics.id'=>$request->subtopic_id])
            //->orWhere('posts.post_title', 'LIKE', '%'.$request->search_word.'%')
            ->select('users.email','users.name','levels.name', 'notes.id', 'notes.notes_file', 'notes.file_type', 'notes.views', 'notes.notes_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
            ->distinct()
            ->get();
        $search_level_name = Level::where('id', $request->level_id)->first();    
        $search_subject_name = Subject::where('id', $request->subject_id)->first();
        $search_topic_name = Topic::where('id',$request->topic_id)->first();
        $search_subtopic_name = SubTopic::where('id',$request->subtopic_id)->first();

        $searchLevelName = count($search_level_name) > 0? $search_level_name->name: '';
        $searchSubjectName = count($search_subject_name)>0? $search_subject_name->subject_name: '';
        $searchTopicName = count($search_topic_name) > 0? $search_topic_name->topic_name : '';
        $searchSubtopic = count($search_subtopic_name) > 0 ? $search_subtopic_name->subtopic : '';
        $search_key_words = "Your search criteria is '{$searchLevelName}', '{$searchSubjectName}', '{$searchTopicName}', '{$searchSubtopic}'"; 

        return view('backend/school/uploaded_notes')->with([
            'notes'=>$notes,
            'levels'=>$levels,
            'subjects'=>$subjects,
            'topics'=>$topics,
            'subtopics'=>$subtopics,
            'search_key_words'=>$search_key_words
            ]);
    }
*/
    /*** Topics alone ---> as a side menu  <---  ****/
    public function sidebar_topics_index(){
        return view('/backend/school/sidebar_topics');
    }

    public function fetchSidebartopics(){
        return Datatables::of(
                DB::table('topics')
                    ->join('subjects', 'subjects.id', '=', 'topics.subject_id')
                    ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
                    ->leftJoin('sub_topics', 'topics.id', '=', 'sub_topics.topic_id')
                    ->select('topics.*', 'subjects.subject_name')
                    ->distinct()
                    ->get()
                )->make(true); 
    }

    /*** Sub Topics alone ---> as a side menu  <---  ****/
    public function sidebar_subtopics_index(){
        return view('/backend/school/sidebar_subtopics');
    }

    public function fetchSidebarSubtopics(){
        return Datatables::of(
                DB::table('sub_topics')
                    ->join('topics', 'topics.id', '=', 'sub_topics.topic_id')
                    ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
                    ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
                    ->select('sub_topics.*', 'subjects.subject_name', 'topics.topic_name')
                    ->distinct()
                    ->get()
                )->make(true); 
    }

    /**** Students ****/
    public function students_index(){
        return view('backend/students/index');
    }

    public function fetchStudentsData(){
        return Datatables::of(User::where(['user_type'=>'Student']))->make(true);
    }

    
    public function getStudent(Request $request){
        $validator = Validator::make($request->all(), [
            'student_id' => 'required'
        ]);

        if($validator->passes())
        {
            $student = User::where(['id'=> $request->student_id])->first();
            return $student;
        }else{
            return redirect('/admin/students')->withErrors($validator)->withInput(); 
        }    
    }

    public function edit_student(Request $request){
        // // getting all of the post data
        // echo $request->student_id; exit();

        $account_owner = User::where(['id'=>$request->student_id])->first();

        $student = User::where(['id'=>$request->student_id])->update([
            'user_status'=> $request->user_status
            ]);

        //send email to the owner of the account
        if($request->user_status == 'Active'){
            mail($request->email,"ESPO Account Update","Your account has been activated");
        }else{
            mail($request->email,"ESPO Account Update","Your account has been deactivated"); 
        }

        return redirect('/admin/students')->with(['student_success' => 'You have successfully updated '.$account_owner->name.' account']);
    }

    public function student_profile($student_id){
        $user = User::where(['id'=>$student_id])->first();
        $videos = DB::table('videos')
            ->join('posts', 'videos.post_id', '=', 'posts.id')
            ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
            ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
            ->where(['users.id'=>$student_id])
            ->select('levels.name', 'videos.id', 'videos.video_file', 'videos.file_type', 'videos.views','videos.video_pricing', 'videos.video_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
            ->distinct('videos')
            ->get();
        $notes = DB::table('notes')
            ->join('posts', 'notes.post_id', '=', 'posts.id')
            ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'topics.subject_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
            ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
            ->where(['users.id'=>$student_id])
            ->select('levels.name', 'notes.id', 'notes.notes_file', 'notes.file_type', 'notes.views', 'notes.notes_pricing', 'notes.notes_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
            ->distinct('notes')
            ->get();
  
        return view('backend/students/student')->with(['user'=>$user, $notes, $videos]);
    }

}
