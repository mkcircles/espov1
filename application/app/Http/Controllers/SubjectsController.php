<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use App\User;
use App\Level;
use App\Notes;
use App\Subject;
use App\Topic;
use App\SubTopic;
use App\Question;
use App\Answer;
use App\Video;
use App\School;
use App\Post;
use App\Ratings;
use App\Subscriptions;
use Datatables;

class SubjectsController extends Controller
{
    public function subjects_index(){
        $levels = Level::all();
        return view('/backend/school/index',['levels'=>$levels]);
    }

    public function fetch_subjects_data(){
    	return Datatables::of(
    		DB::table('subjects')
                ->join('levels', 'levels.id', '=', 'subjects.level_id')
                ->select('subjects.*', 'levels.name')
                ->get()
    		)->make(true);
    }

    public function getSubjectDetails($subject_id){
    	$subject = Subject::where(['id'=>$subject_id])->first();
        $level = Level::where(['id'=>$subject->level_id])->first();
    	return view('backend/school/subject')->with([
            'subject_id'=>$subject_id,
            'level_id'=>$subject->level_id,
            'subject_name'=>$subject->subject_name,
            'level_name'=>$level->name]);
    }

    public function fetch_subject_topics_data(Request $request){
        $validator = Validator::make($request->all(), [
            'subject_id' => 'required',
            'level_id' => 'required'
        ]);

        if($validator->passes())
        {
            return Datatables::of(
                DB::table('levels')
                    ->join('subjects', 'levels.id', '=', 'subjects.level_id')
                    ->leftJoin('topics', 'subjects.id', '=', 'topics.subject_id')
                    ->where(['levels.id'=>$request->level_id, 'subjects.id'=>$request->subject_id])
                    ->select('topics.*')
                    ->get()
                )->make(true);
        }else{
            return redirect('/admin/subjects')->withErrors($validator)->withInput();
        }  
    }

    public function create_subject_topic(Request $request){
        $validator = Validator::make($request->all(), [
            'subject_id' => 'required',
            'topic_name' => 'required',
            'topic_desc' => 'required'
        ]);

        if($validator->passes())
        {
            $topicExists = Topic::where(['topic_name'=> $request->topic_name, 'subject_id'=>$request->subject_id])->first();

            if(count($topicExists) == 0){
                $new_topic = new Topic();
                $new_topic->subject_id = $request->subject_id;
                $new_topic->topic_name = $request->topic_name;
                $new_topic->description = $request->topic_desc;
                $new_topic->views = 0;
                $new_topic->save();

                return redirect('/admin/subjects/'.$request->subject_id)->with(['topic_success' => 'You have successfully added a topic under this subject']); 
            }else{
                return redirect('/admin/subjects/'.$request->subject_id)->with(['topic_error' => 'Duplicates are not allowed, topic already exists']); 
            }     
        }else{
            return redirect('/admin/subjects/'.$request->subject_id)->withErrors($validator)->withInput();
        }
    }

    public function getSubjectTopic(Request $request){
        $validator = Validator::make($request->all(), [
            'topic_id' => 'required',
        ]);

        if($validator->passes())
        {
            $topic_info = Topic::where(['id'=>$request->topic_id])->first();
            return $topic_info;
        }else{
            return redirect('/admin/subjects')->withErrors($validator)->withInput();
        }
    }

    public function edit_subject_topic(Request $request){
        $validator = Validator::make($request->all(), [
            'topic_id' => 'required',
            'topic_name' => 'required',
            'topic_desc' => 'required',
            'subject_id' => 'required'
        ]);

        if($validator->passes())
        {
            $topicExists = Topic::where(['topic_name'=> $request->topic_name, 'subject_id'=>$request->subject_id])->first();

            if(count($topicExists) == 0){
                $update_topic = Topic::where(['id'=>$request->topic_id])->update([
                        'topic_name'=>$request->topic_name,
                        'description'=>$request->topic_desc
                    ]);

                return redirect('/admin/subjects/'.$request->subject_id)->with(['topic_success' => 'You have successfully edited a topic under this subject']); 
            }else{
                return redirect('/admin/subjects/'.$request->subject_id)->with(['topic_error' => 'Duplicates are not allowed, topic already exists']); 
            }     
        }else{
            return redirect('/admin/subjects/'.$request->subject_id)->withErrors($validator)->withInput();
        }
    }

    public function getSubjectLevelDetails(Request $request){
    	$validator = Validator::make($request->all(), [
            'subject_id' => 'required',
        ]);

        if($validator->passes())
        {
	    	$subject_level_details = DB::table('subjects')
	                ->join('levels', 'levels.id', '=', 'subjects.level_id')
	                ->where(['subjects.id'=>$request->subject_id])
	                ->select('subjects.*', 'levels.name')
	                ->get();
	        return $subject_level_details;
        }else{
        	return redirect('/admin/subjects')->withErrors($validator)->withInput();
        }        
    }

    public function addSubjectLevel(Request $request){
    	$validator = Validator::make($request->all(), [
            'subject_name' => 'required',
            'level_chosen_id' => 'required',
            'subject_status' => 'required'
        ]);

        if($validator->passes())
        {
        	$subjectExists = Subject::where(['subject_name'=> $request->subject_name, 'level_id'=>$request->level_chosen_id])->first();
        	
        	if(count($subjectExists) == 0){

	        	$newSubject = new Subject();
	        	$newSubject->subject_name = $request->subject_name;
	        	$newSubject->subject_status = $request->subject_status;
	        	$newSubject->level_id = $request->level_chosen_id;
	        	$newSubject->save();

        	    return redirect('/admin/subjects')->with(['subject_success' => 'You have successfully added a subject']);
        	}

            $level = Level::where(['id'=>$request->level_chosen_id])->first();
        	return redirect('/admin/subjects')->with(['subject_error' => 'The subject '.$request->subject_name.' already exists under '.$level->name]);
        }else{
        	return redirect('/admin/subjects')->withErrors($validator)->withInput();
        }
    }

    public function editSubjectLevel(Request $request){
    	$validator = Validator::make($request->all(), [
            'subject_id' => 'required',
            'subject_name' => 'required',
            'level_chosen_id' => 'required',
            'subject_status' => 'required'
        ]);

        if($validator->passes())
        {
        	$subjectExists = Subject::where(['subject_name'=> $request->subject_name, 'level_id'=>$request->level_chosen_id])->first();
        	if(count($subjectExists) == 0){
	        	$editSubject = Subject::where(['id'=>$request->subject_id])->update([
	 				'subject_name'=> $request->subject_name,
	 				'level_id'=> $request->level_chosen_id,
	 				'subject_status'=> $request->subject_status
	        		]);
        	    return redirect('/admin/subjects')->with(['subject_success' => 'You have successfully edited the subject']);
        	}
            $level = Level::where(['id'=>$request->level_chosen_id])->first();
        	return redirect('/admin/subjects')->with(['subject_error' => 'The subject '.$request->subject_name.' already exists under '.$level->name]);
        }else{
        	return redirect('/admin/subjects')->withErrors($validator)->withInput();
        }
    }

    public function getSubjectTopics($topic_id){
        $topic = Topic::where(['id'=> $topic_id])->first();
        $subject = DB::table('subjects')
                 ->join('topics', 'subjects.id', '=', 'topics.subject_id')
                 ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
                 ->where(['topics.id'=>$topic_id])
                 ->first();        

        return view('backend/school/topic')->with([
            'topic_id'=>$topic_id,
            'topic_name'=>$topic->topic_name,
            'subject_name'=>$subject->subject_name,
            'level_name'=>$subject->name,
            'subject_id'=>$subject->id
            ]);
    }

    public function fetchSubjectTopicsData(Request $request){
        $validator = Validator::make($request->all(), [
            'topic_id' => 'required'
        ]);

        if($validator->passes())
        {
            return Datatables::of(
                SubTopic::where(['topic_id'=>$request->topic_id])->get()
                )->make(true);
        }else{
            return redirect('/admin/levels')->withErrors($validator)->withInput(); 
        }
    }

    public function create_topic_subtopic(Request $request){
        $validator = Validator::make($request->all(), [
            'topic_id' => 'required',
            'subtopic_name' => 'required',
            'subtopic_desc' => 'required'
        ]);

        if($validator->passes())
        {
            $subtopicExists = SubTopic::where(['subtopic'=> $request->subtopic_name, 'topic_id'=>$request->topic_id])->first();

            if(count($subtopicExists) == 0){
                $new_subtopic = new SubTopic();
                $new_subtopic->topic_id = $request->topic_id;
                $new_subtopic->subtopic = $request->subtopic_name;
                $new_subtopic->desc = $request->subtopic_desc;
                $new_subtopic->subtopic_views = 0;
                $new_subtopic->save();

                return redirect('/admin/topic/'.$request->topic_id)->with(['subtopic_success' => 'You have successfully added a sub topic under this topic']); 
            }else{
                return redirect('/admin/topic/'.$request->topic_id)->with(['subtopic_error' => 'Duplicates are not allowed, sub topic already exists']); 
            }     
        }else{
            return redirect('/admin/topic/'.$request->topic_id)->withErrors($validator)->withInput();
        }
    }

    public function getTopicSubtopic(Request $request){
        $validator = Validator::make($request->all(), [
            'subtopic_id' => 'required',
        ]);

        if($validator->passes())
        {
            $subtopic_info = SubTopic::where(['id'=>$request->subtopic_id])->first();
            return $subtopic_info;
        }else{
            return redirect('/admin/subjects')->withErrors($validator)->withInput();
        }
    }

    public function edit_topic_subtopic(Request $request){
        $validator = Validator::make($request->all(), [
            'subtopic_id' => 'required',
            'subtopic_name' => 'required',
            'subtopic_desc' => 'required',
            'topic_id' => 'required'
        ]);

        if($validator->passes())
        {
            $subtopicExists = SubTopic::where(['subtopic'=> $request->subtopic_name, 'topic_id'=>$request->topic_id])->first();

            if(count($subtopicExists) == 0){
                $update_topic = SubTopic::where(['id'=>$request->subtopic_id])->update([
                        'subtopic'=>$request->subtopic_name,
                        'desc'=>$request->subtopic_desc
                    ]);

                return redirect('/admin/topic/'.$request->topic_id)->with(['subtopic_success' => 'You have successfully edited a sub topic under this subject']); 
            }else{
                return redirect('/admin/topic/'.$request->topic_id)->with(['subtopic_error' => 'Duplicates are not allowed, sub topic already exists']); 
            }     
        }else{
            return redirect('/admin/topic/'.$request->topic_id)->withErrors($validator)->withInput();
        }
    }

    public function getSubtopicsVideosAndNotes($subtopic_id){
        $subject = DB::table('subjects')
                 ->join('topics', 'subjects.id', '=', 'topics.subject_id')
                 ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
                 ->leftJoin('sub_topics', 'topics.id', '=', 'sub_topics.topic_id')
                 ->where(['sub_topics.id'=>$subtopic_id])
                 ->first(); 
        $videos = DB::table('videos')
            ->join('posts', 'videos.post_id', '=', 'posts.id')
            ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
            ->leftJoin('subjects', 'subjects.id', '=', 'teacher_subjects.subject_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
            ->where(['sub_topics.id'=>$subtopic_id])
            //->where(['users.id'=>2])
            ->select('users.email','users.name','levels.name', 'videos.id', 'videos.video_file', 'videos.file_type', 'videos.views', 'videos.video_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
            ->get();
        $notes = DB::table('notes')
            ->join('posts', 'posts.id', '=', 'notes.post_id')
            ->leftJoin('sub_topics', 'sub_topics.id', '=', 'posts.subtopic_id')
            ->leftJoin('topics', 'topics.id', '=', 'sub_topics.topic_id')
            ->leftJoin('videos', 'posts.id', '=', 'videos.post_id')
            ->leftJoin('questions', 'posts.id', '=', 'questions.post_id')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('teacher_subjects', 'teacher_subjects.user_id', '=', 'users.id')
            ->leftJoin('subjects', 'subjects.id', '=', 'teacher_subjects.subject_id')
            ->leftJoin('levels', 'levels.id', '=', 'subjects.level_id')
            ->leftJoin('subscriptions', 'subscriptions.post_id', '=', 'posts.id')
            ->where(['sub_topics.id'=>$subtopic_id])
            //->where(['users.id'=>2])
            ->select('users.email','users.name','levels.name', 'notes.id', 'notes.notes_file', 'notes.file_type', 'notes.views', 'notes.notes_status','subjects.subject_name', 'topics.topic_name', 'sub_topics.subtopic', 'posts.post_title', 'posts.cover_photo', DB::raw('(select count(*) from subscriptions where subscriptions.post_id=posts.id) as student_subscriptions') )
            ->get();    
        return view('backend/school/content')->with([
            'videos'=>$videos,
            'notes'=>$notes,
            'subject_name'=>$subject->subject_name,
            'level_name'=>$subject->name,
            'subject_id'=>$subject->id,
            'topic_name'=>$subject->topic_name,
            'subtopic_name'=>$subject->subtopic
            ]);
    }
}
