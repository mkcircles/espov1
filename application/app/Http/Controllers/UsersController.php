<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use URL;
use Validator;
use App\User;

class UsersController extends Controller
{

    public function login(){
    	return view('login');
    }

    public function authenticate(Request $request){
    	$this->validate($request, [
            'email' => 'required',
            'password' => 'required'
            ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            //check if administrator or teacher...
            $user = User::where(['id'=>Auth::id()])->first();
            if($user->user_type == 'Admin'){
                return redirect('/admin')->with(['success' => 'Logged in successfully']);
            }else if($user->user_type == 'Teacher'){
                return redirect('/teacher')->with(['success' => 'Logged in successfully']);
            }else{
                //
            }
           
        }

        return redirect()->back()->with(['user_login_errors'=>'Wrong email or password']);
    }

    public function logout(){
    	Auth::logout();
        return redirect('/');
    }
    
    
    public function view_profile(){
        $user = User::find(Auth::id());
        return view('profile')->with(['user' => $user]);
    }

    public function change_password(Request $request){
    	 $this->validate($request, [
            'oldpwd' => 'required',
            'newpwd' => 'required',
            'confirm_newpwd' => 'required'
        ]);
        
        $user = User::find(Auth::id());
        if(Auth::attempt(['email'=>$user->email, 'password'=>$request->oldpwd])){
            //check if newpwd == confirm_newpwd...
            if($request->newpwd == $request->confirm_newpwd){
                //update the password...
                $updatePassword = $user->update(['password'=>bcrypt($request->newpwd)]);
                return redirect('/user/profile')->with(['profile_success'=>'Your password has been changed successfully']);
            }else{
                return redirect('/user/profile')->with(['profile_error'=>'The new passwords should match']);
            }
        }         
        else{
            return redirect('/user/profile')->with(['profile_error'=>'The old password is wrong']);
        }
    }

    public function forgot_password(){
        return view('password_forgot');
    }

    public function send_email(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if($validator->passes()){
            $user = User::where(['email'=>$request->email])->first();
            if(count($user) > 0){
                mail($request->email,"Forgot Password","Please click on the link to reset your password. ".URL::to('/reset/view/'.$user->remember_token));
                return redirect('/')->with(['email_sent_success'=>'A link has been sent to your email, please check and reset password']);   
            }
            return redirect()->back()->with(['email_not_found_error'=>'This email does not exist']);
        }

        return redirect()->back()->withErrors($validator)->withInput();
    }

    public function reset_view($remembertoken){
        if(is_null($remembertoken)){
            return redirect('/')->with(['expired_token'=>'Please click on forgot password to reset your password']);
        }
        return view('password_reset')->with(['remembertoken'=>$remembertoken]);
    }

    public function reset_password(Request $request){
        $validator = Validator::make($request->all(), [
            'newpwd' => 'required',
            'confirm_newpwd' => 'required',
            'remembertoken' => 'required'
        ]);

        if($validator->passes()){
            $user = User::where(['remember_token'=>$request->remembertoken])->first();
            if(count($user) > 0){
                //check if newpwd == confirm_newpwd...
                if($request->newpwd == $request->confirm_newpwd){
                    //update the password...
                    $updatePassword = $user->update(['password'=>bcrypt($request->newpwd)]);
                    return redirect('/')->with(['success'=>'Your password has been reset successfully']);
                }else{
                    return redirect()->back()->with(['pwd_match_error'=>'The passwords should match']);
                }
            }

            return redirect('/')->with(['expired_token'=>'Please click on forgot password to reset your password']);
        }

        return redirect()->back()->withErrors($validator)->withInput();
    }

}
