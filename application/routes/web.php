<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/************ FRONT END ***************/
Route::get('/', 'FrontController@front');
Route::get('/user-login', function () {    return view('user_login'); });
Route::get('/courses', 'FrontController@getCourses');
Route::get('/courses/{level_id}', 'FrontController@getLevelCourses');
Route::get('/subject/{id}', 'FrontController@getSubjectCourses');
Route::get('/notes', 'FrontController@getNotes');
Route::get('/notes/{id}', 'FrontController@getNotesDetails');
Route::get('/videos', 'FrontController@getVideos');
Route::get('/videos/{id}', 'FrontController@getVideoDetails');


// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/login', 'UsersController@login');
Route::get('/forgot/password', 'UsersController@forgot_password');
Route::post('/users/authenticate', 'UsersController@authenticate');
Route::get('/logout', 'UsersController@logout');
Route::get('/user/profile', 'UsersController@view_profile');
Route::post('/user/password/change', 'UsersController@change_password');
Route::post('/send/email', 'UsersController@send_email');

Route::get('/admin', function () {
    return view('backend/index');
});

/********* Levels ***********/

Route::get('/admin/levels', 'AdminController@levels_index');
Route::get('/admin/levels/data/fetch', 'AdminController@fetchLevelsData');
Route::post('/admin/levels/create', 'AdminController@create_level');
Route::get('/level/get', 'AdminController@getLevel');
Route::post('/admin/levels/edit', 'AdminController@edit_level');
Route::post('/admin/levels/delete', 'AdminController@delete_level');
Route::get('/admin/level/{level_id}', 'AdminController@getLevelDetails');

/********* Subjects *********/

Route::get('/admin/level/subjects/data/fetch', 'AdminController@fetch_levels_details_data');
Route::post('/admin/level/subjects/create', 'AdminController@create_subject_under_level');
Route::get('/level/subject/get', 'AdminController@get_subject_under_level');
Route::post('/admin/level/subjects/edit', 'AdminController@edit_subject_under_level');

//Subject Topics

Route::get('/admin/subjects', 'SubjectsController@subjects_index');
Route::get('/admin/subjects/data/fetch', 'SubjectsController@fetch_subjects_data');
Route::get('/subject/level/get', 'SubjectsController@getSubjectLevelDetails');
Route::post('/subjects/level/add', 'SubjectsController@addSubjectLevel');
Route::post('/subjects/level/edit', 'SubjectsController@editSubjectLevel');
Route::get('/admin/subjects/{subject_id}', 'SubjectsController@getSubjectDetails');
Route::get('/subject/topics/data/fetch', 'SubjectsController@fetch_subject_topics_data');
Route::post('/subject/topics/create', 'SubjectsController@create_subject_topic');
Route::get('/subject/topic/get', 'SubjectsController@getSubjectTopic');
Route::post('/subject/topics/edit', 'SubjectsController@edit_subject_topic');
Route::get('/admin/topic/{topic_id}', 'SubjectsController@getSubjectTopics');
Route::get('/topic/subtopic/data/fetch', 'SubjectsController@fetchSubjectTopicsData');
Route::post('/topic/subtopic/create', 'SubjectsController@create_topic_subtopic');
Route::get('/topic/subtopic/get', 'SubjectsController@getTopicSubtopic');
Route::post('/subject/topics/subtopics/edit', 'SubjectsController@edit_topic_subtopic');
Route::get('/admin/subtopic/content/{subtopic_id}', 'SubjectsController@getSubtopicsVideosAndNotes');

//Subject uploads...
Route::get('/admin/uploaded/notes', 'AdminController@getUploadedNotes');
Route::get('/admin/uploaded/videos', 'AdminController@getUploadedVideos');
Route::get('/admin/uploaded/subjects/get', 'AdminController@getSubjectsAjax');

//admin topics...
Route::get('/admin/topics', 'AdminController@sidebar_topics_index');
Route::get('/admin/topic/data/fetch', 'AdminController@fetchSidebarTopics');

//admin subtopics...
Route::get('/admin/subtopics', 'AdminController@sidebar_subtopics_index');
Route::get('/admin/subtopic/data/fetch', 'AdminController@fetchSidebarSubtopics');


/** Maurice's **/
// Route::get('/admin/topic/{topic_id}', function () {
//     return view('backend/school/topic');
// });

/********* Teachers *********/
Route::get('/admin/teachers', 'AdminController@teachers_index');
Route::get('/admin/teachers/data/fetch', 'AdminController@fetchTeachersData');
Route::get('/admin/teachers/{teacher_id}', 'AdminController@teacher_profile');
Route::post('/admin/teachers/create', 'AdminController@create_teacher');
Route::get('/teacher/get', 'AdminController@getTeacher');
Route::post('/admin/teachers/edit', 'AdminController@edit_teacher');

/********* Students Admin *********/
Route::get('/admin/students', 'AdminController@students_index');
Route::get('/admin/students/data/fetch', 'AdminController@fetchStudentsData');
Route::get('/admin/students/{student_id}', 'AdminController@student_profile');
Route::get('/student/get', 'AdminController@getStudent');
Route::post('/admin/students/edit', 'AdminController@edit_student');
Route::get('/notes/data/fetch', 'AdminController@fetchStudentNotes');
Route::get('/videos/data/fetch', 'AdminController@fetchStudentVideos');
Route::get('/student/notes/get', 'AdminController@getStudentNotesData');
Route::get('/student/videos/get', 'AdminController@getStudentVideosData');

/******* Students Account ***********/
Route::post('/admin/students/login', 'StudentsController@login');
Route::post('/admin/students/register', 'StudentsController@register');

/********* Administrators *********/
Route::get('/admins', 'AdminController@admins_index');
Route::get('/admins/data/fetch', 'AdminController@fetchAdminsData');
Route::post('/admin/create', 'AdminController@create_admin');
Route::get('/admin/get', 'AdminController@getAdmin');
Route::post('/admin/edit', 'AdminController@edit_admin');


/**********************************************************************
** TEACHER MODULE
 *********************************************************************/

// Route::get('/teacher', function () { return view('teacher/index'); });
// Route::get('/teacher/subjects', function () { return view('teacher/subjects'); });
Route::get('/teacher/subjects', 'TeachersController@getTeacherSubjects');
Route::get('/teacher/subjects/data/fetch', 'TeachersController@fetchTeacherSubjectsData');
Route::get('/teacher', 'TeachersController@teacher_index');
Route::get('/teacher/profile', 'TeachersController@teachers_profile');
Route::get('/teachers/data/fetch', 'TeachersController@fetchTeacherData');
Route::get('/teacher/notes', 'TeachersController@getNotes');
Route::get('/teacher/subject/topics/get', 'TeachersController@getTeacherSubjectTopics');
Route::get('/teacher/subject/topics/subtopics/get', 'TeachersController@getTeacherSubjectTopicSubtopics');
Route::post('/teacher/notes/post', 'TeachersController@postNotes');
Route::get('/teacher/videos', 'TeachersController@getVideos');
Route::get('/notes/data/get', 'TeachersController@fetchNotesData');
Route::get('/video/get', 'TeachersController@fetchVideoData');
Route::post('/teacher/videos/post', 'TeachersController@postVideos');

/************************************************************************
** PAYMENT MODULE
************************************************************************/
Route::get('/post/pay', 'PaymentsController@makePayment');