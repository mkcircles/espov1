@extends('backend.layouts.master')

@section('styles')
    <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css"/>
@endsection

@section('content')
    <div class="page-container">
        @include('backend.includes.sidebar')

        <div class="main-content">
            @include('backend.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/admin"><i class="fa-home"></i>Home</a></li>
                <li><a href="/admin/subjects"><i class="fa-home"></i>Subjects</a></li>
                <li class="active"><strong>Subject Name</strong></li>
            </ol>

            <h3>{{ $level_name }} Level - {{ $subject_name }} </h3>
            <hr/>

            <div class="row">
                <div class="col-sm-3">

                    <div class="tile-stats tile-primary">
                        <div class="icon"><i class="entypo-docs"></i></div>
                        <div class="num" data-start="0" data-end="10" data-postfix=" " data-duration="1500"
                             data-delay="0">-, 83
                        </div>

                        <h3>Sub Topics</h3>
                    </div>

                </div>

                <div class="col-sm-3">

                    <div class="tile-stats tile-red">
                        <div class="icon"><i class="entypo-users"></i></div>
                        <div class="num" data-start="0" data-end="120" data-postfix=" " data-duration="1500"
                             data-delay="0"> 120
                        </div>
                        <h3>Topic Followers</h3>
                    </div>

                </div>

                <div class="col-sm-3">
                    <div class="tile-stats tile-aqua">
                        <div class="icon"><i class="entypo-user"></i></div>
                        <div class="num">4</div>
                        <h3>Teachers</h3>
                    </div>

                </div>

                <div class="col-sm-3">
                    <div class="tile-stats tile-blue">
                        <div class="icon"><i class="entypo-suitcase"></i></div>
                        <div class="num" data-start="0" data-end="400" data-postfix=" " data-duration="1700"
                             data-delay="0"> 400
                        </div>
                        <h3>Topic Views</h3>
                    </div>

                </div>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger" style='margin-bottom:10px'>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('topic_success'))
                 <div class="alert alert-success" role="alert">
                         {{ session('topic_success') }}
                 </div>
            @endif

            @if(session('topic_error'))
               <div class="alert alert-danger" role="alert">
                         {{ session('topic_error') }}
                 </div>
            @endif

            <a href="javascript:;" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});">
                <button class="btn btn-info pull-right btn-icon icon-left" type="button">Add Topic <i
                            class="entypo-plus"></i></button>
            </a>


            <h4><strong>Registered Topics</strong></h4>

            <!-- <div class="dataTables_wrapper no-footer">
                <table class="table table-bordered table-striped datatable dataTable no-footer" id="levels_datatable"
                       role="grid" aria-describedby="table-2_info" style="font-size: 12px">
                    <thead>
                    <tr>
                        <td>Topic Name</td>
                        <td>Description</td>
                        <td>Status</td>
                        <td>Created On</td>
                        <td>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr role="row" class="odd">
                        <td class="sorting_1">Measurement Of Physical Quantities</td>
                        <td>This topic covers "Measurement Of Physical Properties" of O Level Physics.</td>
                        <td>Active</td>
                        <td>2016-02-19</td>
                        <td>
                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                            <a href="/admin/topic/4" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-docs"></i> Sub Topics</a>
                        </td>
                    </tr>
                    <tr role="row" class="even">
                        <td class="sorting_1">Speed, Velocity And Acceleration</td>
                        <td>This topic covers "Speed, Velocity and Acceleration" of O Level Physics.</td>
                        <td>Active</td>
                        <td>2016-02-19</td>

                        <td>
                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                            <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-docs"></i> Sub Topics</a>
                        </td>
                    </tr>
                    <tr role="row" class="odd">
                        <td class="sorting_1">Forces & Turning Effect Of Forces</td>
                        <td>This topic covers "Forces & Turning Effect Of Forces" of O Level Physics.</td>
                        <td>Active</td>
                        <td>2016-02-19</td>

                        <td>
                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                            <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-docs"></i> Sub Topics</a>
                        </td>
                    </tr>
                    <tr role="row" class="even">
                        <td class="sorting_1">Mass, Weight And Density</td>
                        <td>This topic covers "Mass, Weight and Density" of O Level Physics.</td>
                        <td>Active</td>
                        <td>2016-02-19</td>
                        <td>
                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                            <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-docs"></i>Sub Topics</a>
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div> -->
            <div class="dataTables_wrapper no-footer">
                <table class="table table-bordered table-striped datatable dataTable no-footer" id="subject_topics_datatable"
                       role="grid" aria-describedby="table-2_info" style="font-size: 12px">
                    <thead>
                    <tr>
                        <td>Topic Name</td>
                        <td>Description</td>
                        <td>Views</td>
                        <td>Created On</td>
                        <td>Actions</td>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>

    </div>
<div class="modal fade" id="modal-6">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Register New Topic for <strong>{{ $subject_name }}</strong></h4>
            </div>

            <form action="/subject/topics/create" method="post">

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Subject</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Level name"
                                       value="{{ $subject_name }}" readonly name="subject_name">
                                <input type="hidden" class="form-control" id="field-1" value="1" readonly
                                       name="subject_id">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Topic</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Topic Name" name="topic_name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Topic Description</label>
                                <textarea class="form-control" id="field-1" name="topic_desc" placeholder="Topic description"></textarea>
                            </div>
                            <input type='hidden' name='subject_id' value='{{ $subject_id }}'>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        </div>


                </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-info">Save changes</button>
        </div>

        </form>
    </div>
</div>
</div>

<!-- edit modal -->
<div class="modal fade" id="edit_modal-6">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Topic for <strong>{{ $subject_name }}</strong></h4>
            </div>

            <form action="/subject/topics/edit" method="post">

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Subject</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Level name"
                                       value="{{ $subject_name }}" readonly name="subject_name">
                                <input type="hidden" class="form-control" id="field-1" value="1" readonly
                                       name="subject_id">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Topic</label>
                                <input type="text" class="form-control" id="topic_name" placeholder="Topic Name" name="topic_name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Topic Description</label>
                                <textarea class="form-control" id="topic_desc" name="topic_desc" placeholder="Topic description"></textarea>
                            </div>
                            <input type='hidden' name='subject_id' value='{{ $subject_id }}'>
                            <input type='hidden' name='topic_id' id='topic_id'>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        </div>


                </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-info">Save changes</button>
        </div>

        </form>
    </div>
</div>
</div>
@endsection

@section('scripts')
  <!-- DataTables -->
  <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

  <!-- datatables btns -->
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  
  <!-- App scripts -->
  <script>
  $(function() {

      $('#subject_topics_datatable').DataTable({
          processing: true,
          serverSide: true,
          dom: 'Bflrtip',
          buttons: [
              {
                  extend: 'copy',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'excel',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'pdf',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'print',
                  exportOptions: {
                      columns: [0,1]
                  }
              }
          ],
          ajax: { 
            url:'{{ URL::asset('/subject/topics/data/fetch') }}',
            type: "GET",
            data: { "subject_id" : {{ $subject_id }}, "level_id" : {{ $level_id }} }
          },
          columns: [
              { data: 'topic_name', name: 'topic_name' },
              { data: 'description', name: 'description' },
              { data: 'views', name: 'views' },
              { data: 'created_at', name: 'created_at' },
              { data: 'id', name: 'id',
                "targets": 0,
                "render": function ( data, type, row ) {
                            if(data != ""){
                              var action_btns = '<a id="editSubjectTopic" rowId="'+data+'" class="btn btn-default btn-sm btn-icon icon-left" href="#"><i class="entypo-pencil"></i>Edit</a>';
                              action_btns += '&nbsp;<a id="deleteSubjectTopic" delete_rowId="'+data+'" class="btn btn-danger btn-sm btn-icon icon-left" href="#"><i class="entypo-cancel"></i>Delete</a>';
                              action_btns += '&nbsp;<a id="profileSubjectTopic" profile_rowId="'+data+'" class="btn btn-info btn-sm btn-icon icon-left" href="/admin/topic/'+data+'"><i class="entypo-info"></i>View Sub Topics</a>';
                              return action_btns;
                            }
                            return '';
                          }
              },
          ]
      });

      //edit data...
      $('#subject_topics_datatable tbody').on('click', '#editSubjectTopic',function(e){
        e.preventDefault();
        var topic_id = $(this).attr('rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/subject/topic/get',
          data: {topic_id: topic_id},
          success: function( data ) {
            console.log(data);
            $("#topic_name").val(data.topic_name);
            $("#topic_id").val(data.id);
            $("#topic_desc").val(data.description);

            $("#edit_modal-6").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });

      //delete data...
      $('#subject_topics_datatable tbody').on('click', '#deleteSubjectTopic',function(e){
        e.preventDefault();
        var topic_id = $(this).attr('delete_rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/level/subject/get',
          data: {topic_id: topic_id},
          success: function( data ) {
            $("#delete_level_id").val(data.id);
            $("#level_to_delete").text(data.name);

            $("#delete_level").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });
         
  });
  </script>
@endsection
