@extends('backend.layouts.master')


@section('content')
    <div class="page-container">
        @include('backend.includes.sidebar')

        <div class="main-content">
            @include('backend.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/admin"><i class="fa-home"></i>Home</a></li>
                <li><a href="/admin/subjects"><i class="fa-home"></i>Subjects</a></li>
                <li><a href="/admin/subjects/2"><i class="fa-home"></i>Subject Name</a></li>
                <li class="active"><strong>Topic Name</strong></li>
                <li class="active"><strong>Content</strong></li>
            </ol>
            
            <form action="/admin/uploaded/notes" method="get">  
              
              <div class="input-group custom-search-form">
                  <input type="text" class="form-control" name="search_word" placeholder="Search...">
                  <span class="input-group-btn">
                      <button class="btn btn-default-sm" type="submit">
                          <i class="fa fa-search"><!--<span class="hiddenGrammarError" pre="" data-mce-bogus="1"-->i>
                      </button>
                  </span>
              </div>
              <a href="#" id="filter-link">Filter</a>
              <div class="row" id="filters">
                <div class="col-md-6">
                  <div class="form-group">
                    @if(count($levels) > 0)
                        <label for="field-1" class="control-label">Level</label>
                        <select class="form-control" id="levels" name="level_id" required>
                            <option selected disabled hidden>Choose Level</option>
                            @foreach($levels as $level)
                                <option class='all_levels' id='{{ $level->id }}' value="{{ $level->id }}">{{ $level->name }}</option>
                            @endforeach
                        </select>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Subject</label>
                        <select class="form-control" id="subjects" name="subject_id" required>
                            <option selected disabled hidden>Choose Subject</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Topic</label>
                        <select class="form-control" id="topics" name="topic_id" required>
                            <option selected disabled hidden>Choose Topic</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Sub Topic</label>
                        <select class="form-control" id="subtopics" name="subtopic_id" required>
                            <option selected disabled hidden>Choose Sub Topic</option>
                        </select>
                    </div>
                </div>
              </div>  
              <input type='hidden' name='_token' value='{{ csrf_token() }}'>
              <input type='hidden' name="form_search_set_id" value="1">
            </form>
            <hr>

            @if( isset($search_key_words) )
            <p>{{ $search_key_words }}</p>
            @endif


            @if (count($errors) > 0)
              <div class="alert alert-danger" style='margin-bottom:10px'>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            @if(session('notes_success'))
              <div class="alert alert-success" role="alert">
                {{ session('notes_success') }}
              </div>
            @endif

            @if(session('notes_error'))
              <div class="alert alert-danger" role="alert">
                {{ session('notes_error') }}
              </div>
            @endif
            @if(session('video_success'))
              <div class="alert alert-success" role="alert">
                {{ session('video_success') }}
              </div>
            @endif

            @if(session('video_error'))
              <div class="alert alert-danger" role="alert">
                {{ session('video_error') }}
              </div>
            @endif

                <div class="row">

                  @if(count($notes) > 0)
                  @foreach($notes as $note)
                      <div class="col-sm-3">

                          <div class="tile-stats tile-white-cyan">
                            <img src="{{ URL::asset( '/uploads/files/' . $note->cover_photo) }}" class="cover_photo">
                              <h4>{{ $note->subject_name }}</h4>
                              <p>{{ $note->topic_name }}</p>
                              Post Title:<p>{{ $note->post_title }}</p>
                              Posted by:{{ $note->user }}
                              <hr/>
                              <div class="pull-left">
                                  Views: {{ $note->views > 0 ? $note->views:0 }}
                              </div>
                              <div class="panel-options pull-right">
                                  
                                  <a href="test2.pdf" data-toggle="modal" class="view_notes" id="{{ $note->id }}" data-target="#sample-modal-dialog-1" title="View" class="bg"><i class="entypo-eye"></i> view</a>
                                  <a href="{{ URL::asset( '/uploads/files/' . $note->notes_file)  }}" download="{{$note->notes_file}}">Download</a>
                                  <a href="#" title="Delete" data-rel="close" class="delete_notes" id="{{ $note->id }}"><i class="entypo-cancel"></i> Delete</a>
                              </div>
                          </div>

                      </div>
                  @endforeach
                  @else
                      <div class="col-sm-3">
                          <b>No results found</b>
                      </div>        
                  @endif
                </div>
            
        </div>

    </div>
        <!-- viewing pdf -->
<div class="modal fade" id="view_pdf_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Viewing pdf for <strong id="notes_subject_name"></strong></h4>
            </div>

            <form action="/teacher/notes/post" method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="row">
                        <div id="example1"></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Delete Pdf</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  <script src="{{ asset('assets/backend/js/pdfobject.js') }}"></script>
  <!-- App scripts -->
  <script>
  $(function() {

    //initial disable all the subjects, topics and subtopics...
    $('#topics').prop('disabled', true);
    $('#subtopics').prop('disabled', true);
    $('#subjects').prop('disabled', true);

    $('#levels').change(function(){
      var level_id = $(this).children(":selected").attr("id");
      //alert(subject_id);
      //ajax call for topics...
      $.ajax({
        type: "GET",
        url: '/admin/uploaded/subjects/get',
        data: {level_id: level_id},
        success: function( data ) {
          //check if data is not empty...
          //first clear all the options
          $('#subjects').find('option').remove().end().append('<option selected disabled hidden>Choose Subject</option>');
          console.log(data);
          $.each(data, function(i, value) {
              $('#subjects').append($('<option>').text(value.subject_name).attr('value', value.id).attr('id', value.id));
              $('#subjects').prop('disabled', false);
          });
        },
        error: function(data){
          alert('failed to fetch subjects, could not parse subjects');
        }
      });
    });

    $('#filters').on('change', '#subjects',function(){
            var subject_id = $(this).children(":selected").attr("id");
            //alert(subject_id);
            //ajax call for topics...
            $.ajax({
              type: "GET",
              url: '/teacher/subject/topics/get',
              data: {subject_id: subject_id},
              success: function( data ) {
                //check if data is not empty...
                //first clear all the options
                $('#topics').find('option').remove().end().append('<option selected disabled hidden>Choose Topic</option>');
          
                console.log(data);
                $.each(data, function(i, value) {
                    $('#topics').append($('<option>').text(value.topic_name).attr('value', value.id).attr('id', value.id));
                    $('#topics').prop('disabled', false);
                });
              },
              error: function(data){
                alert('failed to fetch topics, please ensure you have registered topics');
              }
            });
        });

        $('#filters').on('change','#topics',function(){
            var topic_id = $(this).children(":selected").attr("id");
            //alert(topic_id);
            //ajax call for topics...
            $.ajax({
              type: "GET",
              url: '/teacher/subject/topics/subtopics/get',
              data: {topic_id: topic_id},
              success: function( data ) {
                //check if data is not empty...
                //first clear all the options
                $('#subtopics').find('option').remove().end().append('<option selected disabled hidden>Choose SubTopic</option>');
          
                console.log(data);
                $.each(data, function(i, value) {
                    $('#subtopics').append($('<option>').text(value.subtopic).attr('value', value.id).attr('id', value.id));
                    $('#subtopics').prop('disabled', false);
                });
              },
              error: function(data){
                alert('failed to fetch topics, please ensure you have registered topics');
              }
            });
        });

        //edit data...
        $('.view_notes').click(function(e){
            e.preventDefault();
            var notes_id = $(this).attr('id');
            if(notes_id == 0){
                alert('No notes exist, please create notes under this subject');
                //call modal for creating notes...

            }else{
                var data;
                $.ajax({
                  type: "GET",
                  url: '/notes/data/get',
                  data: {notes_id: notes_id},
                  success: function( data ) {
                    console.log(data);
                    /*$("#fileviewer").attr({ src: "/uploads/files/"+data[0].notes_file });*/
                    // $("#level_name").val(data.name);
                    // $("#level_id").val(data.id);
                    $("#notes_subject_name").text(data[0].subject_name+'(subject) - '+ data[0].topic_name +'(topic) - '+ data[0].subtopic +'(sub-topic)');

                    // PDFObject.embed("{{ URL::asset( '/uploads/files/Breakdown.pdf') }}", "#example1");
                    //"{{ URL::asset( '/uploads/files/' . $note->cover_photo) }}"
                    PDFObject.embed(document.location.origin +'/uploads/files/'+ data[0].notes_file, "#example1");
                    $("#view_pdf_modal").modal();
                  },
                  error: function(data){
                    alert('failed to post data');
                  }
                });
            }
        
        });

        $("#view_pdf_modal").on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
        });    
      
    
  });
  </script>
@endsection
