@extends('backend.layouts.master')

@section('styles')
  <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
@endsection

@section('content')
    <div class="page-container">
        @include('backend.includes.sidebar')

        <div class="main-content">
            @include('backend.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/admin"><i class="fa-home"></i>Home</a></li>
                <li><a href="/admin/levels">School Levels</a></li>
                <li class="active"><strong>Primary</strong></li>

                @if (count($errors) > 0)
                    <div class="alert alert-danger" style='margin-bottom:10px'>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('subject_success'))
                     <div class="alert alert-success" role="alert">
                             {{ session('subject_success') }}
                     </div>
                @endif

                @if(session('subject_error'))
                   <div class="alert alert-danger" role="alert">
                             {{ session('subject_error') }}
                     </div>
                @endif
            </ol>
            <a href="javascript:;" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});">
                <button class="btn btn-success pull-right btn-icon icon-left" type="button">Add Subject <i
                            class="entypo-plus"></i></button>
            </a>

            <h2>Primary Level</h2>
            <hr/>

            <h4>Registered Subjects for this Level {{ $level_id }}</h4>

            <div class="dataTables_wrapper no-footer">
                <table class="table table-bordered table-striped datatable dataTable no-footer" id="levels_datatable" role="grid" aria-describedby="table-2_info">
                    <thead>
                    <tr>
                        <td>Subject Name</td>
                        <td>Description</td>
                        <!-- <td>Sales Summary</td> -->
                        <td>Created</td>
                        <td>Actions</td>
                    </tr>
                    </thead>

                    <tbody></tbody>
                </table>

            </div>


            <div class="modal fade" id="modal-6">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Register New Subject Level</h4>
                        </div>

                        <form action="/admin/level/subjects/create" method="post">

                            <div class="modal-body">

                                <div class="row">
                                    <div class="col-md-8">

                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Subject Name</label>

                                            <input type="text" class="form-control" id="field-1" placeholder="Subject Name" name="subject_name">
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Choose Status</label>

                                            <select class="form-control" name='subject_status'>
                                                <option value='Active'>Active</option>
                                                <option value='Inactive'>Inactive</option>
                                            </select>
                                        </div>

                                        <input type="hidden" name="level_chosen_id" value=" {{ $level_id }}">

                                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-info">Save changes</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="edit_subject_datatable">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Edit Subject Level</h4>
                        </div>

                        <form action="/admin/level/subjects/edit" method="post">

                            <div class="modal-body">

                                <div class="row">
                                    <div class="col-md-8">

                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Subject Name</label>

                                            <input type="text" class="form-control" placeholder="Subject Name" id="subject_name" name="subject_name">
                                        </div>
                                        <div class="form-group">
                                            <label for="field-1" class="control-label">Choose Status</label>

                                            <select class="form-control" id='subject_status' name='subject_status'>
                                                <option value='Active'>Active</option>
                                                <option value='Inactive'>Inactive</option>
                                            </select>
                                        </div>

                                        <input type="hidden" name="subject_id" id="subject_id" >

                                        <input type="hidden" name="level_chosen_id" value="{{ $level_id }}"> 

                                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-info">Save changes</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>


@endsection

@section('scripts')
  <!-- DataTables -->
  <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

  <!-- datatables btns -->
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  
  <!-- App scripts -->
  <script>
  $(function() {

      $('#levels_datatable').DataTable({
          processing: true,
          serverSide: true,
          dom: 'Bflrtip',
          buttons: [
              {
                  extend: 'copy',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'excel',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'pdf',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'print',
                  exportOptions: {
                      columns: [0,1]
                  }
              }
          ],
          ajax: { 
            url:'{{ URL::asset('/admin/level/subjects/data/fetch') }}',
            type: "GET",
            data: { "level_id" : {{ $level_id }} }
          },
          columns: [
              { data: 'subject_name', name: 'subject_name' },
              { data: 'subject_status', name: 'subject_status' },
              { data: 'created_at', name: 'created_at' },
              { data: 'id', name: 'id',
                "targets": 0,
                "render": function ( data, type, row ) {
                            var action_btns = '<a id="editSubject" rowId="'+data+'" class="btn btn-default btn-sm btn-icon icon-left" href="#"><i class="entypo-pencil"></i>Edit</a>';
                            action_btns += '&nbsp;<a id="deleteSubject" delete_rowId="'+data+'" class="btn btn-danger btn-sm btn-icon icon-left" href="#"><i class="entypo-cancel"></i>Delete</a>';
                            //action_btns += '&nbsp;<a id="profileSubject" profile_rowId="'+data+'" class="btn btn-info btn-sm btn-icon icon-left" href="level/'+data+'"><i class="entypo-info"></i>Profile</a>';
                            return action_btns;
                          }
              },
          ]
      });

      //edit data...
      $('#levels_datatable tbody').on('click', '#editSubject',function(e){
        e.preventDefault();
        var subject_id = $(this).attr('rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/level/subject/get',
          data: {subject_id: subject_id, level_chosen_id: {{ $level_id }} },
          success: function( data ) {
            console.log(data);
            $("#subject_name").val(data.subject_name);
            $("#subject_id").val(data.id);
            $('#subject_status option[value="'+data.subject_status+'"]').prop('selected','true');

            $("#edit_subject_datatable").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });

      //delete data...
      $('#levels_datatable tbody').on('click', '#deleteSubject',function(e){
        e.preventDefault();
        var subject_id = $(this).attr('delete_rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/level/subject/get',
          data: {subject_id: subject_id},
          success: function( data ) {
            $("#delete_level_id").val(data.id);
            $("#level_to_delete").text(data.name);

            $("#delete_level").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });
         
  });
  </script>
@endsection
