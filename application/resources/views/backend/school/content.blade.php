@extends('backend.layouts.master')


@section('content')
    <div class="page-container">
        @include('backend.includes.sidebar')

        <div class="main-content">
            @include('backend.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/admin"><i class="fa-home"></i>Home</a></li>
                <li><a href="/admin/subjects"><i class="fa-home"></i>Subjects</a></li>
                <li><a href="/admin/subjects/2"><i class="fa-home"></i>Subject Name</a></li>
                <li class="active"><strong>Topic Name</strong></li>
                <li class="active"><strong>Content</strong></li>
            </ol>

            <h3>{{ $subject_name }}({{ $level_name }} level) - {{ $topic_name }} - {{ $subtopic_name }} </h3>
            <hr/>

            @if (count($errors) > 0)
              <div class="alert alert-danger" style='margin-bottom:10px'>
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            @if(session('notes_success'))
              <div class="alert alert-success" role="alert">
                {{ session('notes_success') }}
              </div>
            @endif

            @if(session('notes_error'))
              <div class="alert alert-danger" role="alert">
                {{ session('notes_error') }}
              </div>
            @endif
            @if(session('video_success'))
              <div class="alert alert-success" role="alert">
                {{ session('video_success') }}
              </div>
            @endif

            @if(session('video_error'))
              <div class="alert alert-danger" role="alert">
                {{ session('video_error') }}
              </div>
            @endif

            <div class="row">
              <ul class="nav nav-tabs "><!-- available classes "bordered", "right-aligned" -->
                  <li class="active">
                      <a href="#home" data-toggle="tab" aria-expanded="true">
                          <span class="visible-xs"><i class="entypo-home"></i></span>
                          <span class="hidden-xs">Notes</span>
                      </a>
                  </li>
                  <li class="">
                      <a href="#profile" data-toggle="tab" aria-expanded="false">
                          <span class="visible-xs"><i class="entypo-user"></i></span>
                          <span class="hidden-xs">Video</span>
                      </a>
                  </li>
              </ul>

              <div class="tab-content">
              
              <div class="tab-pane active" id="home">                  
                <div class="row">

                  @if(count($notes) > 0)
                  @foreach($notes as $note)
                      <div class="col-sm-3">

                          <div class="tile-stats tile-white-cyan">
                            <img src="{{ URL::asset( '/uploads/files/' . $note->cover_photo) }}" class="cover_photo">
                              <h4>{{ $note->subject_name }}</h4>
                              <p>{{ $note->topic_name }}</p>
                              Post Title:<p>{{ $note->post_title }}</p>
                              <hr/>
                              <div class="pull-left">
                                  Views: {{ $note->views > 0 ? $note->views:0 }}
                              </div>
                              <div class="panel-options pull-right">
                                  
                                  <a href="test2.pdf" data-toggle="modal" class="view_notes" id="{{ $note->id }}" data-target="#sample-modal-dialog-1" title="View" class="bg"><i class="entypo-eye"></i> view</a>
                                  <a href="{{ URL::asset( '/uploads/files/' . $note->notes_file)  }}" download="{{$note->notes_file}}">Download</a>
                                  <a href="#" title="Delete" data-rel="close" class="delete_notes" id="{{ $note->id }}"><i class="entypo-cancel"></i> Delete</a>
                              </div>
                          </div>

                      </div>
                  @endforeach
                  @else
                      <div class="col-sm-3">
                          <b>No notes available yet</b>
                      </div>        
                  @endif
                </div>
              </div>
                        
              <div class="tab-pane" id="profile">
                <div class="row">

                    @if(count($videos) > 0)
                    @foreach($videos as $video)
                        <div class="col-sm-3">

                            <div class="tile-stats tile-white-cyan">
                                <img src="{{ URL::asset( '/uploads/files/' . $video->cover_photo) }}" class="cover_photo">
                                <h4>{{ $video->subject_name }}</h4>
                                <p>{{ $video->topic_name }}</p>
                                <hr/>
                                <div class="pull-left">
                                    Views: {{ $video->views > 0 ? $video->views:0 }}
                                </div>
                                <div class="panel-options pull-right">
                                    <a href="javascript:;" title="View" class="bg view_video" id="{{ $video->id }}"><i class="entypo-eye"></i> view</a>
                                    <a href="#" title="Delete" data-rel="close" class="delete_video" id="{{ $video->id }}"><i class="entypo-cancel"></i> Delete</a>
                                </div>
                            </div>

                        </div>
                    @endforeach 
                    @else
                        <div class="col-sm-3">
                            <b>No videos available yet</b>
                        </div>  
                    @endif

                </div>
              </div>

              </div>


            </div>

            
        </div>

    </div>
@endsection

@section('scripts')
  
  <!-- App scripts -->
  <script>
  $(function() {
      
      //edit data...
      $('#topic_subtopics_datatable tbody').on('click', '#editTopicSubtopic',function(e){
        e.preventDefault();
        var subtopic_id = $(this).attr('rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/topic/subtopic/get',
          data: {subtopic_id: subtopic_id},
          success: function( data ) {
            console.log(data);
            $("#subtopic_name").val(data.subtopic);
            $("#subtopic_id").val(data.id);
            $("#subtopic_desc").val(data.desc);

            $("#edit_modal-6").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });

      //delete data...
      $('#topic_subtopics_datatable tbody').on('click', '#deleteTopicSubtopic',function(e){
        e.preventDefault();
        var subtopic_id = $(this).attr('delete_rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/topic/subtopic/get',
          data: {subtopic_id: subtopic_id},
          success: function( data ) {
            $("#delete_level_id").val(data.id);
            $("#level_to_delete").text(data.name);

            $("#delete_level").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });
         
  });
  </script>
@endsection
