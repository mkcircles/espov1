@extends('backend.layouts.master')

@section('styles')
  <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
@endsection

@section('content')
    <div class="page-container">
        @include('backend.includes.sidebar')

        <div class="main-content">
            @include('backend.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/admin"><i class="fa-home"></i>Home</a></li>
                <li class="active"><strong>School Levels</strong></li>
            </ol>
            @if (count($errors) > 0)
                 <div class="alert alert-danger" style='margin-bottom:10px'>
                     <ul>
                         @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
            @endif
            @if(session('level_success'))
                 <div class="alert alert-success" role="alert">
                         {{ session('level_success') }}
                 </div>
            @endif

            @if(session('level_error'))
                <div class="alert alert-danger" role="alert">
                         {{ session('level_error') }}
                </div>
            @endif
            <a href="javascript:;" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});">
                <button class="btn btn-blue pull-right btn-icon icon-left" type="button">Add Level <i class="entypo-user-add"></i></button>
            </a>

            <h2>School Levels</h2>

        <div class="dataTables_wrapper no-footer">
            <table class="table table-bordered table-striped datatable dataTable no-footer" id="levels_datatable" role="grid" aria-describedby="table-2_info">
                <thead>
                <tr>
                    <td>Level</td>
                    <td>Subject Number</td>
                    <td>Attached Teachers</td>
                    <td>Actions</td>
                </tr>
                </thead>

                <tbody></tbody>
            </table>

        </div>

    </div>
        <div class="modal fade" id="modal-6">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Register New School Level</h4>
                    </div>

                    <form action="/admin/levels/create" method="post">

                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-8">

                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Level Name</label>

                                        <input type="text" class="form-control" id="field-1" placeholder="Level name" name="level_name">
                                    </div>

                                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="edit_level">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit School Level</h4>
                    </div>

                    <form action="/admin/levels/edit" method="post">

                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-8">

                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Level Name</label>

                                        <input type="text" class="form-control" id="level_name" placeholder="Level name" name="level_name">
                                    </div>

                                    <input type="hidden" class="form-control" id="level_id" name='level_id'>
                                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="delete_level">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete School Level</h4>
                    </div>

                    <form action="/admin/levels/delete" method="post">

                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-8">

                                    <div class="form-group">
                                        Are sure you want to delete <p id="level_to_delete"></p> ?
                                    </div>

                                    <input type="hidden" class="form-control" id="delete_level_id" name='level_id'>
                                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-info">Yes</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

<!-- viewing pdf -->
<div class="modal fade" id="view_pdf_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Viewing pdf for <strong id="notes_subject_name"></strong></h4>
            </div>

            <form action="/teacher/notes/post" method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="row">
                        <div id="example1"></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Delete Pdf</button>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- Modal 2 (Custom Width)-->
<div class="modal fade custom-width" id="modal-2">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Custom Width Modal</h4>
            </div>

            <div class="modal-body">
               <div class="row">
                   <div class="col-md-8">
                       <iframe id="youtubeiframe" width="560" height="315" frameborder="0" allowfullscreen></iframe>
                   </div>
                   <div class="col-md-4">
                       <p id="video_post_title"></p>
                       <p id="video_subject_name"></p>
                       <p id="video_topic_name"></p>
                       <p id="video_subtopic_name"></p>
                       <p id="video_views"></p>
                       <p id="video_student_subscriptions"></p>

                   </div>
               </div>

            </div>

            <div class="modal-footer">
                <p></p>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
  <!-- DataTables -->
  <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

  <!-- datatables btns -->
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>


        <!-- App scripts -->
  <script>
  $(function() {

      $('#levels_datatable').DataTable({
          processing: true,
          serverSide: true,
          dom: 'Bflrtip',
          buttons: [
              {
                  extend: 'copy',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'excel',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'pdf',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'print',
                  exportOptions: {
                      columns: [0,1]
                  }
              }
          ],
          ajax: '{{ URL::asset('/admin/levels/data/fetch') }}',
          columns: [
              { data: 'name', name: 'name' },
              { data: 'no_of_subjects', name: 'no_of_subjects' },
              { data: 'no_of_teachers', name: 'no_of_teachers' },
              { data: 'id', name: 'id',
                "targets": 0,
                "render": function ( data, type, row ) {
                            var action_btns = '<a id="editLevel" rowId="'+data+'" class="btn btn-default btn-sm btn-icon icon-left" href="#"><i class="entypo-pencil"></i>Edit</a>';
                            action_btns += '&nbsp;<a id="deleteLevel" delete_rowId="'+data+'" class="btn btn-danger btn-sm btn-icon icon-left" href="#"><i class="entypo-cancel"></i>Delete</a>';
                            action_btns += '&nbsp;<a id="profileLevel" profile_rowId="'+data+'" class="btn btn-info btn-sm btn-icon icon-left" href="level/'+data+'"><i class="entypo-info"></i>View Subjects</a>';
                            return action_btns;
                          }
              },
          ]
      });

      //edit data...
      $('#levels_datatable tbody').on('click', '#editLevel',function(e){
        e.preventDefault();
        var level_id = $(this).attr('rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/level/get',
          data: {level_id: level_id},
          success: function( data ) {
            console.log(data);
            $("#level_name").val(data.name);
            $("#level_id").val(data.id);

            $("#edit_level").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });

      //delete data...
      $('#levels_datatable tbody').on('click', '#deleteLevel',function(e){
        e.preventDefault();
        var level_id = $(this).attr('delete_rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/level/get',
          data: {level_id: level_id},
          success: function( data ) {
            $("#delete_level_id").val(data.id);
            $("#level_to_delete").text(data.name);

            $("#delete_level").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });
         
  });
  </script>
@endsection
