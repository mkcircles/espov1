@extends('backend.layouts.master')

@section('styles')
    <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css"/>
@endsection

@section('content')
    <div class="page-container">
        @include('backend.includes.sidebar')

        <div class="main-content">
            @include('backend.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/admin"><i class="fa-home"></i>Home</a></li>
                <li><a href="/admin/subjects"><i class="fa-home"></i>Subjects</a></li>
                <li><a href="/admin/subjects/2"><i class="fa-home"></i>Subject Name</a></li>
                <li class="active"><strong>Topic Name</strong></li>
            </ol>

            <h4><strong>Registered topics</strong></h4>

            <!-- <div class="dataTables_wrapper no-footer">
                <table class="table table-bordered table-striped datatable dataTable no-footer" id="levels_datatable"
                       role="grid" aria-describedby="table-2_info" style="font-size: 12px">
                    <thead>
                    <tr>
                        <td>Post Title</td>
                        <td>Sub Topic</td>
                        <td>Status</td>
                        <td>Created By</td>
                        <td>Created On</td>
                        <td>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr role="row" class="odd">
                        <td class="sorting_1">Measurement Of Physical Quantities</td>
                        <td>This topic covers "Measurement Of Physical Properties" of O Level Physics.</td>
                        <td>Active</td>
                        <td>Teacher Name</td>
                        <td>2016-02-19</td>
                        <td>
                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                            <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-docs"></i> Sub Topics</a>
                        </td>
                    </tr>
                    <tr role="row" class="even">
                        <td class="sorting_1">Speed, Velocity And Acceleration</td>
                        <td>This topic covers "Speed, Velocity and Acceleration" of O Level Physics.</td>
                        <td>Active</td>
                        <td>Teacher Name</td>
                        <td>2016-02-19</td>

                        <td>
                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                            <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-docs"></i> Sub Topics</a>
                        </td>
                    </tr>
                    <tr role="row" class="odd">
                        <td class="sorting_1">Forces & Turning Effect Of Forces</td>
                        <td>This topic covers "Forces & Turning Effect Of Forces" of O Level Physics.</td>
                        <td>Active</td>
                        <td>Teacher Name</td>
                        <td>2016-02-19</td>

                        <td>
                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                            <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-docs"></i> Sub Topics</a>
                        </td>
                    </tr>
                    <tr role="row" class="even">
                        <td class="sorting_1">Mass, Weight And Density</td>
                        <td>This topic covers "Mass, Weight and Density" of O Level Physics.</td>
                        <td>Active</td>
                        <td>Teacher Name</td>
                        <td>2016-02-19</td>
                        <td>
                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                            <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-docs"></i>Sub Topics</a>
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div> -->
            <div class="dataTables_wrapper no-footer">
                <table class="table table-bordered table-striped datatable dataTable no-footer" id="topics_datatable"
                       role="grid" aria-describedby="table-2_info" style="font-size: 12px">
                    <thead>
                    <tr>
                        <td>Topic</td>
                        <td>Subject</td>
                        <td>Description</td>
                        <td>Views</td>
                        <td>Created On</td>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>

        </div>

    </div>
@endsection

@section('scripts')
  <!-- DataTables -->
  <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

  <!-- datatables btns -->
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  
  <!-- App scripts -->
  <script>
  $(function() {

      $('#topics_datatable').DataTable({
          processing: true,
          serverSide: true,
          dom: 'Bflrtip',
          buttons: [
              {
                  extend: 'copy',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'excel',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'pdf',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'print',
                  exportOptions: {
                      columns: [0,1]
                  }
              }
          ],
          ajax: { 
            url:'{{ URL::asset('/admin/topic/data/fetch') }}',
            type: "GET"
          },
          columns: [
              { data: 'topic_name', name: 'topic_name' },
              { data: 'subject_name', name: 'subject_name' },
              { data: 'description', name: 'description' },
              { data: 'views', name: 'views' },
              { data: 'created_at', name: 'created_at' },
              // { data: 'id', name: 'id',
              //   "targets": 0,
              //   "render": function ( data, type, row ) {
              //               if(data != ""){
              //                 var action_btns = '<a id="editTopicSubtopic" rowId="'+data+'" class="btn btn-default btn-sm btn-icon icon-left" href="#"><i class="entypo-pencil"></i>Edit</a>';
              //                 action_btns += '&nbsp;<a id="deleteTopicSubtopic" delete_rowId="'+data+'" class="btn btn-danger btn-sm btn-icon icon-left" href="#"><i class="entypo-cancel"></i>Delete</a>';
              //                 action_btns += '&nbsp;<a id="profileTopicSubtopic" profile_rowId="'+data+'" class="btn btn-info btn-sm btn-icon icon-left" href="/admin/subtopic/content/'+data+'"><i class="entypo-info"></i>View Content</a>';
              //                 return action_btns;
              //               }
              //               return '';
              //             }
              // },
          ]
      });
         
  });
  </script>
@endsection
