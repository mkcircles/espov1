@extends('backend.layouts.master')

@section('styles')
    <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
@endsection

@section('content')
    <div class="page-container">
        @include('backend.includes.sidebar')

        <div class="main-content">
            @include('backend.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/admin"><i class="fa-home"></i>Home</a></li>
                <li><a href="/admin/teachers"><i class="fa-home"></i>Student</a></li>
                <li class="active"><strong>Student Name</strong></li>
            </ol>

            <div class="profile-env">
                <header class="row">
                    <div class="col-sm-3">
                        <a href="#" class="profile-picture">
                            <img src="{{ asset('assets/backend/images/profile-picture.png') }}" class="img-responsive img-circle">
                        </a>
                    </div>
                    <div class="col-sm-7">
                        <ul class="profile-info-sections">
                            <li>
                                <div class="profile-name">
                                    <strong>
                                        <a href="#">{{ $user->name }}</a>
                                        <a href="#" class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online"></a>
                                        <!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
                                    <span><a href="#">Co-Founder at Laborator</a></span>
                                </div>
                            </li>
                            <li>
                                <div class="profile-stat">
                                    @if( isset($notes) )
                                    <h3>{{ count($notes) }}</h3>
                                    @else
                                    <h3>0</h3>
                                    @endif
                                    <span><a href="#">Notes Posts</a></span>
                                </div>
                            </li>

                            <li>
                                <div class="profile-stat">
                                    @if( isset($videos) )
                                    <h3>{{ count($videos) }}</h3>
                                    @else
                                    <h3>0</h3>
                                    @endif
                                    <span><a href="#">Video Posts</a></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="profile-buttons">
                        </div>
                    </div>
                </header>

                <section class="profile-info-tabs">
                    <div class="row">
                        <div class="col-sm-offset-3 col-sm-4">
                            <ul class="user-details">
                                <li><a href="#"><i class="entypo-phone"></i>{{ $user->contact }}</a></li>
                                <li><a href="#"><i class="entypo-location"></i>{{ $user->address }}</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <ul class="user-details">
                                <li><a href="#"><i class="entypo-mail"></i>{{ $user->email }}</a></li>
                                <li><a href="#"><i class="entypo-calendar"></i>16 October</a></li>
                            </ul>
                        </div>

                    </div>
                </section>

            </div>

            <div class="row">

                    <ul class="nav nav-tabs "><!-- available classes "bordered", "right-aligned" -->
                        <li class="active">
                            <a href="#home" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="entypo-home"></i></span>
                                <span class="hidden-xs">Notes</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#profile" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="entypo-user"></i></span>
                                <span class="hidden-xs">Video</span>
                            </a>
                        </li>
                    </ul>

                    @if (count($errors) > 0)
                    <div class="alert alert-danger" style='margin-bottom:10px'>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                         </ul>
                    </div>
                    @endif
                    @if(session('notes_success'))
                         <div class="alert alert-success" role="alert">
                                 {{ session('notes_success') }}
                         </div>
                    @endif

                    @if(session('notes_error'))
                       <div class="alert alert-danger" role="alert">
                                 {{ session('notes_error') }}
                         </div>
                    @endif
                    @if(session('video_success'))
                         <div class="alert alert-success" role="alert">
                                 {{ session('video_success') }}
                         </div>
                    @endif

                    @if(session('video_error'))
                       <div class="alert alert-danger" role="alert">
                                 {{ session('video_error') }}
                         </div>
                    @endif

                    <div class="tab-content">
                    
                    <div class="tab-pane active" id="home">           
                        <div class="dataTables_wrapper no-footer">
                            <table class="table table-bordered table-striped datatable dataTable no-footer" id="notes_datatable" role="grid" aria-describedby="table-2_info">
                                <thead>
                                <tr>
                                    <td>Subject</td>
                                    <td>Topic</td>
                                    <td>Sub Topic</td>                                    
                                    <td>Level</td>
                                    <td>Status</td>
                                    <td>Date</td>
                                </tr>
                                </thead>

                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane" id="profile">

                        <div class="row">
                            <div class="dataTables_wrapper no-footer">
                                <table class="table table-bordered table-striped datatable dataTable no-footer" id="videos_datatable" role="grid" aria-describedby="table-2_info">
                                    <thead>
                                    <tr>                                        
                                        <td>Subject</td>
                                        <td>Topic</td>
                                        <td>Sub Topic</td>
                                        <td>Level</td>
                                        <td>Status</td>
                                        <td>Date</td>
                                    </tr>
                                    </thead>

                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    </div>


            </div>

            </div>
        </div>

        <div>

        </div>
@endsection

@section('scripts')
    <!-- App scripts -->
    <script>
    $(function() {

        $('#notes_datatable').DataTable({
          processing: true,
          serverSide: true,
          dom: 'Bflrtip',
          buttons: [
              {
                  extend: 'copy',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'excel',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'pdf',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'print',
                  exportOptions: {
                      columns: [0,1]
                  }
              }
          ],
          ajax: { 
            url:'{{ URL::asset('/notes/data/fetch') }}'
          },
          columns: [
              { data: 'subject_name', name: 'subject_name' },
              { data: 'topic_name', name: 'topic_name' },
              { data: 'subtopic', name: 'subtopic' },
              { data: 'name', name: 'name' },
              { data: 'subject_status', name: 'subject_status' },
              { data: 'created_at', name: 'created_at' },
              { data: 'id', name: 'id',
                "targets": 0,
                "render": function ( data, type, row ) {
                            var action_btns = '<a id="editStudent" rowId="'+data+'" class="btn btn-default btn-sm btn-icon icon-left" href="#"><i class="entypo-pencil"></i>Edit</a>';
                            action_btns += '&nbsp;<a id="deleteStudent" delete_rowId="'+data+'" class="btn btn-danger btn-sm btn-icon icon-left" href="#"><i class="entypo-cancel"></i>Delete</a>';
                            return action_btns;
                          }
              },
          ]
      });

      //edit data...
      $('#notes_datatable tbody').on('click', '#editStudent',function(e){
        e.preventDefault();
        var student_id = $(this).attr('rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/student/notes/get',
          data: {student_id: student_id},
          success: function( data ) {
            console.log(data);
            $("#student_id").val(data[0].id);
            $('#subject_status option[value="'+data[0].subject_status+'"]').prop('selected','true');

            $("#edit_notes_modal").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });

      $('#notes_datatable').DataTable({
          processing: true,
          serverSide: true,
          dom: 'Bflrtip',
          buttons: [
              {
                  extend: 'copy',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'excel',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'pdf',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'print',
                  exportOptions: {
                      columns: [0,1]
                  }
              }
          ],
          ajax: { 
            url:'{{ URL::asset('/videos/data/fetch') }}'
          },
          columns: [
              { data: 'subject_name', name: 'subject_name' },
              { data: 'topic_name', name: 'topic_name' },
              { data: 'subtopic', name: 'subtopic' },
              { data: 'name', name: 'name' },
              { data: 'subject_status', name: 'subject_status' },
              { data: 'created_at', name: 'created_at' },
              { data: 'id', name: 'id',
                "targets": 0,
                "render": function ( data, type, row ) {
                            var action_btns = '<a id="editVideoStudent" rowId="'+data+'" class="btn btn-default btn-sm btn-icon icon-left" href="#"><i class="entypo-pencil"></i>Edit</a>';
                            action_btns += '&nbsp;<a id="deleteVideoStudent" delete_rowId="'+data+'" class="btn btn-danger btn-sm btn-icon icon-left" href="#"><i class="entypo-cancel"></i>Delete</a>';
                            return action_btns;
                          }
              },
          ]
      });

      //edit data...
      $('#videos_datatable tbody').on('click', '#editVideoStudent',function(e){
        e.preventDefault();
        var student_id = $(this).attr('rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/student/videos/get',
          data: {student_id: student_id},
          success: function( data ) {
            console.log(data);
            $("#student_id").val(data[0].id);
            $('#subject_status option[value="'+data[0].subject_status+'"]').prop('selected','true');

            $("#edit_videos_modal").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });
         
  });

  </script>
@endsection
