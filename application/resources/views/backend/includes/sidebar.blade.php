<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="/admin">
                    <img src="{{ asset('assets/backend/images/logo@2x.png') }}" width="120" alt="" />
                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <li>
                <a href="/admin">
                    <i class="entypo-home"></i><span class="title">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="/admin/levels">
                    <i class="entypo-chart-bar"></i><span class="title">School Levels</span>
                </a>
            </li>
            <li class="has-sub">
                <a href="/admin/teachers"><i class="entypo-user"></i><span class="title">Teachers</span></a>
                <ul>
                    <li><a href="/admin/teachers"><span class="title">Registered Teachers</span></a></li>
                    <li><a href="#"><span class="title">Chat Open</span></a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="/admin/subjects"><i class="entypo-clipboard"></i><span class="title">Subjects</span></a>
                <ul>
                    <li><a href="/admin/subjects"><span class="title">Registered Subjects</span></a></li>
                    <li><a href="/admin/topics"><span class="title">Subject Topics</span></a></li>
                    <li><a href="/admin/subtopics"><span class="title">Subject Sub Topics</span></a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="/admin/teachers"><i class="entypo-user"></i><span class="title">Uploaded Content</span></a>
                <ul>
                    <li><a href="/admin/uploaded/notes"><span class="title">Uploaded Notes</span></a></li>
                    <li><a href="/admin/uploaded/videos"><span class="title">Uploaded Videos</span></a></li>
                </ul>
            </li>
            <li>
                <a href="/admin/students">
                    <i class="entypo-chart-bar"></i><span class="title">Students</span>
                </a>
            </li>
            <li>
                <a href="/admins">
                    <i class="entypo-chart-bar"></i><span class="title">Admins</span>
                </a>
            </li>
            <li>
                <a href="/user/profile">
                    <i class="entypo-chart-bar"></i><span class="title">My Profile</span>
                </a>
            </li>

        </ul>

    </div>

</div>