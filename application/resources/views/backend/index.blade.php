@extends('backend.layouts.master')

@section('content')
<div class="page-container">
    @include('backend.includes.sidebar')

    <div class="main-content">
        @include('backend.includes.topMenu')

        <div class="row">
            <div class="col-sm-3 col-xs-6">

                <div class="tile-stats tile-red">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div class="num" data-start="0" data-end="83" data-postfix="" data-duration="1500" data-delay="0">0</div>

                    <h3>Registered users</h3>
                    <p>so far in our blog, and our website.</p>
                </div>

            </div>

            <div class="col-sm-3 col-xs-6">

                <div class="tile-stats tile-green">
                    <div class="icon"><i class="entypo-chart-bar"></i></div>
                    <div class="num" data-start="0" data-end="135" data-postfix="" data-duration="1500" data-delay="600">0</div>

                    <h3>Daily Visitors</h3>
                    <p>this is the average value.</p>
                </div>

            </div>

            <div class="clear visible-xs"></div>

            <div class="col-sm-3 col-xs-6">

                <div class="tile-stats tile-aqua">
                    <div class="icon"><i class="entypo-mail"></i></div>
                    <div class="num" data-start="0" data-end="23" data-postfix="" data-duration="1500" data-delay="1200">0</div>

                    <h3>New Messages</h3>
                    <p>messages per day.</p>
                </div>

            </div>

            <div class="col-sm-3 col-xs-6">

                <div class="tile-stats tile-blue">
                    <div class="icon"><i class="entypo-rss"></i></div>
                    <div class="num" data-start="0" data-end="52" data-postfix="" data-duration="1500" data-delay="1800">0</div>

                    <h3>Subscribers</h3>
                    <p>on our site right now.</p>
                </div>

            </div>
        </div><br/>
        <div class="row">

            <div class="col-sm-4">

                <div class="panel panel-primary">
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th class="padding-bottom-none text-center">
                                <br>
                                <br>
                                <span class="monthly-sales"><canvas width="262" height="110" style="width: 262px; height: 110px; vertical-align: top; display: inline-block;"></canvas></span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="panel-heading">
                                <h4>Monthly Sales</h4>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="col-sm-8">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-title">Latest Updated Profiles</div>

                        <div class="panel-options">
                            <a class="bg" href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"><i class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <div class="panel-body with-table"><table class="table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Activity</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Art Ramadani</td>
                                <td>CEO</td>
                                <td>
                                    <a class="btn btn-default btn-sm btn-icon icon-left" href="#"><i class="entypo-pencil"></i> Edit</a>
                                    <a class="btn btn-danger btn-sm btn-icon icon-left" href="#"><i class="entypo-cancel"></i> Delete </a>
                                    <a class="btn btn-info btn-sm btn-icon icon-left" href="#"><i class="entypo-info"></i> Profile</a>
                                </td>
                            </tr>

                            <tr>
                                <td>2</td>
                                <td>Ylli Pylla</td>
                                <td>Font-end Developer</td>
                                <td>
                                    <a class="btn btn-default btn-sm btn-icon icon-left" href="#"><i class="entypo-pencil"></i> Edit</a>
                                    <a class="btn btn-danger btn-sm btn-icon icon-left" href="#"><i class="entypo-cancel"></i> Delete </a>
                                    <a class="btn btn-info btn-sm btn-icon icon-left" href="#"><i class="entypo-info"></i> Profile</a>
                                </td>
                            </tr>

                            <tr>
                                <td>3</td>
                                <td>Arlind Nushi</td>
                                <td>Co-founder</td>
                                <td>
                                    <a class="btn btn-default btn-sm btn-icon icon-left" href="#"><i class="entypo-pencil"></i> Edit</a>
                                    <a class="btn btn-danger btn-sm btn-icon icon-left" href="#"><i class="entypo-cancel"></i> Delete </a>
                                    <a class="btn btn-info btn-sm btn-icon icon-left" href="#"><i class="entypo-info"></i> Profile</a>
                                </td>
                            </tr>

                            </tbody>
                        </table></div>
                </div>

            </div>

        </div>
    </div>

</div>
@endsection