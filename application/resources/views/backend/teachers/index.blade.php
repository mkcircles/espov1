@extends('backend.layouts.master')

@section('styles')
    <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
@endsection

@section('content')
    <div class="page-container">
        @include('backend.includes.sidebar')

        <div class="main-content">
    @include('backend.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/admin"><i class="fa-home"></i>Home</a></li>
                <li class="active"><strong>Teaches</strong></li>
            </ol>
            <a href="javascript:;" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});">
                <button class="btn btn-blue pull-right btn-icon icon-left" type="button">Register Teacher <i class="entypo-user-add"></i></button>
            </a>
            <h3>Registered Teachers</h3>
            @if (count($errors) > 0)
                 <div class="alert alert-danger" style='margin-bottom:10px'>
                     <ul>
                         @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
            @endif
            @if(session('teacher_success'))
                 <div class="alert alert-success" role="alert">
                         {{ session('teacher_success') }}
                 </div>
            @endif

            @if(session('teacher_error'))
               <div class="alert alert-danger" role="alert">
                         {{ session('teacher_error') }}
                 </div>
            @endif
            <hr/>
            <div id="table-2_wrapper" class="dataTables_wrapper no-footer">
                <table class="table table-bordered table-striped datatable dataTable no-footer" id="teachers_datatable" role="grid"
                       aria-describedby="table-2_info" style="font-size: 12px">
                    <thead>
                    <tr>
                        <td>Name</td>
                        <td>Phone</td>
                        <td>Email</td>
                        <td>Address</td>
                        <td>Status</td>
                        <td>Actions</td>
                    </tr>
                    </thead>

                    <tbody></tbody>
                </table>

            </div>


</div>
        <div class="modal fade" id="modal-6">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Register New Teacher</h4>
                    </div>

                    <form action="/admin/teachers/create" method="post" enctype="multipart/form-data">    

                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Teacher's Name</label>
                                        <input type="text" class="form-control" id="field-1" placeholder="Full Name" name="name">
                                    </div>

                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Phone Number</label>
                                        <input type="text" class="form-control" id="field-1" placeholder="Phone Number" name="phone">
                                    </div>

                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Country</label>
                                        <input type="text" class="form-control" id="field-1" placeholder="Uganda" name="country">
                                    </div>

                                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Email</label>
                                        <input type="text" class="form-control" id="field-1" placeholder="Teacher Email" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Address</label>
                                        <input type="text" class="form-control" id="field-1" placeholder="Address" name="address">
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="control-label">Profile Pic</label>
                                        <input type="file" class="form-control" id="field-file" placeholder="Placeholder" name="profile_pic">
                                    </div>

                                    </div>
                                <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Description</label>
                                    <textarea class="form-control" id="field-ta" rows="5" placeholder="Simple Description" name="profile_desc"></textarea>
                                </div>
                                    </div>

                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Save changes</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="edit_teacher">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Teacher</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <img src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQS5tXW5alC9zMTf5gPgtHjvHoXsvXKstv1crXRICg5qINmbV2g68W-sGfCmg" class="img-rounded" id="coverPhoto" style="width: 120px; margin: 6px">
                        </div>
                        <div class="col-md-9">
                            <form action="/admin/teachers/edit" method="post" enctype="multipart/form-data">

                                <div class="modal-body">

                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Teacher's Name</label>
                                                <input type="text" class="form-control" id="name" placeholder="Full Name" name="name">
                                            </div>

                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Phone Number</label>
                                                <input type="text" class="form-control" id="contact" placeholder="Phone Number" name="phone">
                                            </div>

                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Country</label>
                                                <input type="text" class="form-control" id="country" placeholder="Uganda" name="country">
                                            </div>

                                            <input type='hidden' name='teacher_id' id='teacher_id'>

                                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Email</label>
                                                <input type="text" class="form-control" id="email" placeholder="Teacher Email" name="email">
                                            </div>
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Address</label>
                                                <input type="text" class="form-control" id="address" placeholder="Address" name="address">
                                            </div>
                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Profile Pic</label>
                                                <input type="file" class="form-control" id="profile_pic" placeholder="Placeholder" name="profile_pic">
                                            </div>

                                        </div>
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Choose Status</label>

                                                <select class="form-control" id='user_status' name='user_status'>
                                                    <option value='Active'>Active</option>
                                                    <option value='Inactive'>Inactive</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-1" class="control-label">Description</label>
                                                <textarea class="form-control" id="profile_desc" rows="5" placeholder="Simple Description" name="profile_desc"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-info">Save changes</button>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
@endsection

@section('scripts')
  <!-- DataTables -->
  <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

  <!-- datatables btns -->
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  
  <!-- App scripts -->
  <script>
  $(function() {

      $('#teachers_datatable').DataTable({
          processing: true,
          serverSide: true,
          dom: 'Bflrtip',
          buttons: [
              {
                  extend: 'copy',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'excel',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'pdf',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'print',
                  exportOptions: {
                      columns: [0,1]
                  }
              }
          ],
          ajax: '{{ URL::asset('/admin/teachers/data/fetch') }}',
          columns: [
              { data: 'name', name: 'name' },
              { data: 'contact', name: 'contact' },
              { data: 'email', name: 'email' },
              { data: 'address', name: 'address' },
              { data: 'user_status', name: 'user_status' },
              { data: 'id', name: 'id',
                "targets": 0,
                "render": function ( data, type, row ) {
                            var action_btns = '<a id="editTeacher" rowId="'+data+'" class="btn btn-default btn-sm btn-icon icon-left" href="#"><i class="entypo-pencil"></i>Edit</a>';
                            action_btns += '&nbsp;<a id="deleteTeacher" delete_rowId="'+data+'" class="btn btn-danger btn-sm btn-icon icon-left" href="#"><i class="entypo-cancel"></i>Delete</a>';
                            action_btns += '&nbsp;<a id="profileTeacher" profile_rowId="'+data+'" class="btn btn-info btn-sm btn-icon icon-left" href="/admin/teachers/'+data+'"><i class="entypo-info"></i>Profile</a>';
                            return action_btns;
                          }
              },
          ]
      });

      //edit data...
      $('#teachers_datatable tbody').on('click', '#editTeacher',function(e){
        e.preventDefault();
        var teacher_id = $(this).attr('rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/teacher/get',
          data: {teacher_id: teacher_id},
          success: function( data ) {
            console.log(data);
            $("#name").val(data.name);
            $("#contact").val(data.contact);
            $("#email").val(data.email);
            $("#address").val(data.address);
            $("#country").val(data.country);
            $("#profile_desc").val(data.profile_desc);
            $("#user_status").val(data.user_status);
            $("#profile_pic").attr({ src: "/uploads/teacher_photos/"+data.profile_pic });
            $("#teacher_id").val(data.id);

            $("#edit_teacher").modal(); 
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });

      //delete data...
      $('#teachers_datatable tbody').on('click', '#deleteLevel',function(e){
        e.preventDefault();
        var level_id = $(this).attr('delete_rowId');
        var data;
        $.ajax({
          type: "GET",
          url: '/level/get',
          data: {level_id: level_id},
          success: function( data ) {
            $("#delete_level_id").val(data.id);
            $("#level_to_delete").text(data.name);

            $("#delete_level").modal();
          },
          error: function(data){
            alert('failed to post data');
          }
        });
        
      });
         
  });
  </script>
@endsection
