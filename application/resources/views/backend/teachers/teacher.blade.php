@extends('backend.layouts.master')

@section('styles')
    <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
@endsection

@section('content')
    <div class="page-container">
        @include('backend.includes.sidebar')

        <div class="main-content">
            @include('backend.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/admin"><i class="fa-home"></i>Home</a></li>
                <li><a href="/admin/teachers"><i class="fa-home"></i>Teachers</a></li>
                <li class="active"><strong>Teacher Name</strong></li>
            </ol>

            <div class="profile-env">
                <header class="row">
                    <div class="col-sm-3">
                        <a href="#" class="profile-picture">
                            <img src="{{ asset('assets/backend/images/profile-picture.png') }}" class="img-responsive img-circle">
                        </a>
                    </div>
                    <div class="col-sm-7">
                        <ul class="profile-info-sections">
                            <li>
                                <div class="profile-name">
                                    <strong>
                                        <a href="#">{{ $user->name }}</a>
                                        <a href="#" class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online"></a>
                                        <!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
                                    <span><a href="#">Co-Founder at Laborator</a></span>
                                </div>
                            </li>
                            <li>
                                <div class="profile-stat">
                                    <h3>{{ count($notes) }}</h3>
                                    <span><a href="#">Notes Posts</a></span>
                                </div>
                            </li>

                            <li>
                                <div class="profile-stat">
                                    <h3>{{ count($videos) }}</h3>
                                    <span><a href="#">Video Posts</a></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="profile-buttons">
                        </div>
                    </div>
                </header>

                <section class="profile-info-tabs">
                    <div class="row">
                        <div class="col-sm-offset-3 col-sm-4">
                            <ul class="user-details">
                                <li><a href="#"><i class="entypo-phone"></i>{{ $user->contact }}</a></li>
                                <li><a href="#"><i class="entypo-location"></i>{{ $user->address }}</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <ul class="user-details">
                                <li><a href="#"><i class="entypo-mail"></i>{{ $user->email }}</a></li>
                                <li><a href="#"><i class="entypo-calendar"></i>16 October</a></li>
                            </ul>
                        </div>

                    </div>
                </section>

            </div>

            <div class="row">

                    <ul class="nav nav-tabs "><!-- available classes "bordered", "right-aligned" -->
                        <li class="active">
                            <a href="#home" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="entypo-home"></i></span>
                                <span class="hidden-xs">Notes</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#profile" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="entypo-user"></i></span>
                                <span class="hidden-xs">Video</span>
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="home">
                               @if (count($errors) > 0)
                 <div class="alert alert-danger" style='margin-bottom:10px'>
                     <ul>
                         @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
            @endif
            @if(session('notes_success'))
                 <div class="alert alert-success" role="alert">
                         {{ session('notes_success') }}
                 </div>
            @endif

            @if(session('notes_error'))
               <div class="alert alert-danger" role="alert">
                         {{ session('notes_error') }}
                 </div>
            @endif
            @if(session('video_success'))
                 <div class="alert alert-success" role="alert">
                         {{ session('video_success') }}
                 </div>
            @endif

            @if(session('video_error'))
               <div class="alert alert-danger" role="alert">
                         {{ session('video_error') }}
                 </div>
            @endif

            <div class="row">

                @if(count($notes) > 0)
                @foreach($notes as $note)
                    <div class="col-sm-3">

                        <div class="tile-stats tile-white-cyan">
                          <img src="{{ URL::asset( '/uploads/files/' . $note->cover_photo) }}" class="cover_photo">
                            <h4>{{ $note->name }}</h4>
                            <h4>{{ $note->subject_name }}</h4>
                            <p>{{ $note->topic_name }}</p>
                            <p>UGX {{ $note->notes_pricing }}</p>
                            Post Title:<strong>{{ $note->post_title }}</strong>
                            <hr/>
                            <div class="pull-left">
                                Views: {{ $note->views > 0 ? $note->views:0 }}
                            </div>
                            <div class="panel-options pull-right">
                                
                                <a href="test2.pdf" data-toggle="modal" class="view_notes" id="{{ $note->id }}" data-target="#sample-modal-dialog-1" title="View" class="bg"><i class="entypo-eye"></i> view</a>
                                <a href="{{ URL::asset( '/uploads/files/' . $note->notes_file)  }}" download="{{$note->notes_file}}">Download</a>
                                <a href="#" title="Delete" data-rel="close" class="delete_notes" id="{{ $note->id }}"><i class="entypo-cancel"></i> Delete</a>
                            </div>
                        </div>

                    </div>
                @endforeach
                @else
                    <div class="col-sm-3">
                        <b>No notes available yet</b>
                    </div>        
                @endif
            </div>


                        </div>
                        <div class="tab-pane" id="profile">

                            <div class="row">

                                @if(count($videos) > 0)
                                @foreach($videos as $video)
                                    <div class="col-sm-3">

                                        <div class="tile-stats tile-white-cyan">
                                            <img src="{{ URL::asset( '/uploads/files/' . $video->cover_photo) }}" class="cover_photo">
                                            <h4>{{ $video->subject_name }}</h4>
                                            <p>{{ $video->topic_name }}</p>
                                            <h4>{{ $video->name }}</h4>
                                            <p>UGX {{ $video->video_pricing }}</p>
                                            <hr/>
                                            <div class="pull-left">
                                                Views: {{ $video->views > 0 ? $video->views:0 }}
                                            </div>
                                            <div class="panel-options pull-right">
                                                <a href="javascript:;" title="View" class="bg view_video" id="{{ $video->id }}"><i class="entypo-eye"></i> view</a>
                                                <a href="#" title="Delete" data-rel="close" class="delete_video" id="{{ $video->id }}"><i class="entypo-cancel"></i> Delete</a>
                                            </div>
                                        </div>

                                    </div>
                                @endforeach 
                                @else
                                    <div class="col-sm-3">
                                        <b>No videos available yet</b>
                                    </div>  
                                @endif

                            </div>
                        </div>

                    </div>


            </div>

            </div>
        </div>
<!-- viewing pdf -->
<div class="modal fade" id="view_pdf_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Viewing pdf for <strong id="notes_subject_name"></strong></h4>
            </div>

            <form action="/teacher/notes/post" method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="row">
                        <div id="example1"></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Delete Pdf</button>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- Modal 2 (Custom Width)-->
<div class="modal fade custom-width" id="modal-2">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Custom Width Modal</h4>
            </div>

            <div class="modal-body">
               <div class="row">
                   <div class="col-md-8">
                       <iframe id="youtubeiframe" width="560" height="315" frameborder="0" allowfullscreen></iframe>
                   </div>
                   <div class="col-md-4">
                       <p id="video_post_title"></p>
                       <p id="video_subject_name"></p>
                       <p id="video_topic_name"></p>
                       <p id="video_subtopic_name"></p>
                       <p id="video_views"></p>
                       <p id="video_student_subscriptions"></p>

                   </div>
               </div>

            </div>

            <div class="modal-footer">
                <p></p>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
    <script src="{{ asset('assets/backend/js/pdfobject.js') }}"></script>
    <!-- App scripts -->
    <script>
    $(function() {

        //edit data...
        $('.view_notes').click(function(e){
            e.preventDefault();
            var notes_id = $(this).attr('id');
            if(notes_id == 0){
                alert('No notes exist, please create notes under this subject');
                //call modal for creating notes...

            }else{
                var data;
                $.ajax({
                  type: "GET",
                  url: '/notes/data/get',
                  data: {notes_id: notes_id},
                  success: function( data ) {
                    console.log(data);
                    /*$("#fileviewer").attr({ src: "/uploads/files/"+data[0].notes_file });*/
                    // $("#level_name").val(data.name);
                    // $("#level_id").val(data.id);
                    $("#notes_subject_name").text(data[0].subject_name+'(subject) - '+ data[0].topic_name +'(topic) - '+ data[0].subtopic +'(sub-topic)');

                    // PDFObject.embed("{{ URL::asset( '/uploads/files/Breakdown.pdf') }}", "#example1");
                    //"{{ URL::asset( '/uploads/files/' . $note->cover_photo) }}"
                    PDFObject.embed(document.location.origin +'/uploads/files/'+ data[0].notes_file, "#example1");
                    $("#view_pdf_modal").modal();
                  },
                  error: function(data){
                    alert('failed to post data');
                  }
                });
            }
        
        });

        $("#view_pdf_modal").on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
        });

        $('.view_video').click(function(e){
            e.preventDefault();
            var video_id = $(this).attr('id');
            if(video_id == 0){
                alert('No video exist, please create a video under this subject');
                //call modal for creating videos...

            }else{
                // alert(video_id);
                var data;
                $.ajax({
                  type: "GET",
                  url: '/video/get',
                  data: {video_id: video_id},
                  success: function( data ) {
                    console.log(data);
                    $("#video_post_title").text('Title: '+ data[0].post_title);
                    $("#video_subject_name").text('Subject: '+ data[0].subject_name);
                    $("#video_topic_name").text('Topic: '+ data[0].topic_name);
                    $("#video_subtopic_name").text('Sub Topic: '+ data[0].subtopic);
                    $("#video_views").text('Views: '+ data[0].views);
                    $("#video_student_subscriptions").text('Subscriptions: '+ data[0].student_subscriptions);
                    $("#youtubeiframe").attr('src', "https://www.youtube.com/embed/"+data[0].video_file);

                    $("#modal-2").modal();
                  },
                  error: function(data){
                    alert('failed to post data');
                  }
                });
            }
        
        });



         
  });

  </script>
@endsection
