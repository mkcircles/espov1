@if(Auth::user()->user_type == 'Teacher')
  @extends('teacher.layout.teacher')
@else
  @extends('backend.layouts.master')
@endif  

@section('content')
    <div class="page-container">
      @if(Auth::user()->user_type == 'Teacher')
        @include('teacher.includes.sidebar')
      @else
        @include('backend.includes.sidebar')
      @endif  

        <div class="main-content">
          @if(Auth::user()->user_type == 'Teacher')
            @include('teacher.includes.topMenu')
          @else
            @include('backend.includes.topMenu')
          @endif  
            <ol class="breadcrumb bc-3">
                <li><a href="/teacher"><i class="fa-home"></i>Home</a></li>
                <li><a href="/teacher/notes">Notes</a></li>
                <li class="active"><strong>Uploaded Notes</strong></li>
            </ol>
            <h2>My Profile</h2>

            @if (count($errors) > 0)
                 <div class="alert alert-danger" style='margin-bottom:10px'>
                     <ul>
                         @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
            @endif
            @if(session('profile_success'))
                 <div class="alert alert-success" role="alert">
                         {{ session('profile_success') }}
                 </div>
            @endif

            @if(session('profile_error'))
               <div class="alert alert-danger" role="alert">
                         {{ session('profile_error') }}
                 </div>
            @endif

            <div class="row">
                <!-- Card Projects -->
              <div class="col-md-6">
                  <div class="card">
                      <div class="card-image">
                        <div class="row">
                          <div class="col-md-1"></div>
                          <div class="col-md-10">
                          <form>
                            <div class="form-group">
                                <label>Name</label>
                                <p>{{ $user->name }}</p>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <p>{{ $user->email }}</p>
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <p>{{ $user->role }}</p>
                            </div>
                           </form>

                          </div>
                          <div class="col-md-1"></div>
                        </div>  
                      </div>
                      
                      <div class="card-content">
                          
                      </div>
                  
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="card">
                      <div class="card-action">
                          <p>Change Password</p>
                      </div>
                      <div class="card-image">
                        <div class="row">
                          <div class="col-md-1"></div>
                          <div class="col-md-10">
                          <form action="/user/password/change" method="post">
                            <div class="form-group">
                                <label>Enter Old password</label>
                                <input type="password" class="form-control" name='oldpwd' required>
                            </div>
                            <div class="form-group">
                                <label>Enter New password</label>
                                <input type="password" class="form-control" name='newpwd' required>
                            </div>
                            <div class="form-group">
                                <label>Confirm New password</label>
                                <input type="password" class="form-control" name='confirm_newpwd' required>
                            </div>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            
                            <button type="submit" class="btn btn-primary waves-effect waves-light pull-right">Confirm</button>
                          </form>

                          </div>
                          <div class="col-md-1"></div>
                        </div>  
                      </div>
                      
                      <div class="card-content">
                          
                      </div>
                  
                  </div>
              </div>
            </div>

        </div>
    </div>

    

@endsection