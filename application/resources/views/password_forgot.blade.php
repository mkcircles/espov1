@extends('teacher.layout.teacher')
 
@section('content')

<div class="container">
    <div class="row" style="margin-top:140px">
        <div class="col-md-4 col-md-offset-4 well">
            <form role="form" action="/send/email" method="post" name="forgot_password_form">
                {{ csrf_field() }}
                
                @if(session('email_not_found_error'))
                   <div class="alert alert-danger" role="alert">
                             {{ session('email_not_found_error') }}
                     </div>
                @endif

                <fieldset>
                    <legend>Forgot Password</legend>

                    <div class="form-group">
                        <label for="name">Email</label>
                        <input type="text" name="email" placeholder="Enter email" value="" class="form-control" />
                        @if ($errors->has('email')) <p class="help-block" style="color:red">{{ $errors->first('email') }}</p> @endif
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Send" class="btn btn-primary pull-right" /><a href="/">Back</a>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>

@stop