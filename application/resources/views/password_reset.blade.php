@extends('teacher.layout.teacher')
  
@section('content')

<div class="container">
    <div class="row" style="margin-top:140px">
        <div class="col-md-4 col-md-offset-4 well">
            <form role="form" action="/reset/password" method="post" name="reset_password_form">
                {{ csrf_field() }}
                
                @if(session('pwd_match_error'))
                   <div class="alert alert-danger" role="alert">
                            {{ session('pwd_match_error') }}
                     </div>
                @endif

                <fieldset>
                    <legend>Reset Password</legend>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label for="name">New Password</label>
                        <input type="password" name="newpwd" placeholder="Enter password" value="" class="form-control" />
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label for="name">Confirm New Password</label>
                        <input type="password" name="confirm_newpwd" placeholder="confirm new password" value="" class="form-control" />
                    </div>
                    <input type="hidden" name="remembertoken" value="{{ $remembertoken }}" />

                    <div class="form-group">
                        <input type="submit" value="Reset" class="btn btn-primary pull-right" /><a href="/">Ignore</a>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>

@stop