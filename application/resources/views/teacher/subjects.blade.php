@extends('teacher.layout.teacher')

@section('styles')
  <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
@endsection

@section('content')
    <div class="page-container">
        @include('teacher.includes.sidebar')

        <div class="main-content">
            @include('teacher.includes.topMenu')

            <div class="row">
                <div class="col-sm-3">

                    <div class="tile-stats tile-white-cyan">
                        <div class="icon"><i class="entypo-paper-plane"></i></div>
                        <div class="num">83</div>

                        <h3>Registered users</h3>
                        <p>so far in our blog, and our website.</p>
                    </div>

                </div>

                <div class="col-sm-3">

                    <div class="tile-stats tile-white-purple">
                        <div class="icon"><i class="entypo-gauge"></i></div>
                        <div class="num">83</div>

                        <h3>Registered users</h3>
                        <p>so far in our blog, and our website.</p>
                    </div>

                </div>

                <div class="col-sm-3">

                    <div class="tile-stats tile-white-pink">
                        <div class="icon"><i class="entypo-mail"></i></div>
                        <div class="num">83</div>

                        <h3>Registered users</h3>
                        <p>so far in our blog, and our website.</p>
                    </div>

                </div>

                <div class="col-sm-3">

                    <div class="tile-stats tile-white-orange">
                        <div class="icon"><i class="entypo-suitcase"></i></div>
                        <div class="num">83</div>

                        <h3>Registered users</h3>
                        <p>so far in our blog, and our website.</p>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="dataTables_wrapper no-footer">
                    <table class="table table-bordered table-striped datatable dataTable no-footer" id="teachers_dashboard_datatable" role="grid" aria-describedby="table-2_info">
                        <thead>
                        <tr>
                            <td>Level</td>
                            <td>Subject</td>
                            <td>Date</td>
                        </tr>
                        </thead>

                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
  <!-- DataTables -->
  <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

  <!-- datatables btns -->
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  
  <!-- App scripts -->
  <script>
  $(function() {

      $('#teachers_dashboard_datatable').DataTable({
          processing: true,
          serverSide: true,
          dom: 'Bflrtip',
          buttons: [
              {
                  extend: 'copy',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'excel',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'pdf',
                  exportOptions: {
                      columns: [0,1]
                  }
              },
              {
                  extend: 'print',
                  exportOptions: {
                      columns: [0,1]
                  }
              }
          ],
          ajax: { 
            url:'{{ URL::asset('/teacher/subjects/data/fetch') }}'
          },
          columns: [
              { data: 'name', name: 'name' },
              { data: 'subject_name', name: 'subject_name' },
              { data: 'created_at', name: 'created_at' },
          ]
      });
         
  });
  </script>
@endsection
