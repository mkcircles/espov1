@extends('teacher.layout.teacher')

@section('styles')
    <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
@endsection

@section('content')
    <div class="page-container">
        @include('teacher.includes.sidebar')

        <div class="main-content">
            @include('teacher.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/teacher"><i class="fa-home"></i>Home</a></li>
                <li><a href="/teacher"><i class="fa-home"></i>Teacher Name</a></li>
                <li class="active"><strong>Profile</strong></li>
            </ol>

            <div class="profile-env">
                <header class="row">
                    <div class="col-sm-3">
                        <a href="#" class="profile-picture">
                            <img src="{{ asset('assets/backend/images/profile-picture.png') }}" class="img-responsive img-circle">
                        </a>
                    </div>
                    <div class="col-sm-7">
                        <ul class="profile-info-sections">
                            <li>
                                <div class="profile-name">
                                    <strong>
                                        <a href="#">Maurice Kamugisha</a>
                                        <a href="#" class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online"></a>
                                        <!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
                                    <span><a href="#">Co-Founder at Laborator</a></span>
                                </div>
                            </li>
                            <li>
                                <div class="profile-stat">
                                    <h3>643</h3>
                                    <span><a href="#">Notes Posts</a></span>
                                </div>
                            </li>

                            <li>
                                <div class="profile-stat">
                                    <h3>108</h3>
                                    <span><a href="#">Video Posts</a></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="profile-buttons">
                        </div>
                    </div>
                </header>

                <section class="profile-info-tabs">
                    <div class="row">
                        <div class="col-sm-offset-3 col-sm-4">
                            <ul class="user-details">
                                <li><a href="#"><i class="entypo-phone"></i>2567724721</a></li>
                                <li><a href="#"><i class="entypo-location"></i>Kampala, Uganda</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <ul class="user-details">
                                <li><a href="#"><i class="entypo-mail"></i>Maurice@kodeclinic.com</a></li>
                                <li><a href="#"><i class="entypo-calendar"></i>16 October</a></li>
                            </ul>
                        </div>

                    </div>
                </section>

            </div>

            <div class="row">

                <ul class="nav nav-tabs "><!-- available classes "bordered", "right-aligned" -->
                    <li class="active">
                        <a href="#home" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="entypo-home"></i></span>
                            <span class="hidden-xs">Notes</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#profile" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs"><i class="entypo-user"></i></span>
                            <span class="hidden-xs">Video</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <table class="table table-bordered table-striped datatable dataTable no-footer" id="table-2" role="grid" aria-describedby="table-2_info">
                            <thead>
                            <tr role="row">
                                <th>Post Title</th>
                                <th>Topic</th>
                                <th>Subject</th>
                                <th>Date Added</th>
                                <th>Status</th>
                                <th style="width: 200px;">Actions</th></tr>
                            </thead>

                            <tbody>

                            <tr role="row" class="odd">
                                <td class="sorting_1">Physical Quantities</td>
                                <td>Measurement Of Physical Properties</td>
                                <td>Physics</td>
                                <td>2016-02-19</td>
                                <td>Active</td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                                    <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-info"></i>View Content</a>
                                </td>
                            </tr>
                            <tr role="row" class="even">
                                <td class="sorting_1">Base Quantities</td>
                                <td>Measurement Of Physical Properties</td>
                                <td>Physics</td>
                                <td>2016-02-19</td>
                                <td>Active</td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                                    <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-doc"></i>View Content</a>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="sorting_1">Prefixes</td>
                                <td>Measurement Of Physical Properties</td>
                                <td>Physics</td>
                                <td>2016-02-19</td>
                                <td>Active</td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                                    <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-doc"></i>View Content</a>
                                </td>
                            </tr>
                            <tr role="row" class="even">
                                <td class="sorting_1">Scalar and vector quantities</td>
                                <td>Measurement Of Physical Properties</td>
                                <td>Physics</td>
                                <td>2016-02-19</td>
                                <td>Active</td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                                    <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-doc"></i>View Content</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class="tab-pane" id="profile">

                        <table class="table table-bordered table-striped datatable dataTable no-footer" id="table-2" role="grid" aria-describedby="table-2_info">
                            <thead>
                            <tr role="row">
                                <th>Post Title</th>
                                <th>Topic</th>
                                <th>Subject</th>
                                <th>Date Added</th>
                                <th>Status</th>
                                <th style="width: 200px;">Actions</th></tr>
                            </thead>

                            <tbody>

                            <tr role="row" class="odd">
                                <td class="sorting_1">Physical Quantities</td>
                                <td>Measurement Of Physical Properties</td>
                                <td>Physics</td>
                                <td>2016-02-19</td>
                                <td>Pending</td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                                    <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-video"></i>Watch Video</a>
                                </td>
                            </tr>
                            <tr role="row" class="even">
                                <td class="sorting_1">Base Quantities</td>
                                <td>Measurement Of Physical Properties</td>
                                <td>Physics</td>
                                <td>2016-02-19</td>
                                <td>Approved</td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                                    <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-video"></i>Watch Video</a>
                                </td>
                            </tr>
                            <tr role="row" class="odd">
                                <td class="sorting_1">Prefixes</td>
                                <td>Measurement Of Physical Properties</td>
                                <td>Physics</td>
                                <td>2016-02-19</td>
                                <td>Deactive</td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                                    <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-video"></i>Watch Video</a>
                                </td>
                            </tr>
                            <tr role="row" class="even">
                                <td class="sorting_1">Scalar and vector quantities</td>
                                <td>Measurement Of Physical Properties</td>
                                <td>Physics</td>
                                <td>2016-02-19</td>
                                <td>Active</td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"><i class="entypo-cancel"></i>Delete</a>
                                    <a href="#" class="btn btn-info btn-sm btn-icon icon-left"><i class="entypo-video"></i>Watch Video</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>


            </div>

        </div>
    </div>
@endsection