@extends('teacher.layout.teacher')

@section('content')
    <div class="page-container">
        @include('teacher.includes.sidebar')

        <div class="main-content">
            @include('teacher.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/teacher"><i class="fa-home"></i>Home</a></li>
                <li><a href="/teacher/videos">Videos</a></li>
                <li class="active"><strong>Uploaded Videos</strong></li>
            </ol>
            <h2>Shared Videos</h2>
            <div class="panel-options pull-right">
                <a href="javascript:;" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});" class="btn btn-green" title="Add Post" data-rel="close"><i class="entypo-plus"></i> Add Content</a>
            </div>
            
            @if (count($errors) > 0)
                 <div class="alert alert-danger" style='margin-bottom:10px'>
                     <ul>
                         @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
            @endif
            @if(session('video_success'))
                 <div class="alert alert-success" role="alert">
                         {{ session('video_success') }}
                 </div>
            @endif

            @if(session('video_error'))
               <div class="alert alert-danger" role="alert">
                         {{ session('video_error') }}
                 </div>
            @endif            

            <div class="row">

                @if(count($videos) > 0)
                @foreach($videos as $video)
                    <div class="col-sm-3">

                        <div class="tile-stats tile-white-cyan">
                            <img src="{{ URL::asset( '/uploads/files/' . $video->cover_photo) }}" class="cover_photo">
                            <h4>{{ $video->subject_name }}</h4>
                            <p>{{ $video->topic_name }}</p>
                            <h4>{{ $video->name }}</h4>
                            <p>UGX {{ $video->video_pricing }}</p>
                            <hr/>
                            <div class="pull-left">
                                Views: {{ $video->views > 0 ? $video->views:0 }}
                            </div>
                            <div class="panel-options pull-right">
                                <a href="javascript:;" title="View" class="bg view_video" id="{{ $video->id }}"><i class="entypo-eye"></i> view</a>
                                <a href="#" title="Delete" data-rel="close" class="delete_video" id="{{ $video->id }}"><i class="entypo-cancel"></i> Delete</a>
                            </div>
                        </div>

                    </div>
                @endforeach    
                @endif

            </div>

        </div>
    </div>

    <!-- Modal 2 (Custom Width)-->
    <div class="modal fade custom-width" id="modal-2">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Custom Width Modal</h4>
                </div>

                <div class="modal-body">
                   <div class="row">
                       <div class="col-md-8">
                           <iframe id="youtubeiframe" width="560" height="315" frameborder="0" allowfullscreen></iframe>
                       </div>
                       <div class="col-md-4">
                           <p id="video_post_title"></p>
                           <p id="video_subject_name"></p>
                           <p id="video_topic_name"></p>
                           <p id="video_subtopic_name"></p>
                           <p id="video_views"></p>
                           <p id="video_student_subscriptions"></p>

                       </div>
                   </div>

                </div>

                <div class="modal-footer">
                    <p></p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-6">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Register New Topic for <strong>Subject Name</strong></h4>
            </div>

            <form action="/teacher/videos/post" method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="row filter-parameters">
                        <div class="col-md-6">
                            <div class="form-group">
                                @if(count($teacherz_subjects) > 0)
                                    <label for="field-1" class="control-label">Subject</label>
                                    <select class="form-control" id="subjects" name="subject_id" required>
                                        <option selected disabled hidden>Choose Subject</option>
                                        @foreach($teacherz_subjects as $teacherz_subject)
                                            <option class='teachersubjects' id='{{ $teacherz_subject->id }}' value="{{ $teacherz_subject->id }}">{{ $teacherz_subject->subject_name }} - {{ $teacherz_subject->name }}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <b>You are not registered for any subjects</a>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Topic</label>
                                <select class="form-control" id="topics" name="topic_id" required>
                                    <option selected disabled hidden>Choose Topic</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Sub Topic</label>
                                <select class="form-control" id="subtopics" name="subtopic_id" required>
                                    <option selected disabled hidden>Choose Sub Topic</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Billable(UGX)?</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Put Price of the Notes" name="billable" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5><strong>VIDEO DESCRIPTION</strong></h5><hr/>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Post Title</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Post Title" name="post_title" required>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Requirements</label>
                                <textarea class="form-control" id="field-ta" placeholder="Topic Requirements" name="topic_requirements"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Expectations</label>
                                <textarea class="form-control" id="field-ta" placeholder="Topic expectation" name="topic_expectation"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Video Description</label>
                                <textarea class="form-control" id="field-ta" placeholder="Topic description" name="video_description" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Cover Photo</label>
                                <input type="file" class="form-control" id="field-file" placeholder="Post Cover Posts" name="cover_photo" required>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Video File</label>
                                <input type="file" class="form-control" id="field-file" placeholder="Post Cover Posts" name="video_file" required>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Questions to Attempt</label>
                                <input type="file" class="form-control" id="field-file" placeholder="Questions File" name="questions_file">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Answers</label>
                                <input type="file" class="form-control" id="field-file" placeholder="Answers File" name="answers_file">
                            </div>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        </div>


                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Save Post</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
            
@section('scripts')
  
    <!-- App scripts -->
    <script>
    $(function() {

        //edit data...
        $('.view_video').click(function(e){
            e.preventDefault();
            var video_id = $(this).attr('id');
            if(video_id == 0){
                alert('No video exist, please create a video under this subject');
                //call modal for creating videos...

            }else{
                // alert(video_id);
                var data;
                $.ajax({
                  type: "GET",
                  url: '/video/get',
                  data: {video_id: video_id},
                  success: function( data ) {
                    console.log(data);
                    $("#video_post_title").text('Title: '+ data[0].post_title);
                    $("#video_subject_name").text('Subject: '+ data[0].subject_name);
                    $("#video_topic_name").text('Topic: '+ data[0].topic_name);
                    $("#video_subtopic_name").text('Sub Topic: '+ data[0].subtopic);
                    $("#video_views").text('Views: '+ data[0].views);
                    $("#video_student_subscriptions").text('Subscriptions: '+ data[0].student_subscriptions);
                    $("#youtubeiframe").attr('src', "https://www.youtube.com/embed/"+data[0].video_file);

                    $("#modal-2").modal();
                  },
                  error: function(data){
                    alert('failed to post data');
                  }
                });
            }
        
        });

        //delete data...
        // $('#levels_datatable tbody').on('click', '#deleteLevel',function(e){
        //     e.preventDefault();
        //     var level_id = $(this).attr('delete_rowId');
        //     var data;
        //     $.ajax({
        //       type: "GET",
        //       url: '/level/get',
        //       data: {level_id: level_id},
        //       success: function( data ) {
        //         $("#delete_level_id").val(data.id);
        //         $("#level_to_delete").text(data.name);

        //         $("#delete_level").modal();
        //       },
        //       error: function(data){
        //         alert('failed to post data');
        //       }
        //     });
        
        // });
                //initial disable all the topics and subtopics...
        $('#topics').prop('disabled', true);
        $('#subtopics').prop('disabled', true);

        $('.filter-parameters').on('change','#subjects',function(){
            var subject_id = $(this).children(":selected").attr("id");
            //alert(subject_id);
            //ajax call for topics...
            $.ajax({
              type: "GET",
              url: '/teacher/subject/topics/get',
              data: {subject_id: subject_id},
              success: function( data ) {
                //check if data is not empty...
                console.log(data);
                //first clear all the options
                $('#topics').find('option').remove().end().append('<option selected disabled hidden>Choose Topic</option>');
          
                $.each(data, function(i, value) {
                    $('#topics').append($('<option>').text(value.topic_name).attr('value', value.id).attr('id', value.id));
                    $('#topics').prop('disabled', false);
                });
              },
              error: function(data){
                alert('failed to fetch topics, please ensure you have registered topics');
              }
            });
        });

        $('.filter-parameters').on('change','#topics',function(){
            var topic_id = $(this).children(":selected").attr("id");
            //alert(topic_id);
            //ajax call for topics...
            $.ajax({
              type: "GET",
              url: '/teacher/subject/topics/subtopics/get',
              data: {topic_id: topic_id},
              success: function( data ) {
                //check if data is not empty...
                console.log(data);
                //first clear all the options
                $('#subtopics').find('option').remove().end().append('<option selected disabled hidden>Choose SubTopic</option>');
          
                $.each(data, function(i, value) {
                    $('#subtopics').append($('<option>').text(value.subtopic).attr('value', value.id).attr('id', value.id));
                    $('#subtopics').prop('disabled', false);
                });
              },
              error: function(data){
                alert('failed to fetch topics, please ensure you have registered topics');
              }
            });
        });
         
  });
  </script>
@endsection