<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="/teacher">
                    <img src="{{ asset('assets/backend/images/logo@2x.png') }}" width="120" alt="" />
                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
            <div class="sidebar-user-info">

                <div class="sui-normal">
                    <a href="#" class="user-link">
                        <img src="{{ URL::asset('/uploads/files/'.Auth::user()->profile_pic ) }}" width="55" alt="" class="img-circle" />

                        <span>Welcome,</span>
                        <strong>{{ Auth::user()->name }}</strong>
                    </a>
                </div>

                <div class="sui-hover inline-links animate-in"><!-- You can remove "inline-links" class to make links appear vertically, class "animate-in" will make A elements animateable when click on user profile -->
                    <a href="#">
                        <i class="entypo-pencil"></i>
                        New Page
                    </a>

                    <a href="mailbox.html">
                        <i class="entypo-mail"></i>
                        Inbox
                    </a>

                    <a href="/logout">
                        <i class="entypo-lock"></i>
                        Log Off
                    </a>

                    <span class="close-sui-popup">�</span><!-- this is mandatory -->				</div>
            </div>

            <li><a href="/teacher"><i class="entypo-home"></i><span class="title">Dashboard</span></a></li>
            <li><a href="/teacher/subjects"><i class="entypo-docs"></i><span class="title">Your Subjects</span></a></li>
            <li class="has-sub">
                <a href="/teacher/subjects"><i class="entypo-clipboard"></i><span class="title">Uploaded Content</span></a>
                <ul>
                    <li><a href="/teacher/notes"><span class="title">Subject Notes</span></a></li>
                    <li><a href="/teacher/videos"><span class="title">Subject Videos</span></a></li>
                </ul>
            </li>
            <li><a href="/user/profile"><i class="entypo-docs"></i><span class="title">My Profile</span></a></li>

        </ul>

    </div>

</div>