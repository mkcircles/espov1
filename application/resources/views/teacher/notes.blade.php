@extends('teacher.layout.teacher')

@section('styles')
    <style>
    .pdfobject-container { height: 500px;}
    .pdfobject { border: 1px solid #666; }
    </style>
@endsection

@section('content')
    <div class="page-container">
        @include('teacher.includes.sidebar')

        <div class="main-content">
            @include('teacher.includes.topMenu')
            <ol class="breadcrumb bc-3">
                <li><a href="/teacher"><i class="fa-home"></i>Home</a></li>
                <li><a href="/teacher/notes">Notes</a></li>
                <li class="active"><strong>Uploaded Notes</strong></li>
            </ol>
            <h2>Uploaded Notes</h2>
            <div class="panel-options pull-right">
                <a href="javascript:;" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});" class="btn btn-green" title="Add Post" data-rel="close"><i class="entypo-plus"></i> Add Content</a>
            </div>

            @if (count($errors) > 0)
                 <div class="alert alert-danger" style='margin-bottom:10px'>
                     <ul>
                         @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
            @endif
            @if(session('notes_success'))
                 <div class="alert alert-success" role="alert">
                         {{ session('notes_success') }}
                 </div>
            @endif

            @if(session('notes_error'))
               <div class="alert alert-danger" role="alert">
                         {{ session('notes_error') }}
                 </div>
            @endif

            <div class="row">

                @if(count($notes) > 0)
                @foreach($notes as $note)
                    <div class="col-sm-3">

                        <div class="tile-stats tile-white-cyan">
                          <img src="{{ URL::asset( '/uploads/files/' . $note->cover_photo) }}" class="cover_photo">
                            <h4>{{ $note->name }}</h4>
                            <h4>{{ $note->subject_name }}</h4>
                            <p>{{ $note->topic_name }}</p>
                            <p>UGX {{ $note->notes_pricing }}</p>
                            Post Title:<strong>{{ $note->post_title }}</strong>
                            <hr/>
                            <div class="pull-left">
                                Views: {{ $note->views > 0 ? $note->views:0 }}
                            </div>
                            <div class="panel-options pull-right">
                                
                                <a href="test2.pdf" data-toggle="modal" class="view_notes" id="{{ $note->id }}" data-target="#sample-modal-dialog-1" title="View" class="bg"><i class="entypo-eye"></i> view</a>
                                <a href="{{ URL::asset( '/uploads/files/' . $note->notes_file)  }}" download="{{$note->notes_file}}">Download</a>
                                <a href="#" title="Delete" data-rel="close" class="delete_notes" id="{{ $note->id }}"><i class="entypo-cancel"></i> Delete</a>
                            </div>
                        </div>

                    </div>
                @endforeach    
                @endif
            </div>

        </div>
    </div>

    <!-- Register new notes -->
    <div class="modal fade" id="modal-6">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Register New Topic for <strong>Subject Name</strong></h4>
            </div>

            <form action="/teacher/notes/post" method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="row filter-notes">
                        <div class="col-md-6">
                            <div class="form-group">
                                @if(count($teacherz_subjects) > 0)
                                    <label for="field-1" class="control-label">Subject</label>
                                    <select class="form-control" id="subjects" name="subject_id" required>
                                        <option selected disabled hidden>Choose Subject</option>
                                        @foreach($teacherz_subjects as $teacherz_subject)
                                            <option class='teachersubjects' id='{{ $teacherz_subject->id }}' value="{{ $teacherz_subject->id }}">{{ $teacherz_subject->subject_name }} - {{ $teacherz_subject->name }}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <b>You are not registered for any subjects</a>
                                @endif    
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Topic</label>
                                <select class="form-control" id="topics" name="topic_id" required>
                                    <option selected disabled hidden>Choose Topic</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Sub Topic</label>
                                <select class="form-control" id="subtopics" name="subtopic_id" required>
                                    <option selected disabled hidden>Choose Sub Topic</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Billable(UGX)?</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Put Price of the Notes" name="billable" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5><strong>NOTES DESCRIPTION</strong></h5><hr/>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Post Title</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Post Title" name="post_title" required>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Requirements</label>
                                <textarea class="form-control" id="field-ta" placeholder="Topic Requirements" name="topic_requirements"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Expectations</label>
                                <textarea class="form-control" id="field-ta" placeholder="Topic expectation" name="topic_expectation"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Notes Description</label>
                                <textarea class="form-control" id="field-ta" placeholder="Topic description" name="notes_description" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Cover Photo</label>
                                <input type="file" class="form-control" id="field-file" placeholder="Post Cover Posts" name="cover_photo" required>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Notes File</label>
                                <input type="file" class="form-control" id="field-file" placeholder="Notes File" name="notes_file" required>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Questions to Attempt</label>
                                <input type="file" class="form-control" id="field-file" placeholder="Questions File" name="questions_file">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Answers</label>
                                <input type="file" class="form-control" id="field-file" placeholder="Answers File" name="answers_file">
                            </div>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        </div>


                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Upload Notes</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- viewing pdf -->
<div class="modal fade" id="view_pdf_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Viewing pdf for <strong id="notes_subject_name"></strong></h4>
            </div>

            <form action="/teacher/notes/post" method="post" enctype="multipart/form-data">

                <div class="modal-body">

                    <div class="row">
                        <div id="example1"></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Delete Pdf</button>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/backend/js/pdfobject.js') }}"></script>
    <!-- App scripts -->
    <script>
    $(function() {

        //edit data...
        $('.view_notes').click(function(e){
            e.preventDefault();
            var notes_id = $(this).attr('id');
            if(notes_id == 0){
                alert('No notes exist, please create notes under this subject');
                //call modal for creating notes...

            }else{
                var data;
                $.ajax({
                  type: "GET",
                  url: '/notes/data/get',
                  data: {notes_id: notes_id},
                  success: function( data ) {
                    console.log(data);
                    /*$("#fileviewer").attr({ src: "/uploads/files/"+data[0].notes_file });*/
                    // $("#level_name").val(data.name);
                    // $("#level_id").val(data.id);
                    $("#notes_subject_name").text(data[0].subject_name+'(subject) - '+ data[0].topic_name +'(topic) - '+ data[0].subtopic +'(sub-topic)');

                    // PDFObject.embed("{{ URL::asset( '/uploads/files/Breakdown.pdf') }}", "#example1");
                    //"{{ URL::asset( '/uploads/files/' . $note->cover_photo) }}"
                    PDFObject.embed(document.location.origin +'/uploads/files/'+ data[0].notes_file, "#example1");
                    $("#view_pdf_modal").modal();
                  },
                  error: function(data){
                    alert('failed to post data');
                  }
                });
            }
        
        });

        $("#view_pdf_modal").on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
        });

        //initial disable all the topics and subtopics...
        $('#topics').prop('disabled', true);
        $('#subtopics').prop('disabled', true);

        $('.filter-notes').on('change','#subjects', function(){
            var subject_id = $(this).children(":selected").attr("id");
            //alert(subject_id);
            //ajax call for topics...
            $.ajax({
              type: "GET",
              url: '/teacher/subject/topics/get',
              data: {subject_id: subject_id},
              success: function( data ) {
                //check if data is not empty...
                console.log(data);
                //first clear all the options
                $('#topics').find('option').remove().end().append('<option selected disabled hidden>Choose Topic</option>');
          
                $.each(data, function(i, value) {
                    $('#topics').append($('<option>').text(value.topic_name).attr('value', value.id).attr('id', value.id));
                    $('#topics').prop('disabled', false);
                });
              },
              error: function(data){
                alert('failed to fetch topics, please ensure you have registered topics');
              }
            });
        });

        $('.filter-notes').on('change', '#topics', function(){
            var topic_id = $(this).children(":selected").attr("id");
            //alert(topic_id);
            //ajax call for topics...
            $.ajax({
              type: "GET",
              url: '/teacher/subject/topics/subtopics/get',
              data: {topic_id: topic_id},
              success: function( data ) {
                //check if data is not empty...
                console.log(data);
                //first clear all the options
                $('#subtopics').find('option').remove().end().append('<option selected disabled hidden>Choose SubTopic</option>');
          
                $.each(data, function(i, value) {
                    $('#subtopics').append($('<option>').text(value.subtopic).attr('value', value.id).attr('id', value.id));
                    $('#subtopics').prop('disabled', false);
                });
              },
              error: function(data){
                alert('failed to fetch topics, please ensure you have registered topics');
              }
            });
        });
         
  });

  </script>
@endsection
