@extends('frontend.layouts.front')
@section('meta')
    <title>{{ $subject->subject_name }} - Espo Uganda</title>
    <meta name="description" content="Connect with thousands of other learners and debate ideas, discuss course material, and get help mastering concepts." />
    <meta name="keywords" content="Education, Study, videos, notes, presentations, teachers" />
    <meta property="og:url"  content="http://espo.ug">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{ $subject->subject_name }} - Find Simplified educative content for all school levels, Videos, Notes-Uganda -www.espo.ug">
    <meta property="og:image" content="http://i.imgur.com/DjkYOto.jpg"/>
    <meta property="og:description" content="Each course is like an interactive textbook, featuring pre-recorded videos, quizzes, and projects.">
    <meta property="og:site_name" content="ESPO Uganda">
    <meta property="article:author" content="https://www.facebook.com/espoug">

    <meta property="article:section" content="Uganda Education" />
    <meta property="article:tag" content="Uganda Education Videos and Notes" />

@endsection

@section('main')

    <div class="main">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="javascript:;">Notes</a></li>
                <li class="active">Uploaded Notes</li>
            </ul>
            <div class="row margin-bottom-40">
                <!-- BEGIN CONTENT -->
                <div class="col-md-3">
                    @include('frontend.layouts.refineSearch')
                </div>
                <div class="col-md-9 col-sm-12">
                        @include('frontend.layouts.searchDiv')


                    <div class="content-page">
                        @foreach($posts as $note)

                            <div class="owl-item col-md-3 col-sm-4">

                                <div class="recent-work-item">
                                    @if($note->has_video==1)
                                        <a class="recent-work-description" href="/videos/{{ $note->id }}">
                                            @else
                                                <a class="recent-work-description" href="/notes/{{ $note->id }}">
                                                    @endif
                                                    <em><img src="{{ asset('uploads/files/'.$note->cover_photo) }}"
                                                             alt="Amazing Project"
                                                             style="width: 100%; height: 161px; overflow: hidden"
                                                             class="img-responsive"></em>
                                                    <span class="subject">{{ $note->subject_name }} {{$note->has_video}}</span>
                                                    <h5 class="post-title">{{ $note->post_title }}</h5>

                                                    <p>Topic goes here.</p>
                                                    <ul class="blog-info">
                                                        <li><i class="fa fa-calendar"></i> {{ $note->created_at }}</li>
                                                        <li><i class="fa fa-user"></i> Niwogaba Joel.</li>
                                                    </ul>

                                                </a>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>

            </div>
            <!-- END CONTENT -->
        </div>

    </div>
    </div>


@endsection