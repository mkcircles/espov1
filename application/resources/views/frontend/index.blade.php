@extends('frontend.layouts.front')
@section('meta')
    <meta name="description" content="Connect with thousands of other learners and debate ideas, discuss course material, and get help mastering concepts." />
    <meta name="keywords" content="Education, Study, videos, notes, presentations, teachers" />
    <meta property="og:url"  content="http://espo.ug">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Find Simplified educative content for all school levels, Videos, Notes-Uganda -www.espo.ug">
    <meta property="og:image" content="http://i.imgur.com/DjkYOto.jpg"/>
    <meta property="og:description" content="Each course is like an interactive textbook, featuring pre-recorded videos, quizzes, and projects.">
    <meta property="og:site_name" content="ESPO Uganda">
    <meta property="article:author" content="https://www.facebook.com/espoug">

    <meta property="article:section" content="Uganda Education" />
    <meta property="article:tag" content="Uganda Education Videos and Notes" />


    @endsection
    @section('main')
            <!-- BEGIN SLIDER -->
    <div class="page-slider margin-bottom-40">
        <div class="fullwidthbanner-container revolution-slider">
            <div class="fullwidthabnner">
                <ul id="revolutionul">
                    <!-- THE NEW SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="../../assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="../../assets/frontend/pages/img/revolutionslider/bg9.jpg" alt="">

                        <div class="caption lft slide_title_white slide_item_left"
                             data-x="30"
                             data-y="90"
                             data-speed="400"
                             data-start="1500"
                             data-easing="easeOutExpo">
                            Explore the Power<br><span class="slide_title_white_bold">of ESPO</span>
                        </div>
                        <div class="caption lft slide_subtitle_white slide_item_left"
                             data-x="87"
                             data-y="245"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutExpo">
                            Find the best Educative Notes & Videos Here!
                        </div>

                        <div class="caption lfb"
                             data-x="640"
                             data-y="0"
                             data-speed="700"
                             data-start="1000"
                             data-easing="easeOutExpo">
                            <img src="../../assets/frontend/pages/img/revolutionslider/lady.png" alt="Image 1">
                        </div>
                    </li>

                    <!-- THE FIRST SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="../../assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="../../assets/frontend/pages/img/revolutionslider/bg1.jpg" alt="">

                        <div class="caption lft slide_title slide_item_left"
                             data-x="30"
                             data-y="105"
                             data-speed="400"
                             data-start="1500"
                             data-easing="easeOutExpo">
                            Need a website design?
                        </div>
                        <div class="caption lft slide_subtitle slide_item_left"
                             data-x="30"
                             data-y="180"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutExpo">
                            This is what you were looking for
                        </div>
                        <div class="caption lft slide_desc slide_item_left"
                             data-x="30"
                             data-y="220"
                             data-speed="400"
                             data-start="2500"
                             data-easing="easeOutExpo">
                            Lorem ipsum dolor sit amet, dolore eiusmod<br> quis tempor incididunt. Sed unde omnis iste.
                        </div>
                        <a class="caption lft btn green slide_btn slide_item_left"
                           href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
                           data-x="30"
                           data-y="290"
                           data-speed="400"
                           data-start="3000"
                           data-easing="easeOutExpo">
                            Purchase Now!
                        </a>

                        <div class="caption lfb"
                             data-x="640"
                             data-y="55"
                             data-speed="700"
                             data-start="1000"
                             data-easing="easeOutExpo">
                            <img src="../../assets/frontend/pages/img/revolutionslider/man-winner.png" alt="Image 1">
                        </div>
                    </li>

                    <!-- THE SECOND SLIDE -->
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400"
                        data-thumb="../../assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
                        <img src="../../assets/frontend/pages/img/revolutionslider/bg2.jpg" alt="">

                        <div class="caption lfl slide_title slide_item_left"
                             data-x="30"
                             data-y="125"
                             data-speed="400"
                             data-start="3500"
                             data-easing="easeOutExpo">
                            Powerfull &amp; Clean
                        </div>
                        <div class="caption lfl slide_subtitle slide_item_left"
                             data-x="30"
                             data-y="200"
                             data-speed="400"
                             data-start="4000"
                             data-easing="easeOutExpo">
                            Responsive Admin &amp; Website Theme
                        </div>
                        <div class="caption lfl slide_desc slide_item_left"
                             data-x="30"
                             data-y="245"
                             data-speed="400"
                             data-start="4500"
                             data-easing="easeOutExpo">
                            Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                        </div>
                        <div class="caption lfr slide_item_right"
                             data-x="635"
                             data-y="105"
                             data-speed="1200"
                             data-start="1500"
                             data-easing="easeOutBack">
                            <img src="../../assets/frontend/pages/img/revolutionslider/mac.png" alt="Image 1">
                        </div>
                        <div class="caption lfr slide_item_right"
                             data-x="580"
                             data-y="245"
                             data-speed="1200"
                             data-start="2000"
                             data-easing="easeOutBack">
                            <img src="../../assets/frontend/pages/img/revolutionslider/ipad.png" alt="Image 1">
                        </div>
                        <div class="caption lfr slide_item_right"
                             data-x="735"
                             data-y="290"
                             data-speed="1200"
                             data-start="2500"
                             data-easing="easeOutBack">
                            <img src="../../assets/frontend/pages/img/revolutionslider/iphone.png" alt="Image 1">
                        </div>
                        <div class="caption lfr slide_item_right"
                             data-x="835"
                             data-y="230"
                             data-speed="1200"
                             data-start="3000"
                             data-easing="easeOutBack">
                            <img src="../../assets/frontend/pages/img/revolutionslider/macbook.png" alt="Image 1">
                        </div>
                        <div class="caption lft slide_item_right"
                             data-x="865"
                             data-y="45"
                             data-speed="500"
                             data-start="5000"
                             data-easing="easeOutBack">
                            <img src="../../assets/frontend/pages/img/revolutionslider/hint1-red.png" id="rev-hint1"
                                 alt="Image 1">
                        </div>
                        <div class="caption lfb slide_item_right"
                             data-x="355"
                             data-y="355"
                             data-speed="500"
                             data-start="5500"
                             data-easing="easeOutBack">
                            <img src="../../assets/frontend/pages/img/revolutionslider/hint2-red.png" id="rev-hint2"
                                 alt="Image 1">
                        </div>
                    </li>

                    <!-- THE THIRD SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="../../assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
                        <img src="../../assets/frontend/pages/img/revolutionslider/bg3.jpg" alt="">

                        <div class="caption lfl slide_item_left"
                             data-x="30"
                             data-y="95"
                             data-speed="400"
                             data-start="1500"
                             data-easing="easeOutBack">
                            <iframe src="http://player.vimeo.com/video/56974716?portrait=0" width="420" height="240"
                                    style="border:0" allowFullScreen></iframe>
                        </div>
                        <div class="caption lfr slide_title"
                             data-x="470"
                             data-y="100"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutExpo">
                            Responsive Video Support
                        </div>
                        <div class="caption lfr slide_subtitle"
                             data-x="470"
                             data-y="170"
                             data-speed="400"
                             data-start="2500"
                             data-easing="easeOutExpo">
                            Youtube, Vimeo and others.
                        </div>
                        <div class="caption lfr slide_desc"
                             data-x="470"
                             data-y="220"
                             data-speed="400"
                             data-start="3000"
                             data-easing="easeOutExpo">
                            Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                        </div>
                        <a class="caption lfr btn yellow slide_btn" href=""
                           data-x="470"
                           data-y="280"
                           data-speed="400"
                           data-start="3500"
                           data-easing="easeOutExpo">
                            Watch more Videos!
                        </a>
                    </li>

                    <!-- THE FORTH SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="../../assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="../../assets/frontend/pages/img/revolutionslider/bg4.jpg" alt="">

                        <div class="caption lft slide_title"
                             data-x="30"
                             data-y="105"
                             data-speed="400"
                             data-start="1500"
                             data-easing="easeOutExpo">
                            What else included ?
                        </div>
                        <div class="caption lft slide_subtitle"
                             data-x="30"
                             data-y="180"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutExpo">
                            The Most Complete Admin Theme
                        </div>
                        <div class="caption lft slide_desc"
                             data-x="30"
                             data-y="225"
                             data-speed="400"
                             data-start="2500"
                             data-easing="easeOutExpo">
                            Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                        </div>
                        <a class="caption lft slide_btn btn red slide_item_left"
                           href="http://www.keenthemes.com/preview/index.php?theme=metronic_admin" target="_blank"
                           data-x="30"
                           data-y="300"
                           data-speed="400"
                           data-start="3000"
                           data-easing="easeOutExpo">
                            Learn More!
                        </a>

                        <div class="caption lft start"
                             data-x="670"
                             data-y="55"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutBack">
                            <img src="../../assets/frontend/pages/img/revolutionslider/iphone_left.png" alt="Image 2">
                        </div>

                        <div class="caption lft start"
                             data-x="850"
                             data-y="55"
                             data-speed="400"
                             data-start="2400"
                             data-easing="easeOutBack">
                            <img src="../../assets/frontend/pages/img/revolutionslider/iphone_right.png" alt="Image 3">
                        </div>
                    </li>
                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
            </div>
        </div>
    </div>
    <!-- END SLIDER -->

    <div class="main">
        <div class="container">
            <!-- BEGIN SERVICE BOX -->
            <div class="row service-box margin-bottom-40">
                <div class="col-md-4 col-sm-4">
                    <div class="service-box-heading">
                        <em><i class="fa fa-location-arrow blue"></i></em>
                        <span>Coursework</span>
                    </div>
                    <p>Each course is like an interactive textbook, featuring pre-recorded videos, quizzes, and
                        projects.</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="service-box-heading">
                        <em><i class="fa fa-check red"></i></em>
                        <span>Help & Support</span>
                    </div>
                    <p>Connect with thousands of other learners and debate ideas, discuss course material, and get help
                        mastering concepts.</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="service-box-heading">
                        <em><i class="fa fa-compress green"></i></em>
                        <span>Certificates</span>
                    </div>
                    <p>Earn official recognition for your work, and share your success with friends, colleagues, and
                        employers.</p>
                </div>
            </div>
            <!-- END SERVICE BOX -->

            <!-- BEGIN RECENT WORKS -->
            <div class="row recent-work margin-bottom-40">
                <div class="margin-bottom-20" style="height: 30px">
                    <h3 class="text-center margin-bottom-20 pull-left">Recently Published Notes</h3>
                    <a href="/notes" class="btn btn-sm btn-default pull-right">
                        <i class="fa fa-file-o"></i> View All Notes
                    </a>
                </div>

                <div class="row">
                    @foreach($notes as $note)
                        <div class="owl-item col-md-3 col-sm-4">

                            <div class="home-item">
                                <a class="recent-work-description" href="/notes/{{ $note->id }}">
                                    <em><img src="{{ asset('uploads/files/'.$note->cover_photo) }}"
                                             alt="{{ $note->post_title }}"
                                             style="width: 100%; height: 161px; overflow: hidden"
                                             class="img-responsive"></em>
                                    <span class="subject">{{ $note->subject_name }}</span>
                                    <h5 class="post-title">{{ $note->post_title }}</h5>

                                    <p>{{ $note->topic_name }}</p>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
            <!-- BEGIN RECENT WORKS -->

            <!-- BEGIN RECENT WORKS -->
            <div class="row recent-work margin-bottom-20">
                <div class="margin-bottom-20" style="height: 30px">
                    <h3 class="text-center margin-bottom-20 pull-left">Recently Published Videos</h3>
                    <a href="/videos" class="btn btn-sm btn-default pull-right">
                        <i class="fa fa-file-o"></i> View All Videos
                    </a>
                </div>

                <div class="row">
                    @foreach($videos as $video)
                        <div class="owl-item col-md-3 col-sm-4">

                            <div class="home-item">
                                <a class="recent-work-description" href="/videos/{{ $video->id }}">
                                    <em><img src="{{ asset('uploads/files/'.$video->cover_photo) }}"
                                             alt="{{ $video->post_title }}"
                                             style="width: 100%; height: 161px; overflow: hidden"
                                             class="img-responsive"></em>
                                    <span class="subject">{{ $video->subject_name }}</span>
                                    <h5 class="post-title">{{ $video->post_title }}</h5>

                                    <p>{{ $video->topic_name }}</p>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- BEGIN RECENT WORKS -->
            <div class="row recent-work margin-bottom-40">
                <a href="/courses" class="btn btn-sm blue pull-right">
                    <i class="fa fa-file-o"></i> View All Courses
                </a>

                <h3 class="text-center margin-bottom-20">Latest Posts</h3>

                @foreach($subjects as $subject)
                    <div class="col-md-3 course-card">
                        <a href="/subject/{{ $subject->id }}">
                            <div class="row">
                                <div class="col-md-4 subImg"
                                     style="background-image: url('{{ asset('uploads/files/images.jpg') }}')"></div>
                                <div class="col-md-8">
                                    <span class="subName">{{ $subject->subject_name }}</span>
                                    <span class="sublevel">{{ $subject->name }}</span>

                                    <div class="sub-count" style="margin-top: 20px">
                                        <div class="price-and-count-info">
                                            <span class="course-count color-secondary-text"><span>5 Videos</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                    </div>
                @endforeach
            </div>


            <h2 class="center">Recent Courses</h2>

            <!-- BEGIN RECENT WORKS -->
            <div class="row recent-work margin-bottom-40">
                <div class="col-md-3">
                    <h3><a href="portfolio.html">Recent Works</a></h3>

                    <p>Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde
                        voluptatem. Sed unde omnis iste natus error sit voluptatem.</p>
                </div>
                <div class="col-md-9">
                    <div class="owl-carousel owl-carousel3">
                        <div class="recent-work-item">
                            <em>
                                <img src="../../assets/frontend/pages/img/works/img1.jpg" alt="Amazing Project"
                                     class="img-responsive">
                                <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                                <a href="../../assets/frontend/pages/img/works/img1.jpg" class="fancybox-button"
                                   title="Project Name #1" data-rel="fancybox-button"><i
                                            class="fa fa-search"></i></a>
                            </em>
                            <a class="recent-work-description" href="javascript:;">
                                <strong>Amazing Project</strong>
                                <b>Agenda corp.</b>
                            </a>
                        </div>
                        <div class="recent-work-item">
                            <em>
                                <img src="../../assets/frontend/pages/img/works/img2.jpg" alt="Amazing Project"
                                     class="img-responsive">
                                <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                                <a href="../../assets/frontend/pages/img/works/img2.jpg" class="fancybox-button"
                                   title="Project Name #2" data-rel="fancybox-button"><i
                                            class="fa fa-search"></i></a>
                            </em>
                            <a class="recent-work-description" href="javascript:;">
                                <strong>Amazing Project</strong>
                                <b>Agenda corp.</b>
                            </a>
                        </div>
                        <div class="recent-work-item">
                            <em>
                                <img src="../../assets/frontend/pages/img/works/img3.jpg" alt="Amazing Project"
                                     class="img-responsive">
                                <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                                <a href="../../assets/frontend/pages/img/works/img3.jpg" class="fancybox-button"
                                   title="Project Name #3" data-rel="fancybox-button"><i
                                            class="fa fa-search"></i></a>
                            </em>
                            <a class="recent-work-description" href="javascript:;">
                                <strong>Amazing Project</strong>
                                <b>Agenda corp.</b>
                            </a>
                        </div>
                        <div class="recent-work-item">
                            <em>
                                <img src="../../assets/frontend/pages/img/works/img4.jpg" alt="Amazing Project"
                                     class="img-responsive">
                                <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                                <a href="../../assets/frontend/pages/img/works/img4.jpg" class="fancybox-button"
                                   title="Project Name #4" data-rel="fancybox-button"><i
                                            class="fa fa-search"></i></a>
                            </em>
                            <a class="recent-work-description" href="javascript:;">
                                <strong>Amazing Project</strong>
                                <b>Agenda corp.</b>
                            </a>
                        </div>
                        <div class="recent-work-item">
                            <em>
                                <img src="../../assets/frontend/pages/img/works/img5.jpg" alt="Amazing Project"
                                     class="img-responsive">
                                <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                                <a href="../../assets/frontend/pages/img/works/img5.jpg" class="fancybox-button"
                                   title="Project Name #5" data-rel="fancybox-button"><i
                                            class="fa fa-search"></i></a>
                            </em>
                            <a class="recent-work-description" href="javascript:;">
                                <strong>Amazing Project</strong>
                                <b>Agenda corp.</b>
                            </a>
                        </div>
                        <div class="recent-work-item">
                            <em>
                                <img src="../../assets/frontend/pages/img/works/img6.jpg" alt="Amazing Project"
                                     class="img-responsive">
                                <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                                <a href="../../assets/frontend/pages/img/works/img6.jpg" class="fancybox-button"
                                   title="Project Name #6" data-rel="fancybox-button"><i
                                            class="fa fa-search"></i></a>
                            </em>
                            <a class="recent-work-description" href="javascript:;">
                                <strong>Amazing Project</strong>
                                <b>Agenda corp.</b>
                            </a>
                        </div>
                        <div class="recent-work-item">
                            <em>
                                <img src="../../assets/frontend/pages/img/works/img3.jpg" alt="Amazing Project"
                                     class="img-responsive">
                                <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                                <a href="../../assets/frontend/pages/img/works/img3.jpg" class="fancybox-button"
                                   title="Project Name #3" data-rel="fancybox-button"><i
                                            class="fa fa-search"></i></a>
                            </em>
                            <a class="recent-work-description" href="javascript:;">
                                <strong>Amazing Project</strong>
                                <b>Agenda corp.</b>
                            </a>
                        </div>
                        <div class="recent-work-item">
                            <em>
                                <img src="../../assets/frontend/pages/img/works/img4.jpg" alt="Amazing Project"
                                     class="img-responsive">
                                <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                                <a href="../../assets/frontend/pages/img/works/img4.jpg" class="fancybox-button"
                                   title="Project Name #4" data-rel="fancybox-button"><i
                                            class="fa fa-search"></i></a>
                            </em>
                            <a class="recent-work-description" href="javascript:;">
                                <strong>Amazing Project</strong>
                                <b>Agenda corp.</b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END RECENT WORKS -->
        </div>
    </div>


@endsection