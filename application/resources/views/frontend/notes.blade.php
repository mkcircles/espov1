@extends('frontend.layouts.front')
@section('meta')
    <meta content="Metronic Shop UI description" name="description">
    <meta content="Metronic Shop UI keywords" name="keywords">
    <meta content="keenthemes" name="author">

    <meta property="og:site_name" content="-CUSTOMER VALUE-">
    <meta property="og:title" content="-CUSTOMER VALUE-">
    <meta property="og:description" content="-CUSTOMER VALUE-">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">
@endsection

@section('main')

    <div class="main">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/notes">Notes</a></li>
                <li class="active">Uploaded Notes</li>
            </ul>
            <div class="row margin-bottom-40">
                <!-- BEGIN CONTENT -->
                <div class="col-md-12 col-sm-12">
                    <h1>Uploaded Notes</h1>

                    <div class="content-page">
                        @if(session('notes_error'))
                            <div id="prefix_1431958428926" class="Metronic-alerts alert alert-danger fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>{{ session('notes_error') }}</div>
                        @endif

                        @foreach($notes as $note)
                            <div class="owl-item col-md-3 col-sm-4">

                                <div class="recent-work-item">
                                    <a class="recent-work-description" href="/notes/{{ $note->id }}">
                                        <em><img src="{{ asset('uploads/files/'.$note->cover_photo) }}"
                                                 alt="{{ $note->post_title }}" style="width: 100%; height: 161px; overflow: hidden" class="img-responsive"></em>
                                        <span class="subject">{{ $note->subject_name }}</span>
                                        <h5 class="post-title">{{ $note->post_title }}</h5>

                                        <p>{{ $note->topic_name }}</p>
                                        <ul class="blog-info">
                                            <li><i class="fa fa-calendar"></i> {{ $note->created_at }}</li>
                                            <li><i class="fa fa-user"></i> {{ $note->name }}</li>
                                        </ul>

                                    </a>
                                </div>
                            </div>
                        @endforeach
                            <div class="clearfix"></div>
                            <div class="row pull-right">
                                {{ $notes->links() }}
                            </div>





                    </div>
                </div>

            </div>
            <!-- END CONTENT -->
        </div>

    </div>
    </div>


@endsection
