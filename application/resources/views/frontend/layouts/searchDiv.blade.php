@if(isset($search))
    <div class="content-search margin-bottom-20">
        <div class="row">
            <div class="col-md-7">
                <h4>Search result for <em>{{$search}}</em></h4>
            </div>
            <div class="col-md-5">
                <form action="#">
                    <div class="input-group">
                        <input type="text" placeholder="Search again" class="form-control">
                      <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">Search</button>
                      </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-md-7">
            <h3>Available Courses</h3>
        </div>
        <div class="col-md-5 text-right">
            <form action="#">
                <div class="input-group">
                    <input type="text" placeholder="Search again" class="form-control">
                      <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">Search</button>
                      </span>
                </div>
            </form>
            <!-- /input-group -->
        </div>
    </div>
@endif