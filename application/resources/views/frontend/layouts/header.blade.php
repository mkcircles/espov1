<!-- BEGIN TOP BAR -->
<div class="pre-header">
    <div class="container">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->
            @if(Auth::check())
            <div class="col-md-6 col-sm-6 additional-shop-info">
                <ul class="list-unstyled list-inline">
                    <li><i class="fa fa-phone"></i><span>{{ Auth::user()->contact }}</span></li>
                    <li><i class="fa fa-envelope-o"></i><span>{{ Auth::user()->email }}</span></li>
                </ul>
            </div>
            @endif
            <!-- END TOP BAR LEFT PART -->
            <!-- BEGIN TOP BAR MENU -->
            @if(Auth::check())
            <div class="col-md-6 col-sm-6 additional-nav">
                <ul class="list-unstyled list-inline pull-right">
                    <li><a href="#">My Account</a></li>
                    <li><a href="#">Logout</a></li>
                </ul>
            </div>
            @else
            <div class="col-md-6 col-sm-6 additional-nav">
                <ul class="list-unstyled list-inline pull-right">
                    <li id="login"><a >Log In</a></li>
                    <li id="register"><a >Registration</a></li>
                </ul>
            </div>
            @endif
            <!-- END TOP BAR MENU -->
        </div>
    </div>
</div>
<!-- END TOP BAR -->
<!-- BEGIN HEADER -->
<div class="header">
    <div class="container">
        <a class="site-logo" href="/"><img src="../../assets/frontend/layout/img/logos/espo-logo.png"
                                           style="width: 80px" alt="Metronic FrontEnd"></a>

        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation pull-right font-transform-inherit">
            <ul>
                <li class="dropdown active">
                    <a class="dropdown-toggle" href="/">Home</a></li>
                <li class="dropdown dropdown-megamenu">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
                        Covered Subjects</a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="header-navigation-content">
                                <div class="row">
                                    <div class="col-md-4 header-navigation-col">
                                        <h4>O' Level</h4>
                                        <ul>
                                            <li><a href="subject/1">Physics</a></li>
                                            <li><a href="subject/2">Chemistry</a></li>
                                            <li><a href="">Biology</a></li>
                                            <li><a href="">Mathematics</a></li>
                                            <li><a href="">Commerce</a></li>
                                            <li><a href="">History</a></li>
                                            <li><a href="">Geography</a></li>
                                            <li><a href="">Accounts</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 header-navigation-col">
                                        <h4>A' Level</h4>
                                        <ul>
                                            <li><a href="">Physics</a></li>
                                            <li><a href="">Chemistry</a></li>
                                            <li><a href="">Biology</a></li>
                                            <li><a href="">Mathematics</a></li>
                                            <li><a href="">Commerce</a></li>
                                            <li><a href="">History</a></li>
                                            <li><a href="">Geography</a></li>
                                            <li><a href="">Accounts</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 header-navigation-col">
                                        <h4>Primary</h4>
                                        <ul>
                                            <li><a href="">Science</a></li>
                                            <li><a href="">Social Studies</a></li>
                                            <li><a href="">Mathematics</a></li>
                                            <li><a href="">English</a></li>
                                        </ul>

                                        <h4>Advice</h4>
                                        <ul>
                                            <li><a href="index.html">Jackets</a></li>
                                            <li><a href="index.html">Bottoms</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class=""><a href="/notes">Notes</a></li>
                <li class=""><a href="/videos">Videos</a></li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" data-target="#"
                                        href="javascript:;">Schools & Partners</a></li>

                <!-- BEGIN TOP SEARCH -->
                <li class="menu-search">
                    <span class="sep"></span>
                    <i class="fa fa-search search-btn"></i>

                    <div class="search-box">
                        <form action="#">
                            <div class="input-group">
                                <input type="text" placeholder="Search" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit">Search</button>
                    </span>
                            </div>
                        </form>
                    </div>
                </li>
                <!-- END TOP SEARCH -->
            </ul>
        </div>
        <!-- END NAVIGATION -->
    </div>
</div>
            @if (count($errors) > 0)
                 <div class="alert alert-danger" style='margin-bottom:10px'>
                     <ul>
                         @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
            @endif
            @if(session('student_success'))
                 <div class="alert alert-success" role="alert">
                         {{ session('student_success') }}
                 </div>
            @endif

            @if(session('student_error'))
               <div class="alert alert-danger" role="alert">
                         {{ session('student_error') }}
                 </div>
            @endif
          
            @if(session('student_login_errors'))
               <div class="alert alert-danger" role="alert">
                         {{ session('student_login_errors') }}
                 </div>
            @endif
<!-- Header END -->

<!-- login modal -->
<div class="modal fade" id="login_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Login into your Espo account</h4>
            </div>

            <form action="/admin/students/login" method="post" enctype="multipart/form-data">    

                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-12">                         

                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <div class="form-group">
                            <label for="field-1" class="control-label">Email</label>
                            <input type="text" class="form-control" id="field-1" placeholder="Enter Email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="control-label">Password</label>
                            <input type="password" class="form-control" id="field-1" placeholder="Enter password" name="password">
                        </div>

                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Login</button>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- end login modal -->

<!-- registration modal -->
<div class="modal fade" id="registration_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Register for an Espo Account</h4>
            </div>

            <form action="/admin/students/register" method="post" enctype="multipart/form-data">    

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="field-1" class="control-label">Name</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Full Name" name="name">
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="control-label">Phone Number</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Phone Number" name="phone">
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="control-label">Country</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Uganda" name="country">
                            </div>

                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Email</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Teacher Email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Address</label>
                                <input type="text" class="form-control" id="field-1" placeholder="Address" name="address">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Profile Pic</label>
                                <input type="file" class="form-control" id="field-file" placeholder="Placeholder" name="profile_pic">
                            </div>

                            </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="field-1" class="control-label">Description</label>
                            <textarea class="form-control" id="field-ta" rows="5" placeholder="Simple Description" name="profile_desc"></textarea>
                        </div>
                            </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Sign Up</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- end of registration form -->

@section('scripts')
  <!-- App scripts -->
  <script>
  $(function() {

    //login
    $('#login').click(function(){
        $("#login_modal").modal();
    });

    //registration
    $('#register').click(function(){
        $("#registration_modal").modal();
    });

  });
  </script>

  @if(session('authentication_code') == 1)
    <script>
        $(document).ready(function () {
            $('#login-modal').modal('show');
        });
    </script>
  @endif  
@endsection
