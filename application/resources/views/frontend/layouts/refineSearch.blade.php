<section id="search-filter-section" class="js-search-facets search-facets phone-menu" aria-label="Refine your search"
         tabindex="-1" role="complementary">
    <h2>Refine your search</h2>
    @if(isset($levels))
        <section class="js-level"><h3>Level</h3>
            <ul class="facet-list ">
                @foreach($levels as $level)
                    <li>
                        <a href="/courses/{{ $level->id }}">
                            <button class="facet-option" data-value="Advanced" tabindex="0">
                                {{ $level->name }}
                                <span class="count">{{ $level->id }}<span class="sr"> Subjects</span></span>
                            </button>
                        </a>
                    </li>
                @endforeach

            </ul>
            @elseif(isset($side_subjects))
                <section class="js-level"><h3>Subjects</h3>
                    <ul class="facet-list ">
                        @foreach($side_subjects as $sub)
                            <li>
                                <a href="/subject/{{ $sub->id }}">
                                    <button class="facet-option" data-value="Advanced" tabindex="0">
                                        {{ $sub->subject_name }}
                                        <span class="count">{{ $sub->id }}<span class="sr"> Topics</span></span>
                                    </button>
                                </a>
                            </li>
                        @endforeach

                    </ul>
            @elseif(isset($topics))
                <section class="js-level"><h3>Subject Topics</h3>
                    <ul class="facet-list ">
                        @foreach($topics as $topic)
                            <li>
                                <a href="/subject/{{ $topic->id }}">
                                    <button class="facet-option" data-value="Advanced" tabindex="0">
                                        {{ $topic->topic_name }}
                                        <span class="count">{{ $topic->id }}<span class="sr"> Topics</span></span>
                                    </button>
                                </a>
                            </li>
                        @endforeach

                    </ul>
                    @endif

                </section>
        </section>