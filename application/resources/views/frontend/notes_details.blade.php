@extends('frontend.layouts.front')
@section('meta')
    <meta content="Metronic Shop UI description" name="description">
    <meta content="Metronic Shop UI keywords" name="keywords">
    <meta content="keenthemes" name="author">

    <meta property="og:site_name" content="-CUSTOMER VALUE-">
    <meta property="og:title" content="-CUSTOMER VALUE-">
    <meta property="og:description" content="-CUSTOMER VALUE-">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">
@endsection

@section('main')

    <div class="main">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="javascript:;">Subject</a></li>
                <li class="active">{{ $post->post_title }}</li>
            </ul>
            <div class="row margin-bottom-40">
                <!-- BEGIN CONTENT -->
                <div class="col-md-12 col-sm-12">
                    <h3>{{ $post->post_title }}</h3>

                    <div class="content-page">

                        <div class="row margin-bottom-30">
                            <!-- BEGIN CAROUSEL -->
                            <div class="col-md-5 front-carousel">
                                <div class="carousel slide" id="myCarousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <img alt="" src="{{ asset('uploads/files/'.$post->cover_photo) }}"
                                                 style="width: 100% ">

                                            {{--<div class="carousel-caption">
                                                <p>{{ $post->post_title }}</p>
                                            </div>--}}
                                        </div>

                                    </div>
                                    <!-- Carousel nav -->
                                    {{--<a data-slide="prev" href="#myCarousel" class="carousel-control left">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a data-slide="next" href="#myCarousel" class="carousel-control right">
                                        <i class="fa fa-angle-right"></i>
                                    </a>--}}
                                </div>
                                <ul class="blog-info">
                                    <li><i class="fa fa-calendar"></i> {{ $notes->created_at }}</li>
                                    <li><i class="fa fa-comments"></i> 17</li>
                                </ul>

                                @if( $notes->notes_pricing !="0" )
                                    <a class="btn btn-default btn-sm" data-toggle="modal" href="#basic" href="javascript:;">
                                        <i class="fa fa-comments"></i> BUY NOTES</a>
                                @else
                                    <a class="btn blue btn-sm" href="#notesView" data-toggle="modal" href="{{ asset('uploads/files/'.$notes->notes_file) }}">
                                        <i class="fa fa-comments"></i> DOWNLOAD NOTES</a>

                                @endif

                            </div>
                            <!-- END CAROUSEL -->

                            <!-- BEGIN PORTFOLIO DESCRIPTION -->
                            <div class="col-md-7">
                                <p>{{ $notes->description }}</p>
                                @if( !is_null($notes->expectations) )
                                    <h4>Expectations</h4>
                                    <hr/>
                                    <p>{{ $notes->expectations }}</p>
                                    <br>
                                @endif
                                <strong>Teacher Details</strong>

                                <div class="teacher-info">
                                    <img class="pull-left"
                                         src="{{ asset('uploads/teachers_photos/'.$teacher->profile_pic) }}" alt="">

                                    <div class="pull-left teacher-details">
                                        <span class="teacher-name">{{ $teacher->name }}</span><br/>
                                        <span class="teacher-post"><i class="fa fa-map-marker"
                                                                      aria-hidden="true"></i> {{ $teacher->address }}
                                            ,{{$teacher->country}}</span>
                                    </div>
                                </div>


                            </div>
                            <!-- END PORTFOLIO DESCRIPTION -->
                        </div>
                    </div>

                </div>
                <!-- END CONTENT -->
            </div>

        </div>
    </div>
@endsection
<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title uppercase">PAY FOR {{ $post->post_title }} Notes</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">

                        <h4>Notes Details</h4><hr/>
                        <p>
                            <strong>Subject:</strong> <br/>
                            <strong>Topic:</strong> <br/>
                            <strong>Title:</strong> {{ $post->post_title }}<br/>
                            <strong>Amount:</strong> {{ $notes->notes_pricing.' '.$notes->currency  }}<br/>
                            <strong>Teacher:</strong> {{ $teacher->name }}<br/>
                            <strong>Uploaded on:</strong> {{ $notes->created_at }}<br/>
                        </p>


                    </div>
                    <div class="col-md-6">
                        <h4>Pay with Mobile Money</h4><hr/>
                        <form action=""  method="post">
                            <div class="form-group">
                                <label>Mobile Money Number</label>
                                <input type="text" class="form-control" placeholder="2567....." >
                            </div>
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="text" class="form-control" value="{{ $notes->notes_pricing }}" disabled >
                            </div>

                        </form>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm red" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-sm blue"><i class="fa fa-money"></i> Pay</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade bs-modal-lg" id="notesView" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title uppercase">{{ $post->post_title }} </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <iframe name="iframe" class="readpdf" id="iframe" src="http://www.ncca.ie/uploadedfiles/primary/PCRinsights.pdf"></iframe>
                    </div>
                    <div class="col-md-4">
                        <h4>Comments</h4>
                        <div class="comment">
                            <div class="pull-left comment-details">
                                <span class="user-name">{{ $teacher->name }}</span><br/>
                                <span class="comment-date">09-03-2017 19:45</span><br/>
                                <span class="user-comment">Is it possible to programmatically control (center, zoom, etc.) embed PDF content inside the webpage. I prefer PHP, JS or Python, but any other solution would be welcome.</span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Download</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>