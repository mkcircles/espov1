@extends('frontend.layouts.front')
@section('meta')
    <meta content="Metronic Shop UI description" name="description">
    <meta content="Metronic Shop UI keywords" name="keywords">
    <meta content="keenthemes" name="author">

    <meta property="og:site_name" content="-CUSTOMER VALUE-">
    <meta property="og:title" content="-CUSTOMER VALUE-">
    <meta property="og:description" content="-CUSTOMER VALUE-">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">
@endsection

@section('main')

    <div class="main">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="javascript:;">Notes</a></li>
                <li class="active">Uploaded Notes</li>
            </ul>
            <div class="row margin-bottom-40">
                <!-- BEGIN CONTENT -->
                <div class="col-md-12 col-sm-12">
                    <h1>Uploaded Videos</h1>
                    <div class="content-page">

                        <div class="row">

                            @foreach($videos as $video)
                                <div class="owl-item col-md-3 col-sm-4">

                                    <div class="recent-work-item">
                                        <a class="recent-work-description" href="/videos/{{ $video->id }}">
                                            <em><img src="{{ asset('uploads/files/'.$video->cover_photo) }}"
                                                     alt="{{ $video->post_title }}" style="width: 100%; height: 161px; overflow: hidden" class="img-responsive"></em>
                                            <span class="subject">{{ $video->subject_name }}</span>
                                            <h5 class="post-title">{{ $video->post_title }}</h5>

                                            <p>{{ $video->topic_name }}</p>
                                            <ul class="blog-info">
                                                <li><i class="fa fa-calendar"></i> {{ $video->created_at }}</li>
                                                <li><i class="fa fa-user"></i> {{ $video->name }}</li>
                                            </ul>

                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="clearfix"></div>
                        <div class="row pull-right">
                            {{ $videos->links() }}
                        </div>
                    </div>

                </div>
                <!-- END CONTENT -->
            </div>

        </div>
    </div>


@endsection