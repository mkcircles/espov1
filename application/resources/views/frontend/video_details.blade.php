@extends('frontend.layouts.front')
@section('meta')
    <meta content="Metronic Shop UI description" name="description">
    <meta content="Metronic Shop UI keywords" name="keywords">
    <meta content="keenthemes" name="author">

    <meta property="og:site_name" content="-CUSTOMER VALUE-">
    <meta property="og:title" content="-CUSTOMER VALUE-">
    <meta property="og:description" content="-CUSTOMER VALUE-">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">
@endsection

@section('main')

    <div class="main">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="javascript:;">Subject</a></li>
                <li class="active">{{ $post->post_title }}</li>
            </ul>
            <div class="row margin-bottom-40">
                <!-- BEGIN CONTENT -->
                <div class="col-md-12 col-sm-12">
                    <h3>{{ $post->post_title }}</h3>
                    <div class="content-page">

                        <div class="row margin-bottom-30">
                            <!-- BEGIN CAROUSEL -->
                            <div class="col-md-5 front-carousel">
                                <div class="carousel slide" id="myCarousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <img alt="" src="{{ asset('uploads/files/'.$post->cover_photo) }}" style="width: 100% ">
                                            <div class="carousel-caption">
                                                <p>{{ $post->post_title }}</p>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- Carousel nav -->
                                    <a data-slide="prev" href="#myCarousel" class="carousel-control left">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a data-slide="next" href="#myCarousel" class="carousel-control right">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                                <ul class="blog-info">
                                    <li><i class="fa fa-calendar"></i> {{ $video->created_at }}</li>
                                    <li><i class="fa fa-comments"></i> 17</li>
                                </ul>
                            </div>
                            <!-- END CAROUSEL -->

                            <!-- BEGIN PORTFOLIO DESCRIPTION -->
                            <div class="col-md-7">
                                <p>{{ $video->description }}</p>
                                @if( !is_null($video->expectations) )
                                    <h4>Expectations</h4>
                                    <hr/>
                                    <p>{{ $video->expectations }}</p>
                                    <br>
                                @endif
                                <strong>Teacher Details</strong>
                                <div class="teacher-info">
                                    <img class="pull-left" src="{{ asset('uploads/teachers_photos/'.$teacher->profile_pic) }}" alt="">
                                    <div class="pull-left teacher-details">
                                        <span class="teacher-name">{{ $teacher->name }}</span><br/>
                                        <span class="teacher-post"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $teacher->address }},{{$teacher->country}}</span>
                                    </div>
                                </div>

                                @if( $video->video_pricing !="0" )
                                    <a class="btn btn-sm btn-default" data-toggle="modal" href="#basic" href="javascript:;"> PAY FOR VIDEO</a>
                                @else
                                    <a class="btn btn-sm blue" href="#notesView" data-toggle="modal" href="{{ asset('uploads/files/'.$video->notes_file) }}"> DOWNLOAD VIDEOS</a>

                                @endif

                            </div>
                            <!-- END PORTFOLIO DESCRIPTION -->
                        </div>
                    </div>

                </div>
                <!-- END CONTENT -->
            </div>

        </div>
    </div>
<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title uppercase">PAY FOR {{ $post->post_title }} VIDEO</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">

                        <h4>Video Details</h4><hr/>
                        <p>
                            <strong>Subject:</strong> <br/>
                            <strong>Topic:</strong> <br/>
                            <strong>Title:</strong> {{ $post->post_title }}<br/>
                            <strong>Amount:</strong> {{ $video->video_pricing.' '.$video->currency  }}<br/>
                            <strong>Teacher:</strong> {{ $teacher->name }}<br/>
                            <strong>Uploaded on:</strong> {{ $video->created_at }}<br/>
                        </p>


                    </div>
                    <div class="col-md-6">
                        <h4>Pay with Mobile Money</h4><hr/>
                        <form action=""  method="post">
                            <div class="form-group">
                                <label>Mobile Money Number</label>
                                <input type="text" class="form-control" id="phone" placeholder="2567....." required>
                            </div>
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="text" class="form-control" id="amount" value="{{ $video->video_pricing }}" disabled >
                            </div>
                            <input type="hidden" id="post_id" value="{{ $video->post_id }}">
                        </form>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm red" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-sm blue" id="make_payment"><i class="fa fa-money"></i> Pay</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade bs-modal-lg" id="notesView" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title uppercase">{{ $post->post_title }} </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <iframe width="854" height="480" class="viewVideo" src="https://www.youtube.com/embed/{{ $video->video_file }}" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-4">
                        <h4>Comments</h4>
                        <div class="comment">
                            <div class="pull-left comment-details">
                                <span class="user-name">{{ $teacher->name }}</span><br/>
                                <span class="comment-date">09-03-2017 19:45</span><br/>
                                <span class="user-comment">Is it possible to programmatically control (center, zoom, etc.) embed PDF content inside the webpage. I prefer PHP, JS or Python, but any other solution would be welcome.</span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm red" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="button" class="btn btn-sm blue"><i class="fa fa-download"></i> Download</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@stop
@section('scripts')  
  <!-- App scripts -->
  <script>
  $(function() {
    $('#make_payment').click(function(e){
        e.preventDefault();
        var post_id = $("#post_id").val(),
            amount = $('#amount').val(),
            phone = $('#phone').val();
        //are all inouts filled... 
        if(phone == ''){
            $("#phone").select();
        }   
        alert(post_id+' '+amount+' '+phone);    

        var data;
        $.ajax({
          type: "GET",
          url: '/post/pay',
          data: {
            post_id: post_id,
            amount: amount,
            phone: phone
          },
          success: function( data ) {
            console.log(data);
            //popup modal for payment progress...
            //$("#payment_progress").modal();
            //make another post to payleo...
            callPayleo(phone, amount, data);
          },
          error: function(data){
            alert('failed to post data');
          }
        });
    
    });

    function callPayleo(phone, amount, trans_id){
        alert(phone+' '+amount+' '+trans_id);
        alert('sending to payleo');
        var status, code, message, parsed_data, data;
        $.ajax({              
              crossDomain: true,
              type: "POST",
              headers: {"Access-Control-Allow-Origin: ": "*"},
              url: 'https://vendors.pay-leo.com/api/',
              data: {
                transactionId: trans_id,
                consumerKey: 'NiKZXUJunqC8sejOUGZCIz99sO1490367211',
                auth_signature: '1n5sEC8Peme5h47PFBkMDgVW5e1490367211',
                merchantCode: '56318',
                msisdn: phone,
                amount: amount,
                narration: 'payment for ESPO video'                
              },
              success: function( data ) {
                alert('sending...')
                console.log(data);
                parsed_data = jQuery.parseJSON(data);
                alert(parsed_data['status']+' '+parsed_data['code']+' '+parsed_data['message']);

                //update the espo payments table                
                callbackUrl(trans_id, parsed_data['message'], parsed_data['status']);
                
              },
              error: function(data){
                alert('failed to post data');
              }
            });
    }

    function callbackUrl(trans_id, message, status){

    }

  
  });
  </script>
@endsection
