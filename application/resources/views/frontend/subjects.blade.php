@extends('frontend.layouts.front')
@section('meta')
    <meta name="description" content="Connect with thousands of other learners and debate ideas, discuss course material, and get help mastering concepts." />
    <meta name="keywords" content="Education, Study, videos, notes, presentations, teachers" />
    <meta property="og:url"  content="http://espo.ug">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Find Simplified educative content for all school levels, Videos, Notes-Uganda -www.espo.ug">
    <meta property="og:image" content="http://i.imgur.com/DjkYOto.jpg"/>
    <meta property="og:description" content="Each course is like an interactive textbook, featuring pre-recorded videos, quizzes, and projects.">
    <meta property="og:site_name" content="ESPO Uganda">
    <meta property="article:author" content="https://www.facebook.com/espoug">

    <meta property="article:section" content="Uganda Education" />
    <meta property="article:tag" content="Uganda Education Videos and Notes" />


@endsection

@section('main')

    <div class="main">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="/courses">Courses</a></li>
                <li class="active">Uploaded Notes</li>
            </ul>
            <div class="row margin-bottom-40">
                <!-- BEGIN CONTENT -->
                <div class="col-md-3">
                    @include('frontend.layouts.refineSearch')
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="row margin-bottom-20">
                        @include('frontend.layouts.searchDiv')
                    </div>

                    <div class="content-page">

                        @foreach($subjects as $subject)
                            <div class="col-md-3 course-card">
                                <a href="/subject/{{ $subject->id }}">
                                    <div class="flex-1 side-wrapper front">
                                        <div class="side">
                                            <div class="image-container"
                                                 style="background-image:url(https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://d2j5ihb19pt1hq.cloudfront.net/sdp_page/s12n_logos/python.jpg?auto=format%2Ccompress&amp;dpr=1&amp;fit=crop&amp;w=225&amp;h=130);"
                                                 data-reactid="135"></div>
                                            <div class="content">
                                                <div class="partner-names body-1-text"><span>{{ $subject->name }}</span>
                                                </div>
                                                <div class="offering-name display-1-text">{{ $subject->subject_name }}</div>
                                            </div>
                                            <div class="sub-count">
                                                <div class="price-and-count-info">
                                                    <span class="course-count color-secondary-text"><span>5 Videos</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>

                            </div>
                        @endforeach

                    </div>
                </div>

            </div>
            <!-- END CONTENT -->
        </div>

    </div>
    </div>


@endsection