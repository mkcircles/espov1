@extends('teacher.layout.teacher')

@section('content')

<div class="container">
    <div class="row" style="margin-top:140px">
        <div class="col-md-4 col-md-offset-4 well">
            <form role="form" action="/users/authenticate" method="post" name="signupform">
                {{ csrf_field() }}
                @if (count($errors) > 0)
                    <div class="alert alert-danger" style='margin-bottom:10px'>
                         <ul>
                             @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                             @endforeach
                         </ul>
                     </div>
                @endif
                @if(session('success'))
                     <div class="alert alert-success" role="alert">
                             {{ session('success') }}
                     </div>
                @endif

                @if(session('user_login_errors'))
                   <div class="alert alert-danger" role="alert">
                             {{ session('user_login_errors') }}
                     </div>
                @endif

                @if(session('standard_error'))
                   <div class="alert alert-danger" role="alert">
                             {{ session('standard_error') }}
                     </div>
                @endif

                @if(session('email_sent_success'))
                   <div class="alert alert-success" role="alert">
                             {{ session('email_sent_success') }}
                     </div>
                @endif

                @if(session('expired_token'))
                   <div class="alert alert-success" role="alert">
                             {{ session('expired_token') }}
                     </div>
                @endif

                <fieldset>
                    <legend>Sign In</legend>

                    <div class="form-group">
                        <label for="name">Email</label>
                        <input type="text" name="email" placeholder="Enter email" value="" class="form-control" />
                        @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label for="name">Password</label>
                        <input type="password" name="password" placeholder="Enter password" value="" class="form-control" />
                        @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Sign In" class="btn btn-primary pull-right" /><a href="/forgot/password">Forgot Password?</a>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>

@stop