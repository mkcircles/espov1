<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id');
            $table->string('question_file');
            $table->enum('file_type', ['pdf', 'ppt', 'word']);
            $table->enum('question_status', ['Active', 'Inactive']);
            $table->integer('views');
            $table->mediumInteger('question_pricing');
            $table->enum('currency', ['UGX', 'USD']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
