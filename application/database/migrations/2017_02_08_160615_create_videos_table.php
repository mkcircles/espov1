<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id');
            $table->string('video_file');
            $table->enum('file_type', ['pdf', 'ppt', 'word']);
            $table->string('description');
            $table->string('requirements')->nullable();
            $table->string('expectations')->nullable();
            $table->enum('video_status', ['Active', 'Inactive']);
            $table->integer('views');
            $table->mediumInteger('video_pricing');
            $table->enum('currency', ['UGX', 'USD']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
