<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id');
            $table->string('notes_file');
            $table->enum('file_type', ['pdf', 'ppt', 'word']);
            $table->string('description');
            $table->text('requirements')->nullable();
            $table->text('expectations')->nullable();
            $table->enum('notes_status', ['Active', 'Inactive']);
            $table->integer('views');
            $table->mediumInteger('notes_pricing');
            $table->enum('currency', ['UGX', 'USD']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
